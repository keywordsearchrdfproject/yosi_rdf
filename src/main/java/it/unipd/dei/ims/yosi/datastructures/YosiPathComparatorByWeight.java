package it.unipd.dei.ims.yosi.datastructures;

import java.util.Comparator;

/** Comparator to compare two YosiPath following their weight.
 * To be used with a min heap.
 * 
 * */
public class YosiPathComparatorByWeight implements Comparator<YosiPath>  {

//	@Override
	public int compare(YosiPath o1, YosiPath o2) {
		double w1 = o1.getWeight();
		double w2 = o2.getWeight();
		if(w1<w2)
			return -1;
		else if(w1>w2)
			return 1;
		return 0;
	}
}
