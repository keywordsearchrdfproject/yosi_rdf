package it.unipd.dei.ims.yosi.datastructures;

import java.sql.Connection;
import java.util.Map;

import org.openrdf.model.Model;

/** Reprsents the final result of a Dijkstra execution*/
public class DijkstraExecution {
	
	/** The root of the generated tree*/
	private YosiNode root;
	
	/** The final obtained graph*/
	private Model cluster;
	
	/** A map with the set of YosiNodes composing the graph v* identified by their IRI.
	 * */
	private Map<String, YosiNode> frontier;
	
	/** A connection to a SQL database to be used as support for data that can be useful
	 * .*/
	private Connection connection;
	
	/** A map containing the leaves node identified by their IRI.
	 * */
	private Map<String, YosiNode> leaves;

	public YosiNode getRoot() {
		return root;
	}

	public void setRoot(YosiNode root) {
		this.root = root;
	}

	public Model getCluster() {
		return cluster;
	}

	public void setCluster(Model cluster) {
		this.cluster = cluster;
	}

	public Map<String, YosiNode> getFrontier() {
		return frontier;
	}

	public void setFrontier(Map<String, YosiNode> frontier) {
		this.frontier = frontier;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Map<String, YosiNode> getLeaves() {
		return leaves;
	}

	public void setLeaves(Map<String, YosiNode> leaves) {
		this.leaves = leaves;
	}
	

}
