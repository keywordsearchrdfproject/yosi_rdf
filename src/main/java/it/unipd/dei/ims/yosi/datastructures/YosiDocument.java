package it.unipd.dei.ims.yosi.datastructures;

/** Represents a document with its score.
 *  */
public class YosiDocument {
	
	/** the docno of this document*/
	private String docno;
	
	/** the score of this document */
	private double score;
	
	
	// ##### ##### #####
	
	public String toString() {
		return "docno: " + this.docno + " score: " + this.score;
	}

	public String getDocno() {
		return docno;
	}

	public void setDocno(String docno) {
		this.docno = docno;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
}
