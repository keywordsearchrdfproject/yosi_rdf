package it.unipd.dei.ims.yosi.datastructures;

import java.sql.Connection;

import org.terrier.structures.Index;

/** This class is used as wrapper to include parameters when they need to be passed to various methods
 * */
public class YosiDataWrapper {

	/** String to be used to connect to a PostgreSQL database.*/
	private String jdbcConnectingString;

	/** A connection to a database (in this project, it isa PostgreSQL database)*/
	private Connection connection;

	/** One word of the query.*/
	private String queryWord;

	/** The whole query which we are using.*/
	private String query;

	/** A string used to contain a path to some file that needs to be read (the 
	 * meaning and purpose of the file can change depending on the application).
	 * */
	private String filePath;

	/** The index of the documents generated by the single nodes
	 * of the RDF graph
	 * */
	private Index unigramNodeIndex;
	
	/** The index of the documents generated by the single 
	 * nodes of the RDF graph where the tokens are bigrams
	 * of words. */
	private Index bigramNodeIndex;

	/** Index of the virtual documents v* (unigrams)*/
	private Index unigramVirtualDocumentsIndex;
	
	/** Index of the virtual documents v* using the
	 * bigrams*/
	private Index bigramVirtualDocumentsIndex;

	/** ID of a node. It can change meaning depending on the situation.
	 * */
	private int nodeID;

	/** The parameter sigma that appears in the gaussian weight of the weighted term frequency,
	 * as described in Yosi 2016.
	 * */
	private double sigma;

	/** An int that can be used for different purposes, depending on the application.
	 * */
	private int n;

	private double lambdaUnigramContent; 
	private double lambdaUnigramTitle; 
	private double lambdaBigramContent; 
	private double lambdaBigramTitle; 
	private double lambdaQueryIndependent;
	
	/** The total degree of the graph, i.e. the sum of all the degrees in the graph.
	 * */
	private long totalDegree;
	
	/** Path of the file where to save the list of the top-n keyword nodes*/
	private String keywordsOutputFile;
	
	private String rootsOutputFile;



	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public String getQueryWord() {
		return queryWord;
	}

	public void setQueryWord(String queryWord) {
		this.queryWord = queryWord;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public Index getNodeIndex() {
		return unigramNodeIndex;
	}

	public void setNodeIndex(Index nodeIndex) {
		this.unigramNodeIndex = nodeIndex;
	}

	public Index getVirtualDocumentsIndex() {
		return unigramVirtualDocumentsIndex;
	}

	public void setVirtualDocumentsIndex(Index virtualDocumentsIndex) {
		this.unigramVirtualDocumentsIndex = virtualDocumentsIndex;
	}

	public int getNodeID() {
		return nodeID;
	}

	public void setNodeID(int nodeID) {
		this.nodeID = nodeID;
	}

	public double getSigma() {
		return sigma;
	}

	public void setSigma(double sigma) {
		this.sigma = sigma;
	}

	public double getLambdaUnigramContent() {
		return lambdaUnigramContent;
	}

	public void setLambdaUnigramContent(double lambdaUnigramContent) {
		this.lambdaUnigramContent = lambdaUnigramContent;
	}

	public double getLambdaUnigramTitle() {
		return lambdaUnigramTitle;
	}

	public void setLambdaUnigramTitle(double lambdaUnigramTitle) {
		this.lambdaUnigramTitle = lambdaUnigramTitle;
	}

	public double getLambdaBigramContent() {
		return lambdaBigramContent;
	}

	public void setLambdaBigramContent(double lambdaBigramContent) {
		this.lambdaBigramContent = lambdaBigramContent;
	}

	public double getLambdaBigramTitle() {
		return lambdaBigramTitle;
	}

	public void setLambdaBigramTitle(double lambdaBigramTitle) {
		this.lambdaBigramTitle = lambdaBigramTitle;
	}

	public double getLambdaQueryIndependent() {
		return lambdaQueryIndependent;
	}

	public void setLambdaQueryIndependent(double lambdaQueryIndependent) {
		this.lambdaQueryIndependent = lambdaQueryIndependent;
	}

	public long getTotalDegree() {
		return totalDegree;
	}

	public void setTotalDegree(long totalDegree) {
		this.totalDegree = totalDegree;
	}

	public String getKeywordsOutputFile() {
		return keywordsOutputFile;
	}

	public void setKeywordsOutputFile(String keywordsOutputFile) {
		this.keywordsOutputFile = keywordsOutputFile;
	}

	public Index getBigramNodeIndex() {
		return bigramNodeIndex;
	}

	public void setBigramNodeIndex(Index bigramNodeIndex) {
		this.bigramNodeIndex = bigramNodeIndex;
	}

	public Index getBigramVirtualDocumentsIndex() {
		return bigramVirtualDocumentsIndex;
	}

	public void setBigramVirtualDocumentsIndex(Index bigramVirtualDocumentsIndex) {
		this.bigramVirtualDocumentsIndex = bigramVirtualDocumentsIndex;
	}

	public Index getUnigramNodeIndex() {
		return unigramNodeIndex;
	}

	public void setUnigramNodeIndex(Index unigramNodeIndex) {
		this.unigramNodeIndex = unigramNodeIndex;
	}

	public Index getUnigramVirtualDocumentsIndex() {
		return unigramVirtualDocumentsIndex;
	}

	public void setUnigramVirtualDocumentsIndex(Index unigramVirtualDocumentsIndex) {
		this.unigramVirtualDocumentsIndex = unigramVirtualDocumentsIndex;
	}

	public String getRootsOutputFile() {
		return rootsOutputFile;
	}

	public void setRootsOutputFile(String rootsOutputFile) {
		this.rootsOutputFile = rootsOutputFile;
	}
}
