package it.unipd.dei.ims.yosi.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;
import org.terrier.structures.postings.bit.FieldIterablePosting;
import org.terrier.utility.ApplicationSetup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.bigram.Bigram;
import it.unipd.dei.ims.terrier.bigram.BigramUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiDocument;
import it.unipd.dei.ims.yosi.datastructures.YosiDocumentComparator;

/** Yosi algorithm: phase 8 (online)
 * <p>
 * Ranking of the answer graphs and printing of the results.
 * */
public class AnswerRankingPhase {

	/** Index of unigrams*/
	private Index unigramIndex;

	/** Index of bigrams*/
	private Index bigramIndex;

	private int numberOfDocuments;

	/** Query the class is working on when ranking the documents
	 * */
	private String query;

	private Lexicon<String> unigramLexicon;
	private Lexicon<String> bigramLexicon;

	protected PostingIndex<Pointer> invertedIndex;


	private double lambdaUnigramContent; 
	private double lambdaUnigramTitle; 
	private double lambdaBigramContent; 
	private double lambdaBigramTitle; 
	private double lambdaQueryIndependent;

	private String jdbcConnectionString;

	/** Path of the file with the statis scores*/
	private String staticScoreFilePath;
	
	/** Path of the directory with the unigram index of the answer collection*/
	private String answerUnigramIndexPath;
	
	/** Path of the directory with the bigram index of the answer collection*/
	private String answerBigramIndexPath;
	
	/** Path of the file where we write the answer of the file, a ranking
	 * */
	private String answerRankingOutputFilePath;

	/** A map with a score for each answer graph. The score 
	 * corresponds to the prior.
	 * */
	private Map<Integer, Double> staticScoreMap;

	public AnswerRankingPhase() {
		this.staticScoreMap = new HashMap<Integer, Double>();

		this.setup();
	}
	
	/** Call this method after you have initialized all the string fields that you need in 
	 * order to point to the correct directories and indexes in order to fill the
	 * other fields.
	 * */
	public void initialize() {
		//set the indexes
		this.unigramIndex = IndexOnDisk.createIndex(this.answerUnigramIndexPath, "data");
		this.bigramIndex = IndexOnDisk.createIndex(this.answerBigramIndexPath, "data");

		this.unigramLexicon = this.unigramIndex.getLexicon();
		this.bigramLexicon = this.bigramIndex.getLexicon();
		
		this.invertedIndex = (PostingIndex<Pointer>) this.unigramIndex.getInvertedIndex();
		
		this.getMapOfScore();
	}

	private void setup() {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			String terrier_home = map.get("terrier.home");
			String terrier_etc = map.get("terrier.etc");

			System.setProperty("terrier.home", terrier_home);
			System.setProperty("terrier.etc", terrier_etc);

			this.answerUnigramIndexPath = map.get("answers.unigram.index.directory.path");

			this.answerBigramIndexPath = map.get("answers.bigram.index.directory.path");

			this.query = map.get("query");
			

			lambdaUnigramTitle = Double.parseDouble(map.get("lambda.unigram.title"));
			lambdaUnigramContent = Double.parseDouble(map.get("lambda.unigram.content"));
			lambdaBigramTitle= Double.parseDouble(map.get("lambda.bigram.title"));
			lambdaBigramContent = Double.parseDouble(map.get("lambda.bigram.content"));
			lambdaQueryIndependent = Double.parseDouble(map.get("lambda.query.independent"));

			jdbcConnectionString = map.get("jdbc.connecting.string");

			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/answer_ranking_phase.properties");
			this.staticScoreFilePath= map.get("support.directory.path") + "/static_scores.xml";

			//build the lists and maps
			this.staticScoreMap = new HashMap<Integer, Double>();
			
			this.answerRankingOutputFilePath = map.get("answer.ranking.output.file.path");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**Read from a file static_scores.xml where previously
	 * in the pipeline we saved the query independent scores of the
	 * answer graph, that is the third component of the scoring function
	 * proposed by Yosi 2016 in order to compute the
	 * score of a graph.
	 * <p>
	 *  */
	private void getMapOfScore() {
		//open the xml file
		File fXmlFile = new File(this.staticScoreFilePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			NodeList nList = doc.getElementsByTagName("score");
			for(int i = 0; i < nList.getLength(); ++i) {
				Element n = (Element) nList.item(i);
				String rootId = n.getAttribute("nodeId");
				String score = n.getTextContent();
				this.staticScoreMap.put(Integer.parseInt(rootId), Double.parseDouble(score));
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void rankTheAnswers() throws IOException {
		//take the number of total documents in the collection. 
		this.numberOfDocuments = this.unigramIndex.getCollectionStatistics().getNumberOfDocuments();
		//get the meta index to get the docno of the documents
		MetaIndex metaIndex = unigramIndex.getMetaIndex();

		List<YosiDocument> docList = new ArrayList<YosiDocument>();

		String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from " + DatabaseState.getSchema() +
				".yosi_node";
		PreparedStatement totalDegreeStatement;
		int totalDegree = 0;
		Connection connection = null;
		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
			
			totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet rs = totalDegreeStatement.executeQuery();
			if(rs.next())
				totalDegree = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection != null) {
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
//					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		//rank the documents one by one
		for(int docid = 0; docid < numberOfDocuments; ++docid) {

			String docno = metaIndex.getItem("docno", docid);
			//computation of the score
			double score = this.rankOneDocument(docid, Integer.parseInt(docno), totalDegree);

			//I use the object YosiNode as document
			YosiDocument document = new YosiDocument();
			document.setDocno(docno);
			document.setScore(score);

			docList.add(document);
		}

		//rank the documents
		Collections.sort(docList, new YosiDocumentComparator());
		Path outputPath = Paths.get(this.answerRankingOutputFilePath);
		BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
		
		int i = 0;
		for(YosiDocument doc : docList) {
			//write on file:
			writer.write("query_no Q0 " + doc.getDocno() + " " + i + " " + doc.getScore() + " YosiLM");
			writer.newLine();
			
			System.out.println(doc);
		}
		writer.close();
	}
	
	/** Creates the rank of one document. 
	 * 
	 * @param docid the docid of the document given to it by Terrier
	 * @param rId the docno of the document, the number identificator used inside
	 * the database to identify the root r of the document
	 * */
	private double rankOneDocument(int docid, int rId, int totalDegree) {
		//here we rank the i-th document
		double score = 0;
		//take the id of the document

		//the scoring of this document requests three elements
		ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
		System.setProperty("tokeniser", "EnglishTokeniser");
		score += this.computeUnigramComponent(docid, rId);

		//***** SECOND COMPONENT: BIGRAMS *****
		//need to put the bigram toleniser for the correct elaboration of words
		ApplicationSetup.setProperty("tokeniser", "BigramTokeniser");
		System.setProperty("tokeniser", "BigramTokeniser");
		score += computeBigramComponents(docid);

		//***** THIRD COMPONENT: QUERY INDEPENDENT *****
		score += computeQueryIndependentPotentialFunction(rId, totalDegree);
		
		return score;
	}
	
	private double computeUnigramComponent(int docid, int rId) {
		//the potential function requests two elements, for the title field and the content field
		double score = 0;

		//take the query 
		List<String> queryWords = TerrierUsefulMethods.pipelineStopWordsAndStemmerToListDitinctWords(this.query);

		//***** compute the alphas *****
		/*we compute the alphas outside the next method because
		 * they are independent from the query word,
		 * so we spare computations.
		 * */
		try {
			Pair<Double, Double> alphas = this.computeAlphasForNodesUnigram(docid, rId);
			for(String s : queryWords) {
				//sum over all query words
				score += this.computeOneUnigramComponent(docid, rId, s, alphas);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return score;
	}
	
	private Pair<Double, Double> computeAlphasForNodesUnigram(int docid, int rId) throws IOException {
		//get the average length of the fields inside one document. These are the mu
		double[] averageFieldsLength = this.unigramIndex.getCollectionStatistics().getAverageFieldLengths();
		double muTitle = averageFieldsLength[0];
		double muContent = averageFieldsLength[1];

		//now we need the length of the document in the two fields
		double xTitle = 0, xContent = 0;


		//the next 5 lines of code required me almost 2 hours of research. Thank you Terrier documentation
		//this code finds the length of the fields of this current document
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) this.unigramIndex.getDirectIndex();
		DocumentIndex doi = unigramIndex.getDocumentIndex();
		//get posting list of this document
		FieldIterablePosting postings = (FieldIterablePosting) di.getPostings(doi.getDocumentEntry(docid));
		int[] fieldsLength = postings.getFieldLengths();
		xTitle = fieldsLength[0];
		xContent = fieldsLength[1];
		double tAlpha = (double) muTitle / (muTitle + xTitle);
		double cAlpha = (double) muContent / (muContent + xContent);

		Pair<Double, Double> alphaPair = new MutablePair<Double, Double>(tAlpha, cAlpha);
		return alphaPair;
	}
	
	private double computeOneUnigramComponent(int docid, int rId, String queryWord, Pair<Double, Double> alphas) {

		//**** 1 : compute the maximum likelihood estimator P(q_i, v*) for the title field
		Pair<Double, Double> unigramMLE = computeUnigramMaximumLikelihoodEstimators(docid, queryWord);

		//***** 3 : compute the smoothing values ****** //
		Pair<Double, Double> unigramSmooth = computeUnigramSmoothingValues(queryWord);

		//title part
		//first get the values
		double titleAlpha = alphas.getLeft();
		double titleMLE = unigramMLE.getLeft();
		double titleSmooth = unigramSmooth.getLeft();
		//the computations
		double logArgument = ((1 - titleAlpha) * titleMLE) + ((titleAlpha)*titleSmooth);
		double titleLog = 0;
		if(logArgument>0)
			titleLog = Math.log(logArgument);

		//content part
		double contentAlpha = alphas.getRight();
		double contentMLE = unigramMLE.getRight();
		double contentSmooth = unigramSmooth.getRight();
		// the computations
		logArgument = ((1 - contentAlpha) * contentMLE) + ((contentAlpha)*contentSmooth);
		double contentLog = /*Double.NEGATIVE_INFINITY*/ 0;
		if(logArgument>0)
			contentLog = Math.log(logArgument);

		//all together now
		double unigramContribution = this.lambdaUnigramTitle * titleLog + 
				this.lambdaUnigramContent * contentLog;
		return unigramContribution;
	}
	
	/** COmputes the Maximum Likelihood Estimator for the title field and the content field for unigrams.
	 * */
	private Pair<Double, Double> computeUnigramMaximumLikelihoodEstimators(int docid, String queryWord) {

		//***** MLE in this document *****

		//get length of the two fields of the document
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) this.unigramIndex.getDirectIndex();
		DocumentIndex doi = unigramIndex.getDocumentIndex();
		FieldIterablePosting postings;
		int titleFieldLength = 0, contentFieldLength = 0;
		int[] fieldsLength = {0, 0};
		int[] c = {0, 0};
		int cTitle = 0, cContent = 0;
		try {
			//get the denominators, lengths of the fields of this document
			postings = (FieldIterablePosting) di.getPostings(doi.getDocumentEntry(docid));
			fieldsLength = postings.getFieldLengths();
			
			//look for every word of the document in the lexicon until you find the queryword
			while (postings.next() != IterablePosting.EOL) {
				Map.Entry<String, LexiconEntry> lee = this.unigramLexicon.getLexiconEntry(postings.getId());
				String word = lee.getKey();
				if(word.equals(queryWord)) {
					c = postings.getFieldFrequencies();
					break;
				}
			}
			cTitle = c[0];
			cContent = c[1];
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		titleFieldLength = fieldsLength[0];
		contentFieldLength = fieldsLength[1];

		double titleMLE = 0, contentMLE = 0;

		if(titleFieldLength!=0) {
			titleMLE = (double) cTitle / titleFieldLength;
		}

		if(contentFieldLength != 0) {
			contentMLE = (double) cContent / contentFieldLength;
		}

		Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
		return pair;

	}
	
	private Pair<Double, Double> computeUnigramSmoothingValues(String queryWord) {
		//get the total length of the collection 
		long[] denominators = this.unigramIndex.getCollectionStatistics().getFieldTokens();
		long titleDenominator = denominators[0];
		long contentDenominator = denominators[1];

		FieldLexiconEntry fLe = (FieldLexiconEntry)unigramLexicon.getLexiconEntry(queryWord);
		int[] frequencies = {0, 0}; 
		
		if(fLe!=null) {
			frequencies = fLe.getFieldFrequencies();
		}

		int titleNumerator = frequencies[0];
		int contentNumerator = frequencies[1];

		double tVal = (double) titleNumerator / titleDenominator;
		double cVal = (double) contentNumerator / contentDenominator;

		Pair<Double, Double> pair = new MutablePair<Double, Double>(tVal, cVal);
		return pair;
	}
	
	private double computeBigramComponents(int docid) {
		double score = 0;

		//extrapolate all the possible bigrams from the query
		List<Bigram> bigramList = BigramUsefulMethods.extractDistinctBigramsFromStringWithPipeline(query);

		/* we compute the alphas now because they are the same throughout the next computation */
		Pair<Double, Double> alphas = this.computeAlphasForBigrams(docid);
		
		//now that we have the alphas, we compute the sum over all the bigrams
		for(Bigram queryBigram : bigramList) {
			score += this.computeOneBigramComponent(docid, queryBigram, alphas);
		}
		
		return score;
	}
	
	private double computeQueryIndependentPotentialFunction(int docno, int totalDegree) {
		//now read the score from the static score map
		return this.lambdaQueryIndependent * this.staticScoreMap.get(docno);
	}
	
	/** Compute the two alpha values (one for the content field and one for the title field) 
	 *  for the bigram potential functions for the Dirichlet smoothing procedure.
	 *  
	 *  @param docid the id of the document where we are working.
	 *  */
	private Pair<Double, Double> computeAlphasForBigrams(int docid) {
		//get the mu of the two fields
		double[] averageFieldsLength = this.bigramIndex.getCollectionStatistics().getAverageFieldLengths();
		double muTitle = averageFieldsLength[0];
		double muContent = averageFieldsLength[1];

		//now get the average length of the document's fields
		double xTitle = 0, xContent = 0;

		PostingIndex<Pointer> di = (PostingIndex<Pointer>) this.bigramIndex.getDirectIndex();
		DocumentIndex doi = bigramIndex.getDocumentIndex();
		//get posting list of this document
		FieldIterablePosting postings;
		try {
			//the postings are like iterators that give information for each word in the document. Also,
			//they give frequencies, that are what we need 
			postings = (FieldIterablePosting) di.getPostings(doi.getDocumentEntry(docid));
			int[] fieldsLength = postings.getFieldLengths();
			xTitle = fieldsLength[0];
			xContent = fieldsLength[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		double tAlpha = (double) muTitle / (muTitle + xTitle);
		double cAlpha = (double) muContent / (muContent + xContent);

		Pair<Double, Double> alphaPair = new MutablePair<Double, Double>(tAlpha, cAlpha);
		return alphaPair;
	}
	
	private double computeOneBigramComponent(int docid, Bigram queryBigram, Pair<Double, Double> alphas) {
		//first, compute the Maximum Likelihood Estimators
		Pair<Double, Double> bigramMLE = computeBigramMaximumLikelihoodEstimators(docid, queryBigram);
		
		//now compute the Dirichlet smoothing values
		Pair<Double, Double> bigramSmooth = computeBigramSmoothingValues(queryBigram);
		
		//title part
		double titleAlpha = alphas.getLeft();
		double titleMLE = bigramMLE.getLeft();
		double titleSmooth = bigramSmooth.getLeft();

		//the computations
		double logArgument = ((1 - titleAlpha) * titleMLE) + ((titleAlpha)*titleSmooth);
		double titleLog = /*Double.NEGATIVE_INFINITY*/ 0;
		if(logArgument > 0)
			titleLog = Math.log(logArgument);

		//content part
		double contentAlpha = alphas.getRight();
		double contentMLE = bigramMLE.getRight();
		double contentSmooth = bigramSmooth.getRight();
		// the computations
		logArgument = ((1 - contentAlpha) * contentMLE) + ((contentAlpha)*contentSmooth);
		double contentLog = /*Double.NEGATIVE_INFINITY*/ 0;
		if(logArgument > 0)
			contentLog = Math.log(logArgument);

		//all together now
		double bigramContribution = this.lambdaBigramTitle * titleLog + 
				this.lambdaBigramContent * contentLog;
		return bigramContribution;
	}
	
	private Pair<Double, Double> computeBigramMaximumLikelihoodEstimators(int docid, Bigram queryBigram) {
		/* MLE in this document*/
		//get the frequencies of the query bigram in the fields, the numerators of the MLE
		int[] c = {0, 0};
		int cTitle = 0, cContent = 0;
		
		//get the lengths of the documents (the denominators)
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) this.bigramIndex.getDirectIndex();
		DocumentIndex doi = bigramIndex.getDocumentIndex();
		FieldIterablePosting postings;
		int titleFieldLength = 0, contentFieldLength = 0;
		int[] fieldsLength = {0, 0};
		try {
			postings = (FieldIterablePosting) di.getPostings(doi.getDocumentEntry(docid));
			fieldsLength = postings.getFieldLengths();
			//for every bigram in the document
			while(postings.next() != IterablePosting.EOL) {
				Map.Entry<String, LexiconEntry> lee = this.bigramLexicon.getLexiconEntry(postings.getId());
				String word = lee.getKey();
				if(word.equals(queryBigram.getStandardExpression())) {
					c = postings.getFieldFrequencies();
					break;
				}
			}
			cTitle = c[0];
			cContent = c[1];
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		titleFieldLength = fieldsLength[0];
		contentFieldLength = fieldsLength[1];
		
		double titleMLE = 0, contentMLE = 0;

		if(titleFieldLength!=0) {
			titleMLE = (double) cTitle / titleFieldLength;
		}

		if(contentFieldLength != 0) {
			contentMLE = (double) cContent / contentFieldLength;
		}

		Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
		return pair;
	}
	
	private Pair<Double, Double> computeBigramSmoothingValues(Bigram queryBigram) {
		//get the denominators, total size of the collection (w.r.t. fields)
		long[] denominators = this.bigramIndex.getCollectionStatistics().getFieldTokens();
		long titleDenominator = denominators[0];
		long contentDenominator = denominators[1];
		
		FieldLexiconEntry fLe = (FieldLexiconEntry)bigramLexicon.getLexiconEntry(queryBigram.getStandardExpression());
		
		int[] frequencies = {0, 0}; 
		if(fLe != null) {
			frequencies = fLe.getFieldFrequencies();
		}
		
		int titleNumerator = frequencies[0];
		int contentNumerator = frequencies[1];
		
		double tVal = (double) titleNumerator / titleDenominator;
		double cVal = (double) contentNumerator / contentDenominator;

		Pair<Double, Double> pair = new MutablePair<Double, Double>(tVal, cVal);
		return pair;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getStaticScoreFilePath() {
		return staticScoreFilePath;
	}

	public void setStaticScoreFilePath(String staticScoreFilePath) {
		this.staticScoreFilePath = staticScoreFilePath;
	}

	public String getAnswerUnigramIndexPath() {
		return answerUnigramIndexPath;
	}

	public void setAnswerUnigramIndexPath(String answerUnigramIndexPath) {
		this.answerUnigramIndexPath = answerUnigramIndexPath;
	}

	public String getAnswerBigramIndexPath() {
		return answerBigramIndexPath;
	}

	public void setAnswerBigramIndexPath(String answerBigramIndexPath) {
		this.answerBigramIndexPath = answerBigramIndexPath;
	}

	public String getAnswerRankingOutputFilePath() {
		return answerRankingOutputFilePath;
	}

	public void setAnswerRankingOutputFilePath(String answerRankingOutputFilePath) {
		this.answerRankingOutputFilePath = answerRankingOutputFilePath;
	}

}
