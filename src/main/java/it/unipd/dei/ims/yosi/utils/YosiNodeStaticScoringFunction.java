package it.unipd.dei.ims.yosi.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.terrier.bigram.Bigram;
import it.unipd.dei.ims.terrier.bigram.BigramUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiDataWrapper;

/** This class represents the static scoring function of an entity
 * (node or document) as described by Yosi 2016.
 * 
 * @author Dennis Dosso
 * 
 * */
public class YosiNodeStaticScoringFunction {

	
	private static String SQL_GET_NODE_FIELDS = "SELECT title_, content_ FROM " + 
	DatabaseState.getSchema() + ".yosi_node where id_ = ?";

	private static String SQL_GET_STATIC_RELATIVE_WEIGHT = "SELECT v_id, u_id, ws_ FROM " +
	DatabaseState.getSchema() + ".yosi_virtual_document "
			+ " where v_id=? and u_id=?";

	private static String SQL_GET_VIRTUAL_DOCUMENT_NODES = "select v_id, u_id, ws_ "
			+ "from " + DatabaseState.getSchema() + ".yosi_virtual_document where v_id=?";

	private static String SQL_GET_LENGTHS = "SELECT unigram_length_title, unigram_length_content, "
			+ "bigram_length_title, bigram_length_content from " +
			DatabaseState.getSchema()  + ".yosi_node where id_ = ?";
	
	/** This methods implements the score function for a node as described by
	 * Yosi 2016.
	 * */
	public static double computeKeywordNodeScore(int vDocno, int vDocid, Connection connection, String query, 
			YosiDataWrapper parameters) {
		double score = 0;

		//**** FIRST COMPONENT: UNIGRAMS *****
		//need to put the unigram tokeniser for the correct elaboration of words
		ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
		System.setProperty("tokeniser", "EnglishTokeniser");
		score += computeUnigramComponent(vDocno, vDocid, connection, query, parameters);

		//***** SECOND COMPONENT: BIGRAMS *****
		//need to put the bigram toleniser for the correct elaboration of words
		ApplicationSetup.setProperty("tokeniser", "BigramTokeniser");
		System.setProperty("tokeniser", "BigramTokeniser");
		score += computeBigramComponents(vDocno, parameters);
		
		//re set the EnglishTokeniser at the end
		ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
		System.setProperty("tokeniser", "EnglishTokeniser");

		//***** THIRD COMPONENT: QUERY INDEPENDENT *****
		score+=computeQueryIndependentPotentialFunction(vDocno, parameters);

		return score;
	}
	
	/** Computes the first part of the score of a node u. That means it computes the title and content 
	 * part of the unigram component.
	 * */
	private static double computeUnigramComponent(int vDocno, int vDocid, Connection connection, String query,
			YosiDataWrapper parameters) {
		double score = 0;

		//take the keyword and process it through the pipeline
		List<String> queryWords = 
				TerrierUsefulMethods.
				pipelineStopWordsAndStemmerToListDitinctWords(query);

		//***** compute the alphas *****
		/*we compute the alphas outside the next method because
		 * they are independent from the query word,
		 * so we spare computations.
		 * */
		Pair<Double, Double> alphas = computeAlphasForNodesUnigram(vDocno, vDocid, parameters);
				
		//sum over all the query words
		for(String s : queryWords) {
			score+=computeOneUnigramComponent(vDocno, connection, s, parameters, alphas);

		}

		return score;
	}
	
	/** Computes the value of the potential function for the title field of the unigram.
	 * */
	private static double computeOneUnigramComponent(int vID, Connection connection, String queryWord, 
			YosiDataWrapper parameters,
			Pair<Double, Double> alphas) {

		//**** 1 : compute the maximum likelihood estimator P(q_i, v*) for the title field
		Pair<Double, Double> unigramMLE = computeUnigramMaximumLikelihoodEstimators(vID, queryWord, parameters);

		//***** 3 : compute the smoothing values ****** //
		Pair<Double, Double> unigramSmooth = computeUnigramSmoothingValues(queryWord, parameters);

		//now we can compute the potential function for unigrams

		//title part
		//first get the values
		double titleAlpha = alphas.getLeft();
		double titleMLE = unigramMLE.getLeft();
		double titleSmooth = unigramSmooth.getLeft();
		//the computations
		double logArgument = ((1 - titleAlpha) * titleMLE) + ((titleAlpha)*titleSmooth);
		double titleLog = 0;
		if(logArgument>0)
			titleLog = Math.log(logArgument);

		//content part
		double contentAlpha = alphas.getRight();
		double contentMLE = unigramMLE.getRight();
		double contentSmooth = unigramSmooth.getRight();
		// the computations
		logArgument = ((1 - contentAlpha) * contentMLE) + ((contentAlpha)*contentSmooth);
		double contentLog = 0;
		if(logArgument>0)
			contentLog = Math.log(logArgument);

		//all together now
		double unigramContribution = parameters.getLambdaUnigramTitle() * titleLog + 
				parameters.getLambdaUnigramContent() * contentLog;
		return unigramContribution;

	}
	
	
	/** Here we compute the MLE for the title and content fields for unigram. 
	 * That is, we compute P(q_i | v*).
	 * 
	 * @param vID id of the node u that we are scoring.
	 * */
	private static Pair<Double, Double> computeUnigramMaximumLikelihoodEstimators(int vID, String queryWord, YosiDataWrapper parameters) {

		//get the parameters from the data wrapper
		Connection connection = parameters.getConnection();

		// ****** NUMERATOR ******** //
		//first, we need w_s(v*, v), because it will be used many times

		try {
			double vvScore = 0;

			PreparedStatement stmt = connection.prepareStatement(SQL_GET_STATIC_RELATIVE_WEIGHT);
			stmt.setInt(1, vID);
			stmt.setInt(2, vID);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}
			//now we have the value w_s(v*, v). We now can compute the numerator, wtf(q_i, v*)
			double titleNumerator = 0, contentNumerator = 0;

			//get all the nodes composing v*
			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, vID);
			rs = stmt.executeQuery();
			//iterate over the u in v*
			while(rs.next()) {
				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				//compute the gaussian weight to multiply with the tf
				double weight = MathUsefulMethods.computeGaussianWeight(vuScore, vvScore, parameters.getSigma());

				//now we find the term frequencies
				int tfTitle = 0, tfContent = 0;
				//get the content field of the node u from the yosi_node table in order to compute the frequency of q_i in it
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();
				if(fieldSet.next()) {
					//get the text corresponding to the title and content field
					//we index it in order to get the term frequency of the query word q_i
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");
					try {
						//index the title and check the frequency of the word
						MemoryIndex mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title_);
						Lexicon<String> mLex = mIndex.getLexicon();
						LexiconEntry mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfTitle = mLe.getFrequency();

						//index the content and get the frequency of the query word
						mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content_);
						mLex = mIndex.getLexicon();
						mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfContent = mLe.getFrequency();
						mIndex.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					//multiply with the term frequency for title and content
					//(the weight is the same)
					//weight * tf
					double elem = weight * tfTitle;
					titleNumerator = titleNumerator + elem;

					elem = weight * tfContent;
					contentNumerator = contentNumerator + elem;
				}//if field to check the presence of fields
			}//end iteration over the nodes u of v*. THe numerators have been calculated

			//now retrieving the denominators (lengths of v*)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, vID);
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 1.0, contentLength = 1.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("unigram_length_title");
				contentLength = lengthRs.getDouble("unigram_length_content");
			}

			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = (double) titleNumerator / titleLength;
			}
			if(contentLength!=0) {
				contentMLE = (double) contentNumerator / contentLength;
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	private static Pair<Double, Double> computeAlphasForNodesUnigram(int vDocno,
			int vDocid,
			YosiDataWrapper parameters) {
		Index virtualDocIndex = parameters.getUnigramVirtualDocumentsIndex();
		double[] averageFieldsLength = 
				virtualDocIndex.getCollectionStatistics()
				.getAverageFieldLengths();

		double muTitle = averageFieldsLength[0];
		double muContent = averageFieldsLength[1];

		double xTitle = 0, xContent = 0;

		//get the document connected to the node v
		Connection connection = parameters.getConnection();
		try {
			//we need to rebuild the document v*
			/*NB: this act is necessary because vID is the docno of the document,
			 * but Terrier only permits to access the information of the document
			 * if we have the docid. I need to rewrite the code using the docid.
			 * */
			PreparedStatement nodesPS = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			nodesPS.setInt(1, vDocno);
			ResultSet nodesRS = nodesPS.executeQuery();
			String title_doc = "", content_doc = "";
			while(nodesRS.next()) {
				int uID = nodesRS.getInt("u_id");
				//get the fields of the node in order to recreate the document
				PreparedStatement fieldsPS = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldsPS.setInt(1, uID);
				ResultSet fieldRS = fieldsPS.executeQuery();
				if(fieldRS.next()) {
					title_doc = title_doc + " " + fieldRS.getString("title_");
					content_doc = content_doc + " " + fieldRS.getString("content_");
				}
			}
			
			//now we take the length of the fields
			MemoryIndex titleIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title_doc);
			MemoryIndex contentIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content_doc);
			xTitle = titleIndex.getCollectionStatistics().getAverageDocumentLength();
			xContent = contentIndex.getCollectionStatistics().getAverageDocumentLength();
			titleIndex.close(); contentIndex.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		double tAlpha = (double) muTitle / (muTitle + xTitle);
		double cAlpha = (double) muContent / (muContent + xContent);

		Pair<Double, Double> alphaPair = new MutablePair<Double, Double>(tAlpha, cAlpha);
		return alphaPair;
	}
	
	private static Pair<Double, Double> computeUnigramSmoothingValues(String queryWord, YosiDataWrapper parameters) {
		//compute the total number of words in the collection for both fields (denominators)
		Index nodeIndex = parameters.getUnigramNodeIndex();
		long[] denominators = nodeIndex.getCollectionStatistics().getFieldTokens();
		long titleDenominator = denominators[0];
		long contentDenominator = denominators[1];

		//get the numerator for both fields, that total frequency of the query word in the collection
		Lexicon<String> lex = nodeIndex.getLexicon();
		FieldLexiconEntry fLe = (FieldLexiconEntry)lex.getLexiconEntry(queryWord);
		int[] frequencies = fLe.getFieldFrequencies();
		int titleNumerator = frequencies[0];
		int contentNumerator = frequencies[1];

		double tVal = (double) titleNumerator / titleDenominator;
		double cVal = (double) contentNumerator / contentDenominator;

		Pair<Double, Double> pair = new MutablePair<Double, Double>(tVal, cVal);
		return pair;
	}
	
	/**Computes the bigram component of the score(Q, v) function.
	 * */
	private static double computeBigramComponents(int vID, YosiDataWrapper parameters) {
		//the returning score
		double score = 0;
		
		//extrapolates all the possible bigrams from the query
		String query = parameters.getQuery();
		List<Bigram> bigramList = BigramUsefulMethods.extractDistinctBigramsFromStringWithPipeline(query);
		Pair<Double, Double> alphas = computeAlphasForBigrams(vID, parameters);
		
		for(Bigram queryBigram : bigramList) {
			//sum over all bigrams
			score += computeOneBigramComponent(vID, queryBigram, parameters, alphas);
		}
		return score;
	}
	
	/** Computes the bigram contribution to the sum for one bigram*/
	private static double computeOneBigramComponent(int vID, Bigram queryBigram, YosiDataWrapper parameters,
			Pair<Double, Double> alphas) {
		//first, compute the MLE
		Pair<Double, Double> bigramMLE = computeBigramMaximumLikelihoodEstimators(vID, queryBigram, parameters);

		//compute the alphas
//		Pair<Double, Double> alphas = computeAlphasForBigrams(vID, parameters);

		//compute the Dirichlet smoothing values
		Pair<Double, Double> bigramSmooth = computeBigramSmoothingValues(queryBigram, parameters);
		
		//title part
		double titleAlpha = alphas.getLeft();
		double titleMLE = bigramMLE.getLeft();
		double titleSmooth = bigramSmooth.getLeft();
		
		//the computations
		double logArgument = ((1 - titleAlpha) * titleMLE) + ((titleAlpha)*titleSmooth);
		double titleLog = /*-Double.MAX_VALUE*/ 0;
		if(logArgument > 0)
			titleLog = Math.log(logArgument);
		
		//content part
		double contentAlpha = alphas.getRight();
		double contentMLE = bigramMLE.getRight();
		double contentSmooth = bigramSmooth.getRight();
		// the computations
		logArgument = ((1 - contentAlpha) * contentMLE) + ((contentAlpha)*contentSmooth);
		double contentLog = /*-Double.MAX_VALUE*/ 0;
		if(logArgument > 0)
			contentLog = Math.log(logArgument);
		
		//all together now
		double unigramContribution = parameters.getLambdaUnigramTitle() * titleLog + 
				parameters.getLambdaUnigramContent() * contentLog;
		return unigramContribution;
	}
	
	/** Computes the Dirichlet values using to do smoothing. In particular, computes
	 * P(q_i, q_i+1 | V) for both the title and content fields for bigrams. 
	 * */
	private static Pair<Double, Double> computeBigramSmoothingValues(Bigram queryBigram, YosiDataWrapper parameters) {
		//get the index of bigrams for the virtual documents
		Index bigramNodeIndex = parameters.getBigramNodeIndex();
		
		//get the denominators, the total number of tokens in the collection for each field
		long[] denominators = bigramNodeIndex.getCollectionStatistics().getFieldTokens();
		long titleDenominator = denominators[0];
		long contentDenominator = denominators[1];
		
		//get the numerator
		Lexicon<String> lex = bigramNodeIndex.getLexicon();
		FieldLexiconEntry fLE = (FieldLexiconEntry) lex.getLexiconEntry(queryBigram.getStandardExpression());
		int[] frequencies = {0, 0};
		if(fLE != null)
			frequencies = fLE.getFieldFrequencies();
		int titleNumerator = frequencies[0];
		int contentNumerator = frequencies[1];
		
		double tVal = (double) titleNumerator / titleDenominator;
		double cVal = (double) contentNumerator / contentDenominator;

		Pair<Double, Double> pair = new MutablePair<Double, Double>(tVal, cVal);
		return pair;
	}
	
	/** Computes the Dirichlet smoothing parameters alphas, as described by Yosi 2016.
	 * */
	private static Pair<Double, Double> computeAlphasForBigrams(int vID, YosiDataWrapper parameters) {
		//get the bigram index
		Index bigramDocumetIndex = parameters.getBigramVirtualDocumentsIndex();
		
		//get the mu of the two fields
		double[] averageFieldsLength = bigramDocumetIndex.getCollectionStatistics().getAverageFieldLengths();
		double muTitle = averageFieldsLength[0];
		double muContent = averageFieldsLength[1];
		
		//definition of the length of the current document v
		double xTitle = 0, xContent = 0;
		
		//now we need to re-create the document v* and index it with bigrams in order to get the length
		Connection connection = parameters.getConnection();
		try {
			PreparedStatement nodesPS = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			nodesPS.setInt(1, vID);
			ResultSet nodesRS = nodesPS.executeQuery();
			String title_doc = "", content_doc = "";
			while(nodesRS.next()) {
				//get the id of the node in v*
				int uID = nodesRS.getInt("u_id");
				//get the fields of this node in order to recreate the document
				PreparedStatement fieldPS = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldPS.setInt(1, uID);
				ResultSet fieldRS = fieldPS.executeQuery();
				if(fieldRS.next()) {
					title_doc = title_doc + " " + fieldRS.getString("title_");
					content_doc = content_doc + " " + fieldRS.getString("content_");
				}
			}
			//if we are here, we have the complete document
			//get the index on the bigrams of the documents
			ApplicationSetup.setProperty("tokeniser", "BigramTokeniser");
			MemoryIndex titleIndex = TerrierUsefulMethods.getBigramMemoryIndexFromDocument(title_doc);
			MemoryIndex contentIndex = TerrierUsefulMethods.getBigramMemoryIndexFromDocument(content_doc);
			xTitle = titleIndex.getCollectionStatistics().getAverageDocumentLength();
			xContent = contentIndex.getCollectionStatistics().getAverageDocumentLength(); 
			titleIndex.close(); contentIndex.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		double titleAlpha = 0;
		if(!(muTitle ==0  && xTitle == 0)) {
			//we need to check that we don't have 0 at the denominator
			titleAlpha = (double) muTitle / (muTitle + xTitle);
		}		
		double contentAlpha = 0;
		if(!(muContent ==0  && xContent == 0)) {
			//we need to check that we don't have 0 at the denominator
			contentAlpha = (double) muContent / (muContent + xContent);
		}
		
		Pair<Double, Double> alphaPair = new MutablePair<Double, Double>(titleAlpha, contentAlpha);
		return alphaPair;
	}
	
	/** Computes the MLE for the title and the content field considering bigrams.
	 * */
	private static Pair<Double, Double> computeBigramMaximumLikelihoodEstimators(int vID, Bigram queryBigram, 
			YosiDataWrapper parameters) {

		Connection connection = parameters.getConnection();
		double sigma = parameters.getSigma();

		try {
			//computation of the numerator of the MLE
			//first take the relative static weight of w_s(v*, v) to be used a lot later
			PreparedStatement stmt = connection.prepareStatement(SQL_GET_STATIC_RELATIVE_WEIGHT);
			stmt.setInt(1, vID);
			stmt.setInt(2, vID);
			ResultSet rs = stmt.executeQuery();

			double vvScore = 0;
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}

			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, vID);
			rs = stmt.executeQuery();
			double titleNumerator = 0, contentNumerator = 0;
			while(rs.next()) {
				//sum over all the nodes u in v*

				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				double weight = MathUsefulMethods.computeGaussianWeight(vuScore, vvScore, sigma);

				//get the title and content field in order to compute the bigram frequency
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();

				//compute the term frequency
				int titleTf = 0, contentTf = 0;
				if(fieldSet.next()) {
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");

					//get bigrams from the title
					List<Bigram> titleBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(title_);
					//get the bigram frequency
					titleTf = BigramUsefulMethods.getBigramFrequency(queryBigram, titleBigramList);

					//get bigrams from the content
					List<Bigram> contentBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(content_);
					//get the bigram frequency
					contentTf = BigramUsefulMethods.getBigramFrequency(queryBigram, contentBigramList);
				}
				//update the score of the title
				titleNumerator += weight * titleTf;
				contentNumerator += weight * contentTf;
			}

			//at the end of this WHILE, we have the numerator of the MLE. Now we get the length (denominator)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, vID);
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 0.0, contentLength = 0.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("bigram_length_title");
				contentLength = lengthRs.getDouble("bigram_length_content");
			}

			//now we compute the MLEs
			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = (double) (titleNumerator / titleLength);
			}
			if(contentLength!=0) {
				contentMLE = (double) (contentNumerator / contentLength);
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/** Computes the query independent component of the scoring function. This component is composed by 
	 * the natural logarithm of the ration between the degree of the node v and the total degree of the
	 * graph.
	 * 
	 * */
	private static double computeQueryIndependentPotentialFunction(int vID, YosiDataWrapper parameters) {
		long totalDegree = parameters.getTotalDegree();
		double lambdaQueryIndependent = parameters.getLambdaQueryIndependent();

		//get the numerator
		Connection connection = parameters.getConnection();
		String SQL_GED_NODE_DEGREE = "SELECT degree_ from " + DatabaseState.getSchema() +".yosi_node where id_ = ?";
		try {
			PreparedStatement ps = connection.prepareStatement(SQL_GED_NODE_DEGREE);
			ps.setInt(1, vID);

			ResultSet degreeRS = ps.executeQuery();
			int degree = 0;
			if(degreeRS.next()) {
				degree = degreeRS.getInt(1);
			}

			//now we compute the fraction
			double argument = (double) degree / totalDegree;
			if(argument > 0)
				return lambdaQueryIndependent * Math.log(argument);
			else
				return /*-Double.MAX_VALUE*/ 0;


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}



}
