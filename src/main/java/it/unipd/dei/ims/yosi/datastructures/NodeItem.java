package it.unipd.dei.ims.yosi.datastructures;

/** Object representing a Node with its score. Useful when we 
 * want to rank them.
 * */
public class NodeItem {

	/** Id of the node*/
	private String id_;
	
	/** Score of the node */
	private double score;
	
	public NodeItem(String id) {
		id_ = id;
	}

	//######################
	
	public String getId_() {
		return id_;
	}

	public void setId_(String id_) {
		this.id_ = id_;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
}
