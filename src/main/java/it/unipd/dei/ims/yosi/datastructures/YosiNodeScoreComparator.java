package it.unipd.dei.ims.yosi.datastructures;

import java.util.Comparator;

/** Comparator to obtain a max heap comparing on the score
 * field of the YosiNode
 * */
public class YosiNodeScoreComparator implements Comparator<YosiNode>{
	
	public int compare(YosiNode o1, YosiNode o2) {
		double score1 = o1.getScore();
		double score2 = o2.getScore();
		
		if(score1<score2)
			return 1;
		else if(score1>score2)
			return -1;
		return 0;
	}

}
