package it.unipd.dei.ims.yosi.offline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;

import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.utilities.StringUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.UsefulConstants;

/** Yosi algorithm: phase 4
 * <p>
 * Creation of the virtual documents as text from the virtual graphs v*
 * and indexing.
 * */
public class FromGraphsToFieldTRECDocumentsPhase {

	/** Directory where to find the virtual graphs v*.
	 * */
	private String mainDirectory;

	/** Directory where to write the output virtual documents
	 * */
	private String outputDirectory;

	private int fileCounter;
	
	/** Path of the directory where to store the index 
	 * of the virtual documents in unigrams
	 * */
	private String virtualDocumentUnigramIndexPath;
	
	/** Path of the directory where to store the index 
	 * of the virtual documents in bigrams
	 * */
	private String virtualDocumentBigramIndexPath;


	public FromGraphsToFieldTRECDocumentsPhase() {
		fileCounter = 0;

		this.setup();

	}

	private  void setup() {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
			this.mainDirectory = map.get("virtual.graphs.directory.path");
			this.outputDirectory = map.get("virtual.documents.directory.path");
			this.virtualDocumentUnigramIndexPath = map.get("virtual.documents.unigram.index.path");
			this.virtualDocumentBigramIndexPath = map.get("virtual.documents.bigram.index.path");
			
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Creates TREC documents from the turlte ones and 
	 * indexes them.
	 * */
	public void fromGraphsTOFieldTRECDocuments () {
		if(Thread.interrupted()) {
			ThreadState.setOffLine(false);
			return;
		}
		
		this.fromGraphsToFieldTRECDocumentsConvertion();
		
		if(Thread.interrupted()) {
			ThreadState.setOffLine(false);
			return;
		}
		
		this.indexTheDocuments();
	}
	
	private void indexTheDocuments() {
		
		IndexerDirectoryOfTRECFiles indexer = new IndexerDirectoryOfTRECFiles();
		//set directory of the documents to index
		indexer.setDirectoryToIndex(this.outputDirectory);
		
		indexer.setIndexPath(this.virtualDocumentUnigramIndexPath);
		indexer.index("unigram");
		
		indexer.setIndexPath(this.virtualDocumentBigramIndexPath);
		indexer.index("bigram");
	}
	
	

	private void fromGraphsToFieldTRECDocumentsConvertion() {
		//file representing the output directory
		File outputDirFile = new File(outputDirectory);
		//in case, create the directory
		if(!outputDirFile.exists()) {
			outputDirFile.mkdirs();
		}

		//in case, clean the directory
		try {
			FileUtils.cleanDirectory(outputDirFile);
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//a map to keep track of the paths in order to write a bunch of graphs
		//inside the same TREC file
		Map<String, String> pathMap = new HashMap<String, String>();

		//open the main directory
		File mainDir = new File(mainDirectory);
		if(! mainDir.isDirectory()) {
			throw new IllegalArgumentException("provied path "+ mainDirectory + " is not a directory");
		}

		//queue to keep track of the directories we still have to visit
		Queue<File> directoryQueue = new LinkedList<File>();
		directoryQueue.add(mainDir);

		//loop over all the files inside the main directory
		while(!directoryQueue.isEmpty()) {
			File f = directoryQueue.remove();
			File[] files = f.listFiles();
			for (File file : files) {
				if(file.isDirectory()) {
					//add the directory to the ones we have to visit
					directoryQueue.add(file);
				}
				else {
					//it is a rightful file, add to the map
					String path = file.getAbsolutePath();
					String name = file.getName();
					if(name.equals(".DS_Store"))
						continue;
					String docId = "";
					try {
						docId = StringUsefulMethods.getIdFromFile(file);
					} catch(Exception e) {
						System.err.println("Error in the regular expression");
					}
					pathMap.put(docId, path);
				}
			}
			
			if(pathMap.size() >= 2048) {
				try {
					fileCounter++;
					convertAllFilesInTheMap(pathMap, outputDirectory, fileCounter);
					//clear the map, saving memory
					pathMap.clear();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (RDFParseException e) {
					e.printStackTrace();
				} catch (RDFHandlerException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}//end of while

		if(pathMap.size() > 0) {
			try {
				fileCounter++;
				convertAllFilesInTheMap(pathMap, outputDirectory, fileCounter);
				//clear the map, saving memory
				pathMap.clear();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (RDFParseException e) {
				e.printStackTrace();
			} catch (RDFHandlerException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void convertAllFilesInTheMap(Map<String, String> pathMap, String outputDirectory, int fileCounter) 
			throws RDFParseException, RDFHandlerException, IOException {
		//name of the output file
		String outputFile = outputDirectory + "/" + fileCounter + ".trec";
		System.out.println("printing the file " + outputFile);
		Path outputPath = Paths.get(outputFile);
		BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);

		//for every entry in the map, we have a graph to convert and write inside the TREC file
		for(Entry<String, String> entry : pathMap.entrySet()) {
			
			if(Thread.interrupted()) {
				writer.close();
				ThreadState.setOffLine(false);
				return;
			}
			
			String path = entry.getValue();
			String id = entry.getKey();

			//now read the file
			//open the input stream to the file
			InputStream inputStream = new FileInputStream(new File(path));
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();

			//print the graph in TREC format
			dealWithAGraph(statements, id, writer);

			inputStream.close();
		}
		writer.close();
	}

	private static void dealWithAGraph(Collection<org.openrdf.model.Statement> statements, String id, BufferedWriter writer) throws IOException {
		//write the file
		writer.write("<DOC>");
		writer.newLine();

		writer.write("\t<DOCNO>" + id + "</DOCNO>");
		writer.newLine();
		writeADocument(writer, statements);
		writer.newLine();

		writer.write("</DOC>");
		writer.newLine();
		writer.flush();
	}

	private static void writeADocument(BufferedWriter writer, Collection<org.openrdf.model.Statement> graph) throws IOException {
		//title and content field
		String title = "", content = "";
		//list to keep track of the nodes already visited
		List<String> urlChecklist = new ArrayList<String>();

		//for each triple in the graph
		for(org.openrdf.model.Statement t : graph) {
			//take the subject of this triple
			Resource subject = t.getSubject();

			//we need to check that we don't count more than once a subject node
			//that previously was an object node
			if(!urlChecklist.contains(subject.toString())) {
				urlChecklist.add(subject.toString());
				//take the string from the URL
				String sub = UrlUtilities.takeWordsFromIri(subject.stringValue()); 
				//add the subject to the title and the content 
				title = title + " " + sub;
				content = content + " " + sub;
			}

			//now we deal with predicate and object
			URI predicate = t.getPredicate();
			//add the uri to the content
			String pred = UrlUtilities.takeWordsFromIri(predicate.stringValue());

			//add only to the content field
			content = content + " " + pred;

			//nw deal with the obj
			Value object = t.getObject();
			if(object instanceof Literal) {
				//easily add to the content field
				content = content + " " + object.stringValue();
			} else {
				//it is a URI object, and maybe (but not necessarily) subject of other triples
				String obj = UrlUtilities.takeWordsFromIri(object.stringValue());
				//add to the title
				title = title + " " + obj;
				//add to the content
				content = content + " " + obj;
				//add to the checklist
				urlChecklist.add(object.stringValue());

			}

		}
		//write down
		writer.write("\t<TITLE>"+title.trim()+"</TITLE>");
		writer.newLine();
		writer.write("\t<CONTENT>"+content.trim()+"</CONTENT>");
	}

	public String getMainDirectory() {
		return mainDirectory;
	}

	public void setMainDirectory(String mainDirectory) {
		this.mainDirectory = mainDirectory;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getVirtualDocumentUnigramIndexPath() {
		return virtualDocumentUnigramIndexPath;
	}

	public void setVirtualDocumentUnigramIndexPath(String virtualDocumentUnigramIndexPath) {
		this.virtualDocumentUnigramIndexPath = virtualDocumentUnigramIndexPath;
		File f = new File(virtualDocumentUnigramIndexPath);
		f.mkdirs();
	}

	public String getVirtualDocumentBigramIndexPath() {
		return virtualDocumentBigramIndexPath;
	}

	public void setVirtualDocumentBigramIndexPath(String virtualDocumentBigramIndexPath) {
		this.virtualDocumentBigramIndexPath = virtualDocumentBigramIndexPath;
		File f = new File(virtualDocumentBigramIndexPath);
		f.mkdirs();
	}
	
}
