package it.unipd.dei.ims.yosi.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.yosi.datastructures.DijkstraExecution;
import it.unipd.dei.ims.yosi.datastructures.YosiComparator;
import it.unipd.dei.ims.yosi.datastructures.YosiGraph;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;
import it.unipd.dei.ims.yosi.datastructures.YosiPath;
import it.unipd.dei.ims.yosi.datastructures.YosiPathComparatorByWeight;
import it.unipd.dei.ims.yosi.offline.IndexerDirectoryOfTRECFiles;

/** Yosi Algorithm: phase 7 (online).
 * 
 * <p>
 * NB: as of now, we are printing the whole subgraph, without keeping only the 
 * most weighted paths.
 * */
public class AnswerSubgraphGenerationPhase {

	private static final String SQL_GET_VIRTUAL_DOCUMENT_NODES = "select v_id, u_id, ws_ "
			+ "from " + DatabaseState.getSchema() + ".yosi_virtual_document where v_id=?";

	private static final String SQL_GET_NEIGHBORS = "SELECT subject_, predicate_, object_ "
			+ "from " + DatabaseState.getSchema() +  ".triple_store where subject_ = ?";

	private static final String SQL_GET_NODE_IRI = "SELECT iri from " + DatabaseState.getSchema() + 
			".yosi_node where id_=?";

	private String keywordNodesFilePath;

	private String rootNodesFilePath;

	private String jdbcConnectionString;

	private String supportDirectoryPath;

	private Connection connection;

	/** Path of the directory where to save the answers as file TREC.
	 * */
	private String answerTRECDirectoryPath;

	/** Directory where to save the unigram index of the answers 
	 * */
	private String answersUnigramIndexDirectoryPath;

	/** Directory for the bigram index of the answers documents
	 * */
	private String answersBigramIndexDirectoryPath;

	private double maxScore;

	/** Path of the directory where to save the answers as file 
	 * RDF in format turtle (.ttl)*/
	private String answerGraphsDirectoryPath;

	public AnswerSubgraphGenerationPhase() {
		this.setup();
	}

	private void setup() {
		try {
			Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			//setting system properties for terrier to find the terrier_home and the /etc directory
			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);

			keywordNodesFilePath = map.get("keyword.nodes.file.path");

			rootNodesFilePath = map.get("root.nodes.file.path");

			jdbcConnectionString = map.get("jdbc.connection.string");

			this.supportDirectoryPath = map.get("support.directory.path");

			this.answerGraphsDirectoryPath = map.get("answer.graphs.directory.path");

			this.answerTRECDirectoryPath = map.get("answer.trec.directory.path");

			this.answersUnigramIndexDirectoryPath = map.get("answers.unigram.index.directory.path");

			this.answersBigramIndexDirectoryPath = map.get("answers.bigram.index.directory.path");


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Executes the generation of the answer graphs.
	 * */
	public void generateAnswerSubgraphs() {

		this.checkDirectories();

		connection = null;

		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
			//			connection = DriverManager.getConnection(this.jdbcConnectionString);

			List<YosiNode> rootList = this.readRootNodes(this.rootNodesFilePath, connection);
			Map<String, List<YosiNode>> keyNodesMap = this.readKeynodesFile(this.keywordNodesFilePath, connection);

			this.maxScore = this.findTheFriggingMaxScore(rootList, keyNodesMap);

			//generate both RDF and TREC files
			this.generateAnswers(rootList, keyNodesMap, connection, maxScore, 
					this.answerTRECDirectoryPath, this.answerGraphsDirectoryPath, this.supportDirectoryPath);

			//index the TREC files
			this.indexTheAnswers();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
					//					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	/** Check the existence of the directories we are going to use.
	 * In case, it creates them.
	 * */
	private void checkDirectories() {
		File f = new File(this.supportDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		f = new File(this.answerGraphsDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		f = new File(this.answerTRECDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		f = new File(this.answersUnigramIndexDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}

		f = new File(this.answersBigramIndexDirectoryPath);
		if(!f.exists()) {
			f.mkdirs();
		}


	}

	private void indexTheAnswers() {
		IndexerDirectoryOfTRECFiles indexer = new IndexerDirectoryOfTRECFiles(false);

		indexer.setDirectoryToIndex(this.answerTRECDirectoryPath);

		indexer.setIndexPath(this.answersUnigramIndexDirectoryPath);
		indexer.index("unigram");

		indexer.setIndexPath(this.answersBigramIndexDirectoryPath);
		indexer.index("bigram");
	}

	/** Reads from a provided xml file the root nodes with their scores and returns a 
	 * list of YosiNode. These nodes will contain the information about
	 * id, score and iri.
	 * <p>
	 * The structure of the file must be like this:
	 * <xmp>
	 <roots>
		<root score="-4.839269576876499">57618</root>
		<root score="-7.590919897092794">3896</root>
		...
	</roots>
	 * </xmp>
	 * 
	 * @param inputXmlRootsPath the path of the xml file
	 * @throws SQLException */
	private List<YosiNode> readRootNodes(String inputXmlRootsPath, Connection connection) throws SQLException {
		try {
			//this map contains the couples keyword and the list of IDs corresponding to them
			List<YosiNode> rootsList = new ArrayList<YosiNode>();

			//open the xml file
			File f = new File(inputXmlRootsPath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			doc.getDocumentElement().normalize();

			//parse the elements, take the nodes
			NodeList queryWordsList = doc.getElementsByTagName("root");
			for(int i = 0; i < queryWordsList.getLength(); ++i) {
				//for every root
				Node n = queryWordsList.item(i);
				Element node = (Element)n;
				//get the id of the root and the score 
				String id = node.getTextContent();
				String score = node.getAttribute("score");

				//save them in a YosiNode object
				YosiNode keyNode = new YosiNode();
				keyNode.setId_(Integer.parseInt(id));
				keyNode.setScore(Double.parseDouble(score));

				//get the iri of the node
				PreparedStatement ps = connection.prepareStatement(SQL_GET_NODE_IRI);
				ps.setInt(1, Integer.parseInt(id));
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					keyNode.setStringContent(rs.getString("iri"));
					keyNode.setIri(rs.getString("iri"));
					keyNode.setStringContent(rs.getString("iri"));
				}

				//add the root the the list of roots
				rootsList.add(keyNode);

			}//end of the cicle on every root node
			//now we can return the list
			return rootsList;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}


	/** Reads from an xml file the keyword nodes. The file must be of this kind:
	 * <xmp>
	   <keynodes>
		<node queryword="EXAMPLE_keyword" score="-4.67279572193965">3896</node>
		<node queryword="your_keyword_here" score="-4.673345303874964">57618</node>
		...
	   </keynodes>

	 * </xmp>
	 * <p>
	 * The returning value is a map with a list of nodes divided by the keyword
	 *  @param inputXmlPath the path to a file XML constructed with the sorted list of IDs of
	 *  keyword nodes with the respecitve keyword they contain.
	 * @throws SQLException 
	 *  
	 *  */
	private Map<String, List<YosiNode>> readKeynodesFile(String inputXmlPath, Connection connection) throws SQLException {
		try {
			//this map contains the couples keyword and the list of IDs corresponding to them
			Map<String, List<YosiNode>> keyNodesMap = new HashMap<String, List<YosiNode>>();

			//open the xml file
			File f = new File(inputXmlPath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(f);
			doc.getDocumentElement().normalize();

			//parse the elements, take the nodes
			NodeList queryWordsList = doc.getElementsByTagName("node");
			for(int i = 0; i < queryWordsList.getLength(); ++i) {
				//for every node
				Node n = queryWordsList.item(i);
				Element node = (Element)n;
				//get the id of the node, the score and the keyword contained by the node.
				String id = node.getTextContent();
				String score = node.getAttribute("score");
				String keyword = node.getAttribute("queryword");

				//save them in a YosiNode object
				YosiNode keyNode = new YosiNode();
				keyNode.setId_(Integer.parseInt(id));
				keyNode.setScore(Double.parseDouble(score));
				keyNode.setKeyword(keyword);

				//get the iri of the node
				PreparedStatement ps = connection.prepareStatement(SQL_GET_NODE_IRI);
				ps.setInt(1, Integer.parseInt(id));
				ResultSet rs = ps.executeQuery();
				if(rs.next()) {
					keyNode.setStringContent(rs.getString("iri"));
					keyNode.setIri(rs.getString("iri"));
					keyNode.setStringContent(rs.getString("iri"));
				}

				//insert the node in the correct list
				if(keyNodesMap.containsKey(keyword)) {//the absence of '!' before the condition is correct
					//add the node to the ordered list
					keyNodesMap.get(keyword).add(keyNode);
				} else {
					//we have met a new keyword
					//create a new list, add the node to the list and add the list to the map of lists
					List<YosiNode> keyNodes = new ArrayList<YosiNode>();
					keyNodes.add(keyNode);
					keyNodesMap.put(keyword, keyNodes);
				}
			}//end of the cicle on every keynode
			//now we can return the map
			return keyNodesMap;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}


	/** Given the data structures in the firm (these are given by the previous implementation, don't ask)
	 * finds the maximum score among all of them. 'All of them' is the set S, composed by all the selected
	 * roots and selected keyword nodes, computed in the previous phases of the algorithm and provided with these 
	 * data structures. 
	 * 
	 * @param rootList list with YosiNode objects representing the roots with their scores.
	 * @param keyNodesMap a map containing the sets U_i represented by the corresponding query word q_i.
	 *  */
	private double findTheFriggingMaxScore(List<YosiNode> rootList, Map<String, List<YosiNode>> keyNodesMap) {
		//the scores are negative. We want the max of them, the closest to 0
		double score = -Double.MAX_VALUE;
		//check first among the roots
		for(YosiNode node : rootList) {
			if(node.getScore() > score) {
				score = node.getScore();
			}
		}
		//then check among the keynodes
		for(Entry<String, List<YosiNode>> entry : keyNodesMap.entrySet()) {
			List<YosiNode> keynodesList = entry.getValue();
			for(YosiNode node : keynodesList) {
				if(node.getScore() > score) {
					score = node.getScore();
				}
			}
		}
		return score;
	}

	/** Generates the answer grapjs.
	 * <p>
	 * NB: as to create meaningful answers, we decided here to consider as a graph the whole creation
	 * of the virtual graph v* with all the keyword nodes in its proximity.
	 * The algorithm by Yosi et al. 2016 describes the ranking method which considers only the
	 * most prominent paths. We use that method to rank the graphs, 
	 * but decide to keep the whole graph as a solution.
	 * 
	 * @param maxScore the maximum score of all nodes in S. Necessary to compute the dynamic weights.
	 * @param dirPath where to save the answers as TREC documents
	 * @param graphDirPath where to save the answers as graphs in turtle format
	 * */
	public void generateAnswers(List<YosiNode> rootList, 
			Map<String, List<YosiNode>> keyNodesMap, 
			Connection connection,
			double maxScore,
			String dirPath,
			String graphDirPath,
			String supportDirPath) {
		int counter = 1;

		//clean the directory
		try {
			FileUtils.cleanDirectory(new File(dirPath));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("unable to clean the directory");
		}

		//get the total degree of the graph. It will be useful later.
		int totalDegree = this.getTotalDegree(connection);

		Path outP = Paths.get(supportDirPath + "/static_scores.xml");
		BufferedWriter staticWeightWriter;
		try {
			staticWeightWriter = Files.newBufferedWriter(outP, UsefulConstants.CHARSET_ENCODING);

			staticWeightWriter.write("<scores>");
			staticWeightWriter.newLine();
			for(YosiNode root : rootList) {
				//the name of the file will have the id of the root
				String newDocPath = dirPath + "/" + root.getId_() + ".trec";
				String newGraphPath = graphDirPath + "/" + root.getId_() + ".ttl";
				generateOneAnswer(root, 
						keyNodesMap, 
						connection, 
						maxScore, 
						newDocPath, 
						newGraphPath, 
						counter, totalDegree,
						staticWeightWriter);
				counter++;
			}

			staticWeightWriter.write("</scores>");
			staticWeightWriter.close();

		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	/** Get the total degree of the graph from the RDB database 
	 * with a SUM query.
	 * */
	private int getTotalDegree(Connection connection) {
		String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from " + DatabaseState.getSchema() + ".yosi_node";
		PreparedStatement totalDegreeStatement;
		int totalDegree = 0;
		try {
			totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet rs = totalDegreeStatement.executeQuery();
			if(rs.next())
				totalDegree = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return totalDegree;
	}

	/** Given a root node, generates the answer from that root
	 * following the Yosi algorithm. 
	 * <p>
	 * The method also prints the static weight of the answer graph in a xml file
	 * and the graph itself as a turtle file.
	 * 
	 * @param keyNodesMap map with the lists of the nodes representing the sets U_ri
	 * @param path path where to save the document graph.
	 * @param newGraphPath path where to save the document as file .ttl (turtle) with Blazegraph
	 * @param counter an int which will be used as ID of the document when we print it
	 * */
	private void generateOneAnswer(YosiNode root, 
			Map<String, List<YosiNode>> keyNodesMap, 
			Connection connection,
			double maxScore, 
			String newDocPath, 
			String newGraphPath,
			int counter,
			int totalDegree,
			BufferedWriter qiWriter) {
		//obtain the nodes in r*
		List<Integer> rNodes = new ArrayList<Integer>();

		try {
			PreparedStatement neighborsPS = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			neighborsPS.setInt(1, root.getId_());
			ResultSet vdRs = neighborsPS.executeQuery();

			//get all the nodes inside r* and insert them in the list rNodes
			while(vdRs.next()) {
				int uID = vdRs.getInt("u_id");
				rNodes.add(uID);
			}
			Map<String, List<YosiNode>> includedKeywordNodes = new HashMap<String, List<YosiNode>>();

			//now in the documentNodes list we have all the nodes in r*. 
			//we intersect them with the nodes in each U_i to obtain the U_ri
			//first we collect all the keynodes in every U_i
			for(Entry<String, List<YosiNode>> entry : keyNodesMap.entrySet()) {
				//the query word
				String q_i = entry.getKey();
				//for each set U_i
				List<YosiNode> U_i = entry.getValue();
				//create a new set with the nodes in r* which are also in U_i
				List<YosiNode> U_ir = this.intersectionBetweenNodeListAndIDList(rNodes, U_i);
				//add to the list of keyword nodes v inside r*
				includedKeywordNodes.put(q_i, U_ir);
			}

			//now we have the map with the U_ir for the radix r
			//we can start creating the document
			DijkstraExecution execution = 
					performDijkstraAlgorithm(root, includedKeywordNodes, connection, maxScore, 3);
			
			//print the result as graph (necessary later for evaluation)
			//create on the go the path
			File f = new File(newGraphPath);
			String name = f.getName();
			File g = new File(f.getParentFile().getParent() + "/extended_answer_graphs");
			if(!g.exists())
				g.mkdirs();
			String bigGraphPath = f.getParentFile().getParent() + "/extended_answer_graphs/" + name;
			BlazegraphUsefulMethods.printTheDamnGraph(execution.getCluster(), bigGraphPath);
			//score the paths
			Map<String, PriorityQueue<YosiPath>> keywordsMap = this.rankThePaths(execution);
			this.buildAndPrintTheAnswer(keywordsMap, newDocPath, newGraphPath, root, totalDegree, qiWriter);



		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/** Using the priority queues with the paths inside the answer, creates the best answer and 
	 * prints it. It also generates the query independent score for the final answer ranking function.
	 * 
	 * */
	private void buildAndPrintTheAnswer(Map<String, PriorityQueue<YosiPath>> keywordsMap, 
			String newDocPath,
			String newGraphPath,
			YosiNode root, int totalDegree, BufferedWriter qiWriter) {

		//answer subgraph that we are building
		Model subgraph = new TreeModel();
		//path where to write the subgraph as a document
		Path outputPath = Paths.get(newDocPath);
		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)) {
			//open the TREC document
			writer.write("<DOC>");
			writer.newLine();

			String totalTitle_ = "", totalContent_ = "";
			//list of YosiPaths already used, in case of duplicates
			List<YosiPath> alreadyUsedPaths = new ArrayList<YosiPath>();

			for(Entry<String, PriorityQueue<YosiPath>> entry : keywordsMap.entrySet()) {
				//for each query word, we take the first path in the priority queue
				YosiPath yosiPath = entry.getValue().poll();
				while(/*YosiPath.pathContainedInList(alreadyUsedPaths, yosiPath)*/   alreadyUsedPaths.contains(yosiPath)) {
					//if we already used this path, we search for new paths
					yosiPath = entry.getValue().poll();
				}
				//if we are here, we found a path that can be used
				if(yosiPath==null)
					//check to be sure
					continue;

				//now that we have the nodes of the path, we can use them to write the answer
				totalTitle_ = totalTitle_ + " " + yosiPath.getTitleFieldOfThePath();
				totalContent_ = totalContent_ + " " + yosiPath.getContentFieldOfThePath();
				alreadyUsedPaths.add(yosiPath);
				subgraph.addAll(yosiPath.getCorrespondingSubgraph());
			}//end of the for cycle. We have found all the paths

			writer.write("\t<DOCNO>"+root.getId_()+"</DOCNO>");
			writer.newLine();
			writer.write("\t<TITLE>"+totalTitle_ + "</TITLE>");
			writer.newLine();
			writer.write("\t<CONTENT>"+totalContent_ + "</CONTENT>");
			writer.newLine();

			writer.write("</DOC>");
			writer.close();

			//now we write the document
			//XXX 
			BlazegraphUsefulMethods.printTheDamnGraph(subgraph, newGraphPath);

			//now we found the query independent score for this answer graph a.
			YosiGraph answerGraph = new YosiGraph(alreadyUsedPaths);
			double queryIndependentScore = answerGraph.computeQueryIndependentScore(totalDegree);
			qiWriter.write("\t<score nodeId=\"" + root.getId_() +"\">" + queryIndependentScore + "</score>");
			qiWriter.newLine();
			qiWriter.flush();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/** Given an object DijkstraExecution with the information
	 * about the completed execution of a Dijkstra run to create a tree from a root, 
	 * this methods ranks the various paths from the root to a keyword node present in the tree,
	 * and divides them following the query words that they cover.
	 * 
	 * */
	private Map<String, PriorityQueue<YosiPath>> rankThePaths(DijkstraExecution execution) {

		Map<String, YosiNode> leaves = execution.getLeaves();
		/*map that contains, for each query word
		 * the ordered list with the ordered paths.
		 * */
		Map<String, PriorityQueue<YosiPath>> keywordsPath = new HashMap<String, PriorityQueue<YosiPath>>();


		//create a path for each leaf
		for(Entry<String, YosiNode> leaf : leaves.entrySet()) {
			YosiPath path = new YosiPath();
			//create the path
			path.buildThePath(leaf.getValue());
			//compute the weight of the path
			path.computeAndSetTotalWeight();
			//now, for each keyword of the path
			List<String> leafKeywords = leaf.getValue().getKeywords();
			for(String queryWord : leafKeywords) {
				//if we already have processed the query word
				if(keywordsPath.containsKey(queryWord)) {
					PriorityQueue<YosiPath> pathQueue = keywordsPath.get(queryWord);
					//add the path
					pathQueue.add(path);
				} else {
					//create a new queue
					PriorityQueue<YosiPath> queue = new PriorityQueue<YosiPath>(new YosiPathComparatorByWeight());
					queue.add(path);
					keywordsPath.put(queryWord, queue);
				}
			}
		}

		//return the map 
		return keywordsPath;

	}

	/**
	 * 
	 * @param keywordNodes a map with the keyword nodes inside r*
	 * */
	private DijkstraExecution performDijkstraAlgorithm(YosiNode root, Map<String, 
			List<YosiNode>> keywordNodes, 
			Connection connection,
			double maxS,
			int tau) {
		//get all the vertices in U_r without division by query word
		Map<String, YosiNode> checklist = YosiNode.getMapOfDistinctNodesFromMap(keywordNodes);
		try {
			//retrieve missing information for the root
			root.setRadius(0);
			//set the distance to perform the Dijkstra
			double sc = this.computeDinamicScore(root);
			root.setDistance(sc);
			//set the weight to compute the weight of the path later
			root.setWeight(sc);

			//create min heap to perform Dijkstra
			PriorityQueue<YosiNode> queue = new PriorityQueue<YosiNode>(new YosiComparator());
			queue.add(root);

			//a map to keep track of the nodes inside our frontier (that is, the nodes
			//inside v*)
			Map<String, YosiNode> frontier = new HashMap<String, YosiNode>();

			//a list with the IDs to keep track of the discovered nodes outside our frontier (we are discovering them)
			Map<String, YosiNode> border = new HashMap<String, YosiNode>();

			border.put(root.getIri(), root);//the source will be the first node to enter our frontier

			//map of leaves in the tree (the key is their iri). We will use this to re-create the paths
			Map<String, YosiNode> leaves = new HashMap<String, YosiNode>();

			//the graph that we are going to build and then print
			Model cluster = new TreeModel ();

			while(!queue.isEmpty() && !checklist.isEmpty()) {
				//take the node who needs to go inside the frontier
				YosiNode v = queue.poll();
				String vIri = v.getIri();

				//this node is added to the frontier (we won't touch its distance anymore)
				frontier.put(vIri, v);
				//this node is removed from the borderNodes
				border.remove(vIri);

				if(checklist.containsKey(vIri)) {
					//found one keyword node remove it from the checklist
					v.setKeywords(checklist.get(vIri).getKeywords());
					checklist.remove(vIri);
					//add it to the leaves if it is not the root
					if(!vIri.equals(root.getIri())) {
						leaves.put(vIri, v);
					}
				}


				//get the triple of which this node is the object in order to add it to the building subgraph 
				Statement t = v.getTriple();
				if(t!=null) {
					//if t==null, this is the root node, so it has no previous node

					//add the triple to the building subgraph
					cluster.add(t);
				}

				/** Given the new node in the frontier,
				 * we get the information about its content and title
				 * We will use them to produce the answer 
				 * document once the exploration of the the document has been completed*/
				String SQL_VD_INTERROGATION = "select id_, title_, content_ from " + DatabaseState.getSchema() +
						".yosi_node where iri=?";
				PreparedStatement pfVd = connection.prepareStatement(SQL_VD_INTERROGATION);
				pfVd.setString(1, vIri);
				ResultSet rsVD = pfVd.executeQuery();
				//enrich these nodes
				if(rsVD.next()) {
					int id = rsVD.getInt("id_");
					String textVd = rsVD.getString("title_");
					String contentVd = rsVD.getString("content_");
					//set the title field and content field
					v.setId_(id);
					v.setTitleField(textVd);
					v.setContentField(contentVd);
				}

				int vRadius = v.getRadius();

				//take now the neighbors of this node to extend the neighborhood
				//take the neighbors
				PreparedStatement psNeighbors = connection.prepareStatement(SQL_GET_NEIGHBORS);
				psNeighbors.setString(1, vIri);
				ResultSet rs = psNeighbors.executeQuery();

				//now, for each neighbor
				while(rs.next()) {
					String predicate = rs.getString(2);
					String object = rs.getString(3);
					if(UrlUtilities.checkIfValidURL(object)) {
						//only it is a valid resource, i.e. a URI

						if(frontier.containsKey(object)) {
							//if the neighbor is already inside the frontier (not the border), we leave it alone
							continue;
						}

						double weightOfTheNode = 0;
						//check if the object element is among the leaves
						//in this case, the way we compute the weight is different
						if(checklist.containsKey(object)) {
							YosiNode n = checklist.get(object);
							weightOfTheNode = this.computeDinamicScore(n);
						} else {
							//get the static weight from the database
							//get the weight of the object
							String weightQuery = "SELECT id_, static_weight_ from " + DatabaseState.getSchema() +
									".yosi_node where iri = ?";
							PreparedStatement weightStatement = connection.prepareStatement(weightQuery);
							weightStatement.setString(1, object);
							ResultSet wRs = weightStatement.executeQuery();

							/* If the node is a URI that compares only as object in the dataset, it is not considered*/
							if(wRs.next())
								weightOfTheNode = wRs.getDouble("static_weight_");
							else
								continue;
						}

						//check the distance from s
						//TODO assuming all edges with weight 0,maybe one day we'll change
						double alt = v.getDistance() + weightOfTheNode;

						YosiNode u;

						URI subj = new URIImpl(vIri);
						URI pred = new URIImpl(predicate);
						//the object is surely an URI because we are here inside an if
						URI obj = new URIImpl(object);
						Statement connectingStatement = new StatementImpl(subj, pred, obj);

						if(border.containsKey(object)) {
							//if we have already seen this node, we have the object in the border
							u = border.get(object);
						} else {
							//visiting a new node. Need to create a new object
							u = new YosiNode(object);
							u.setIri(object);

							u.setDistance(Double.MAX_VALUE);
							u.setRadius(vRadius + 1);
							u.setPredicate(predicate);


							//set the score of the node to be used later
							u.setWeight(weightOfTheNode);

							u.setTriple(connectingStatement);
							//set the preceding node
							u.setPreviousNode(v);
							border.put(object, u);
							queue.add(u);

						}

						//anyway, add the statement connecting v to u
						//as immediate neighbor of v
						v.addNeighbour(connectingStatement);

						//check if we have to update the distance of the node
						if(alt < u.getDistance()) {
							//update the distance
							u.setDistance(alt);
							//update the predicate connecting u to v
							u.setPredicate(predicate);
							//update the previous node v in the path
							u.setPreviousNode(v);

							//update the radius and check that we are not too far away
							u.setRadius(vRadius + 1);

							//update the triple connecting u to v
							u.setTriple(connectingStatement);

							//need to update the queue in order to always have the min at the head
							if(queue.contains(u))
								//this operation is O(n), so not really recommended, but we will have
								//small queue, so I reckoned it was ok
								queue.remove(u);
							queue.add(u);
						}
					} else {
						//in this case, it is a literal and we add it anyway to v* (we suppose it is part of the subject node)
						URI subj = new URIImpl(vIri);
						URI pred = new URIImpl(predicate);
						//we know that if we are in this else clause object is a literal
						Literal obj = BlazegraphUsefulMethods.dealWithTheObjectLiteralString(object);

						Statement connectingStatement = new StatementImpl(subj, pred, obj);
						v.addNeighbour(connectingStatement);

						cluster.add(connectingStatement);
					}
				}//end while iteration on the neighborhood
			} //end of the creation of the set of nodes v*

			//now in the frontier and leaves objects we have everything we need
			//create a result object
			DijkstraExecution result = new DijkstraExecution();
			result.setRoot(root);
			result.setCluster(cluster);
			result.setFrontier(frontier);
			result.setConnection(connection);
			result.setLeaves(leaves);

			return result;


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	private double computeDinamicScore(YosiNode v) {
		double fraction = (double) maxScore / v.getScore();
		return 1 - fraction;
	}

	/** Executes the intersection between two list of nodes. One list is made of IDs, the second of YosiNode. 
	 * THe confrontation is made on the IDs.
	 * */
	private List<YosiNode> intersectionBetweenNodeListAndIDList(List<Integer> rNodes, List<YosiNode> U_i) {
		List<YosiNode> returningList = new ArrayList<YosiNode>();
		for(int v_i : rNodes) {
			for(YosiNode u : U_i) {
				if(v_i == u.getId_()) {
					returningList.add(u);
					break;
				}
			}
		}
		return returningList;
	}

	public String getKeywordNodesFilePath() {
		return keywordNodesFilePath;
	}

	public void setKeywordNodesFilePath(String keywordNodesFilePath) {
		this.keywordNodesFilePath = keywordNodesFilePath;
	}

	public String getRootNodesFilePath() {
		return rootNodesFilePath;
	}

	public void setRootNodesFilePath(String rootNodesFilePath) {
		this.rootNodesFilePath = rootNodesFilePath;
	}

	public String getSupportDirectoryPath() {
		return supportDirectoryPath;
	}

	public void setSupportDirectoryPath(String supportDirectoryPath) {
		this.supportDirectoryPath = supportDirectoryPath;
	}

	public String getAnswerTRECDirectoryPath() {
		return answerTRECDirectoryPath;
	}

	public void setAnswerTRECDirectoryPath(String answerTRECDirectoryPath) {
		this.answerTRECDirectoryPath = answerTRECDirectoryPath;
	}

	public String getAnswersUnigramIndexDirectoryPath() {
		return answersUnigramIndexDirectoryPath;
	}

	public void setAnswersUnigramIndexDirectoryPath(String answersUnigramIndexDirectoryPath) {
		this.answersUnigramIndexDirectoryPath = answersUnigramIndexDirectoryPath;
	}

	public String getAnswersBigramIndexDirectoryPath() {
		return answersBigramIndexDirectoryPath;
	}

	public void setAnswersBigramIndexDirectoryPath(String answersBigramIndexDirectoryPath) {
		this.answersBigramIndexDirectoryPath = answersBigramIndexDirectoryPath;
	}

	public String getAnswerGraphsDirectoryPath() {
		return answerGraphsDirectoryPath;
	}

	public void setAnswerGraphsDirectoryPath(String answerGraphsDirectoryPath) {
		this.answerGraphsDirectoryPath = answerGraphsDirectoryPath;
	}


}
