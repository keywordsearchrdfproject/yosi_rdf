package it.unipd.dei.ims.yosi.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.yosi.offline.FromGraphsToFieldTRECDocumentsPhase;
import it.unipd.dei.ims.yosi.offline.SingleNodeDocumentCreationPhase;
import it.unipd.dei.ims.yosi.offline.StatisticsComputationPhase;
import it.unipd.dei.ims.yosi.offline.VirtualGraphsBuildingPhase;
import it.unipd.dei.ims.yosi.offline.YosiStatisticsCalculationPhase;
import it.unipd.dei.ims.yosi.online.AnswerRankingPhase;
import it.unipd.dei.ims.yosi.online.AnswerSubgraphGenerationPhase;
import it.unipd.dei.ims.yosi.online.TopKeywordNodeSelectionPhase;
import it.unipd.dei.ims.yosi.online.TopRootsSelectionPhase;

public class YosiMainClass {
	/** Map with the properties of this class*/
	protected Map<String, String> map;
	
	/** Flag that advise if the class needs to deal with more than one query.
	 * */
	protected boolean multipleQueries;

	/** Timer to control the execution of the single algorithms.
	 * */
	protected static Stopwatch singleMethodTimer;

	/** Timer to control the all around execution.
	 * */
	protected static Stopwatch allAroundTimer;

	/** Query that the class is addressing.
	 * */
	protected String query;

	/** Id of the query the class is executing. 
	 * */
	protected String queryId;

	/** Path of the main query directory, where all the results and
	 * data structures are stored.
	 * */
	protected String mainQueryDirectory;

	protected FileWriter fWriter;
	protected BufferedWriter timeWriter;
	
	/** String to connect to SQL database */
	protected String jdbcConnectionString;


	boolean nodeStatisticsComputationFlag;
	boolean statisticsCalculationFlag;
	boolean singleNodeDocumentsCreationFlag;
	boolean virtualGraphGenerationFlag;
	boolean virtualDocumentsGenerationFlag;
	boolean rootsRankingFlag;
	boolean keywordNodesRankingFlag;
	boolean answerSubgraphsGenerationFlag;
	boolean answerRankingFlag;
	
	protected String schema;
	
	protected String mainAlgorithmDirectory;


	public YosiMainClass() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			nodeStatisticsComputationFlag = Boolean.parseBoolean( map.get("node.statistics.computation.flag") );
			statisticsCalculationFlag = Boolean.parseBoolean( map.get("statistics.calculation.flag") );
			singleNodeDocumentsCreationFlag = Boolean.parseBoolean( map.get("single.node.documents.creation.flag") );
			virtualGraphGenerationFlag = Boolean.parseBoolean( map.get("virtual.graph.generation.flag") );
			virtualDocumentsGenerationFlag = Boolean.parseBoolean( map.get("virtual.documents.generation.flag") );
			rootsRankingFlag = Boolean.parseBoolean( map.get("roots.ranking.flag") );
			keywordNodesRankingFlag = Boolean.parseBoolean( map.get("keyword.nodes.ranking.flag") );
			answerSubgraphsGenerationFlag = Boolean.parseBoolean( map.get("answer.subgraphs.generation.flag") );
			answerRankingFlag = Boolean.parseBoolean( map.get("answer.ranking.flag") );
			multipleQueries = Boolean.parseBoolean(map.get("multiple.queries"));
			this.mainQueryDirectory = map.get("main.query.directory");
			this.jdbcConnectionString = map.get("jdbc.connection.string");
			
			this.schema = map.get("schema");
			DatabaseState.setSchema(this.schema);
			
			File f = new File(mainQueryDirectory);
			if(!f.exists()) {
				f.mkdirs();
			}
			
			this.mainAlgorithmDirectory = f.getParent();

			singleMethodTimer = Stopwatch.createUnstarted();
			allAroundTimer = Stopwatch.createUnstarted();
			
			//we create the file where we will write all the information about this execution
			String outputTimeFilePath = (new File(this.mainQueryDirectory)).getParent() +
					"/log/";
			File g = new File(outputTimeFilePath);
			g.mkdirs();
			outputTimeFilePath += this.schema + "_times.txt";
			this.fWriter = new FileWriter(outputTimeFilePath, true);
			this.timeWriter = new BufferedWriter(fWriter);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void executeYosiAlgorithm() throws IOException {
		this.executeYosiAlgorithm(false);
	}
	
	public void executeYosiAlgorithmMultipleTimes () {
		
		try {
			ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		//get the list of query
		try {
			String queryFile = map.get("query.file");
			Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap(queryFile);
			
			for(Entry<String, String> entry : queryMap.entrySet()) {
				//id of the query
				String queryId = entry.getKey();
				String query = entry.getValue();

				//prepare the execution
				this.setQueryId(queryId);
				this.setQuery(query);

				this.executeYosiAlgorithm(true);
				
			}
			timeWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void executeYosiAlgorithm(boolean multipleTimes) throws IOException {

		allAroundTimer.start();

		// ***** OFFLINE PHASE ***** // (only once)
		this.phase0();

		this.phase1();

		this.phase2 ();

		this.phase3();

		this.phase4();


		//ONLINE PHASE
		timeWriter.write("\n***********\n\nExecuting now the on-line part of query " + this.queryId + "\n"
				+ "keyword query: \n" + query + "\n\n");
		System.out.print("\n***********\n\nExecuting now the on-line part of query " + this.queryId + "\n"
				+ "keyword query: \n" + query + "\n\n");
		this.phase5(multipleTimes);

		this.phase6(multipleTimes);

		this.phase7(multipleTimes);
		
		this.phase8(multipleTimes);
		
		System.out.println("the total time required to compute this query was: " + allAroundTimer.stop());
		timeWriter.write("the total time required to compute the query " + queryId 
				+ " was: " + allAroundTimer);
		allAroundTimer.reset();
		timeWriter.newLine();
	}

	/** Computations of the statistics regarding single nodes and labels
	 * regarding degrees.
	 * <p>
	 * This is the same operation done by the TSA algorithm, 
	 * so you can easily skip it. 
	 *  
	 * */
	protected void phase0() {
		if(nodeStatisticsComputationFlag) {
			//create statistics of nodes and labels. Same method of the TSA algorithm
			Stopwatch timer = Stopwatch.createStarted();
			
			StatisticsComputationPhase phase0 = new StatisticsComputationPhase();
			phase0.computeStatisticsUsingOnlyRDB();

			System.out.println("phase 0 ended in " + timer.stop());
		}
	}

	/** Statistics about single nodes regarding their content.
	 * 
	 * You need the yosi_node and yosi_document table in the database
	 * */
	protected void phase1 () {
		if(statisticsCalculationFlag) {
			System.out.println("Start statistics creation phase 1");
			System.setProperty("tokeniser", "EnglishTokeniser");
			Stopwatch timer = Stopwatch.createStarted();
			
			YosiStatisticsCalculationPhase phase1 = new YosiStatisticsCalculationPhase();
			phase1.computeYosiStatistics();

			System.out.println("phase 1 ended in " + timer.stop());
		}
	}

	/** Creation of single node documents and their indexes both unigram and bigram.
	 * */
	protected void phase2 () {
		if(singleNodeDocumentsCreationFlag) {
			System.out.println("Start phase 2: node documents creation");
			System.setProperty("tokeniser", "EnglishTokeniser");
			Stopwatch timer = Stopwatch.createStarted();
			
			SingleNodeDocumentCreationPhase phase2 = new SingleNodeDocumentCreationPhase();
			phase2.setNodeOutputDirectoryPath(this.mainAlgorithmDirectory + "/node_documents");
			phase2.setNodeDocumentsUnigramIndexPath(this.mainAlgorithmDirectory + "/node_documents_unigram_index");
			phase2.setNodeDocumentsBigramIndexPath(this.mainAlgorithmDirectory + "/node_documents_bigram_index");
			
			phase2.singleNodeDocumentsCreation();

			System.out.println("phase 2 ended in " + timer.stop());
		}
	}

	/** Offline creation of the virtual documents v*
	 * and useful information stored inside the RDB. 
	 * <p>
	 * This phase as of today is highly time consuming.
	 * */
	protected void phase3 () {
		if(virtualGraphGenerationFlag) {
			System.out.println("Start phase 3: virtual graphs creation");
			System.setProperty("tokeniser", "EnglishTokeniser");
			Stopwatch timer = Stopwatch.createStarted();
			
			//I want a second calculation of time
			long start = System.currentTimeMillis();

			VirtualGraphsBuildingPhase phase3 = new VirtualGraphsBuildingPhase();
			phase3.setVirtualGraphsDirectoryPath(this.mainAlgorithmDirectory + "/virtual_graphs");
			phase3.virtualGraphsBuildingPhase();

			long end = System.currentTimeMillis() - start;
			System.out.println("phase 3 ended in " + timer.stop() + " and " + end + " milliseconds");
			
		}
	}

	/** Convertion of the graphs document in TREC document and indexes.
	 * */
	protected void phase4 () {
		if(virtualDocumentsGenerationFlag) {
			System.out.println("Start phase 4: node documents creation");
			System.setProperty("tokeniser", "EnglishTokeniser");
			Stopwatch timer = Stopwatch.createStarted();
			
			FromGraphsToFieldTRECDocumentsPhase phase4 = new FromGraphsToFieldTRECDocumentsPhase();
			phase4.setMainDirectory(this.mainAlgorithmDirectory + "/virtual_graphs");
			phase4.setOutputDirectory(this.mainAlgorithmDirectory + "/virtual_documents");
			phase4.setVirtualDocumentUnigramIndexPath(this.mainAlgorithmDirectory + "/virtual_documents_unigram_index");
			phase4.setVirtualDocumentBigramIndexPath(this.mainAlgorithmDirectory + "/virtual_documents_bigram_index");
			
			phase4.fromGraphsTOFieldTRECDocuments();

			System.out.println("phase 4 ended in " + timer.stop());
			
		}
	}

	protected void phase5 (boolean multipleTimes) throws IOException {
		if(rootsRankingFlag) {
			System.out.println("Start phase 5: roots ranking");
			System.setProperty("tokeniser", "EnglishTokeniser");
			if(singleMethodTimer.isRunning())
				singleMethodTimer.stop();
			Stopwatch timer = Stopwatch.createStarted();
			
			TopRootsSelectionPhase phase5 = new TopRootsSelectionPhase();
			if(multipleTimes) {
				phase5.setQuery(this.query);
				phase5.setRootFile(mainQueryDirectory + "/" + queryId +"/support/roots.txt");
				
				phase5.setVirtualDocumentsUnigramIndexPath(this.mainAlgorithmDirectory 
						+ "/virtual_documents_unigram_index");
				phase5.setVirtualDocumentsBigramIndexPath(this.mainAlgorithmDirectory +
						"/virtual_documents_bigram_index");
				phase5.setNodeVirtualDocumentsUnigramIndexPath(this.mainAlgorithmDirectory + 
						"/node_documents_unigram_index");
				phase5.setNodeVirtualDocumentsBigramIndexPath(this.mainAlgorithmDirectory + 
						"/node_documents_bigram_index");
				
			}
			
			phase5.topRootSelectionPhase();

			System.out.println("phase 5 ended in " + timer.stop());
			
			timeWriter.write("phase 5 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	/**
	 * @throws IOException  */
	protected void phase6 (boolean multipleTimes) throws IOException {
		if(keywordNodesRankingFlag) {
			System.out.println("Start phase 6: keyword nodes ranking");
			System.setProperty("tokeniser", "EnglishTokeniser");
			Stopwatch timer = Stopwatch.createStarted();
			
			TopKeywordNodeSelectionPhase phase6 = new TopKeywordNodeSelectionPhase();
			if(multipleTimes) {
				phase6.setRootNodesFilePath(mainQueryDirectory + "/" + queryId + "/support/roots.txt"); 
				phase6.setKeywordNodesFilePath(mainQueryDirectory + "/" + queryId + "/support/keywords.txt");
				phase6.setQuery(query);
				
				phase6.setVirtualDocumentsUnigramIndexPath(this.mainAlgorithmDirectory 
						+ "/virtual_documents_unigram_index");
				phase6.setVirtualDocumentsBigramIndexPath(this.mainAlgorithmDirectory +
						"/virtual_documents_bigram_index");
				phase6.setNodeVirtualDocumentsUnigramIndexPath(this.mainAlgorithmDirectory + 
						"/node_documents_unigram_index");
				phase6.setNodeVirtualDocumentsBigramIndexPath(this.mainAlgorithmDirectory + 
						"/node_documents_bigram_index");
				//given the validated fields, this method prepares an object to
				//contain a series of other object derived (e.g. indexes)
				phase6.prepareParametersObject();
			}
			phase6.computeTopNKeywordNodes();

			System.out.println("phase 6 ended in " + timer.stop());
			
			timeWriter.write("phase 6 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase7(boolean multipleTimes) throws IOException {
		if(answerSubgraphsGenerationFlag) {
			System.out.println("Start phase 7: generation of answers graphs");
			Stopwatch timer = Stopwatch.createStarted();
			
			AnswerSubgraphGenerationPhase phase7 = new AnswerSubgraphGenerationPhase();
			if(multipleTimes) {
				phase7.setKeywordNodesFilePath(mainQueryDirectory + "/" + queryId + "/support/keywords.txt");
				phase7.setRootNodesFilePath(mainQueryDirectory + "/" + queryId + "/support/roots.txt"); 
				phase7.setSupportDirectoryPath(mainQueryDirectory + "/" + queryId + "/support"); 

				phase7.setAnswerGraphsDirectoryPath(mainQueryDirectory + "/" + queryId + "/answer/anser_graphs");
				phase7.setAnswerTRECDirectoryPath(mainQueryDirectory + "/" + queryId + "/answer/answer_documents");
				phase7.setAnswersUnigramIndexDirectoryPath(mainQueryDirectory + "/" + queryId + "/answer/unigram_index");
				phase7.setAnswersBigramIndexDirectoryPath(mainQueryDirectory + "/" + queryId + "/answer/bigram_index");
			}
			phase7.generateAnswerSubgraphs();

			System.out.println("phase 7 ended in " + timer.stop());
			
			timeWriter.write("phase 7 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}

	protected void phase8 (boolean multipleTimes) throws IOException {
		if(answerRankingFlag) {
			System.out.println("Start phase 8: rank the answer graphs");
			Stopwatch timer = Stopwatch.createStarted();
			
			AnswerRankingPhase phase8 = new AnswerRankingPhase();
			if(multipleTimes) {
				phase8.setAnswerUnigramIndexPath(mainQueryDirectory + "/" + queryId + "/answer/unigram_index");
				phase8.setAnswerBigramIndexPath(mainQueryDirectory + "/" + queryId + "/answer/bigram_index");
				
				phase8.setQuery(this.query);
				
				phase8.setStaticScoreFilePath(mainQueryDirectory + "/" + queryId + "/support/static_scores.xml");
				phase8.setAnswerRankingOutputFilePath(mainQueryDirectory + "/" + queryId + "/answer/ranking.txt");
				
				phase8.initialize();
			}
			phase8.rankTheAnswers();

			System.out.println("phase 8 ended in " + timer.stop());
			
			timeWriter.write("phase 8 ended in " + timer);
			timeWriter.newLine();
			timeWriter.flush();
		}
	}



	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public static void test(String[] args) {
		YosiMainClass execution = new YosiMainClass();
		
		if(!execution.isMultipleQueries()) {
			try {
				execution.executeYosiAlgorithm();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			execution.executeYosiAlgorithmMultipleTimes();
		}
		
	}

	public Map<String, String> getMap() {
		return map;
	}

	public void setMap(Map<String, String> map) {
		this.map = map;
	}

	public boolean isMultipleQueries() {
		return multipleQueries;
	}

	public void setMultipleQueries(boolean multipleQueries) {
		this.multipleQueries = multipleQueries;
	}


}
