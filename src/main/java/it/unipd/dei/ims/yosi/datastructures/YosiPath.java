package it.unipd.dei.ims.yosi.datastructures;

import java.util.ArrayList;
import java.util.List;

import org.openrdf.model.Model;
import org.openrdf.model.impl.TreeModel;

/** this class represents a path in a Yosi virtual document r*
 * from a root node r to the leaves v of the tree.
 *  */
public class YosiPath {
	//the root of the path
	private YosiNode root;
	
	/**the last node of the path*/
	private YosiNode leaf; 
	
	/** The weight of the path */
	private double weight;
	
	/** List of YosiNode representing the path from the leaf to the root.
	 * */
	private List<YosiNode> path;
	
	/** The keyword corresponding to the leaf node
	 * */
	private String keyword;
	
	public YosiPath() {
		this.path = new ArrayList<YosiNode>();
	}

	//##########
	
	/** computes the total weight of the path, sets it to the
	 * current path and returns the value.
	 * 
	 * */
	public double computeAndSetTotalWeight() {
		double totalWeight = 0.0;
		for(YosiNode node : path) {
			totalWeight += node.getWeight();
		}
		this.weight = totalWeight;
		return this.weight;
	}
	
	
	/** Re-creates the path from a leaf up to the root.
	 * */
	public void buildThePath(YosiNode leaf) {
		//set the first node as leaf
		this.leaf = leaf;
		
		path.add(leaf);
		YosiNode previous = leaf.getPreviousNode();
		while(previous!=null) {
			//add the node to the path
			this.path.add(previous);
			//get the previous node
			previous = previous.getPreviousNode();
		}
	}
	
	/** Return a string with the content field of the nodes inside the path
	 * */
	public String getContentFieldOfThePath() {
		String content_= "";
		for(YosiNode node : this.path) {
			//take also the connecting predicate
			content_ = content_ + " " + node.getContentField() + " " + node.getConnectingPredicateAsString();
		}
		return content_;
	}
	
	/** Return a string with the title field of the nodes inside the path
	 * */
	public String getTitleFieldOfThePath() {
		String title_= "";
		for(YosiNode node : this.path) {
			title_ = title_ + " " + node.getTitleField();
		}
		return title_;
	}
	
	public Model getCorrespondingSubgraph() {
		Model cluster = new TreeModel();
		
		for(YosiNode node : path) {
			cluster.addAll(node.getListOfLiteralNeighbors());
		}
		
		return cluster;
	}
	
	
	//##########
	
	//getters and setters
	
	
	public YosiNode getRoot() {
		return root;
	}

	public void setRoot(YosiNode root) {
		this.root = root;
	}

	public YosiNode getLeaf() {
		return leaf;
	}

	public void setLeaf(YosiNode leaf) {
		this.leaf = leaf;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public List<YosiNode> getPath() {
		return path;
	}

	public void setPath(List<YosiNode> path) {
		this.path = path;
	}


	public String getKeyword() {
		return keyword;
	}


	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	/** checks (using the IRI) if the leafs of these two paths are 
	 * equal.
	 *  */
	public boolean gotSameLeaf(YosiPath second) {
		String thisIri = this.getLeaf().getIri();
		String secondIri = second.getLeaf().getIri();
		
		if(thisIri.equals(secondIri))
			return true;
		return false;
	}
	
	/** This methods controls it a path is already contained inside a list.
	 * To do so, considering this application, the method checks if the provided
	 * path has the same leaf of any other path inside the provided list. 
	 * If they have the same leaf, in this application they are the same by construction.
	 * 
	 * */
	public static boolean pathContainedInList(List<YosiPath> list, YosiPath toCheck) {
		for(YosiPath p : list) {
			if(p.gotSameLeaf(toCheck))
				return true;
		}
		return false;
	}
	
	
	
}
