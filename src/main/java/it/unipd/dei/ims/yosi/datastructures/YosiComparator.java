package it.unipd.dei.ims.yosi.datastructures;

import java.util.Comparator;

/**This is a comparator for a min heap*/
public class YosiComparator implements Comparator<YosiNode> {

	public int compare(YosiNode o1, YosiNode o2) {
		double dis1 = o1.getDistance();
		double dis2 = o2.getDistance();
		if(dis1<dis2)
			return -1;
		else if(dis1>dis2)
			return 1;
		return 0;
	}
	
}
