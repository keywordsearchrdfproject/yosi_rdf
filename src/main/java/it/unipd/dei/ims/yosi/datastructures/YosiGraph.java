package it.unipd.dei.ims.yosi.datastructures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;

/** represents a subgraph which is an answer graph for the Yosi algorithm.
 * */

public class YosiGraph {

	/** adjacency list that keeps track of the neighbors for each node. A node is represented by it id. */
	protected Map<Integer, List<YosiNode>> adjacencyList;

	/** List with all the nodes in the graph.
	 * */
	protected List<YosiNode> nodesOfTheGraph;

	/** The score to be calculated. All this object is intended to obtain this one score.
	 * */
	protected int queryIndependentScore;

	/** total degree of the graph*/
	protected int totalDegree;


	private void build() {
		this.adjacencyList = new HashMap<Integer, List<YosiNode>>();
		this.nodesOfTheGraph = new ArrayList<YosiNode>();
	}

	/** Builds a graph from a list of YosiPath with supposedly a common root. 
	 * It is supposed that the graph is connected. 
	 * */
	public YosiGraph(List<YosiPath> pathList) {
		build();
		//now we recreate the graph following the paths
		for(YosiPath path : pathList) {
			//get the list of nodes in the path
			List<YosiNode> nodes = path.getPath();
			for(YosiNode node : nodes) {
				//for each node in the path

				//if necessary, add it to the list of nodes in this graph
				if(!nodesOfTheGraph.contains(node)) {
					nodesOfTheGraph.add(node);
				}

				//take the id of the node
				int nodeId_ = node.getId_();
				//take the parent of the node
				YosiNode parent = node.getPreviousNode();

				if(parent==null)
					continue;
				//check if already present in the adjacencyList map
				if(adjacencyList.containsKey(nodeId_)) {
					//get the adjacency list of the node
					List<YosiNode> nodeList = adjacencyList.get(nodeId_);
					//add to the list the parent of this node if not already present
					if(!nodeList.contains(parent)) {
						nodeList.add(parent);
					}
				} else {
					//new node discovered in the graph
					List<YosiNode> nodeList = new ArrayList<YosiNode>();
					nodeList.add(parent);
					adjacencyList.put(nodeId_, nodeList);
				}

				//do the same for the parent
				int parentId_ = parent.getId_();
				if (adjacencyList.containsKey(parentId_)) {
					List<YosiNode> nodeList = adjacencyList.get(parentId_);
					//add to the list the parent of this node if not already present
					if(!nodeList.contains(node)) {
						nodeList.add(node);
					}
				} else {
					//new node discovered in the graph
					List<YosiNode> nodeList = new ArrayList<YosiNode>();
					nodeList.add(node);
					adjacencyList.put(parentId_, nodeList);
				}

			}
		}
	}

	/** Builds a YosiGraph from a Blazegraph Model*/
	public YosiGraph(Model graph) {
		build();
		//to be efficient, we give ids on the fly to these nodes
		//using the RDB whould be too expensive in time
		int nodeCounter = 0;

		//now we create the graph from the model
		for(Statement t : graph) {
			//get the subject and the object of the node
			//and create YosiNodes out of them
			String sbj = t.getSubject().toString();
			String obj = t.getObject().toString();

			YosiNode subNode = new YosiNode();
			subNode.setIri(sbj);
			YosiNode objNode = new YosiNode();
			objNode.setIri(obj);

			//add the nodes to the list of nodes of this graph in case they are new
			//also, give them a new id
			YosiNode y;
			if((y = this.retrieveByIri(nodesOfTheGraph, subNode)) == null) {
				nodesOfTheGraph.add(subNode);
				subNode.setId_(nodeCounter);
				nodeCounter++;
			} else {
				subNode = y;
			}

			if((y = this.retrieveByIri(nodesOfTheGraph, objNode)) == null) {
				nodesOfTheGraph.add(objNode);
				objNode.setId_(nodeCounter);
				nodeCounter++;
			} else {
				objNode = y;
			}

			//take the id of the object node
			int objId = objNode.getId_();
			//check if the object is already present in the adjacency list
			if(adjacencyList.containsKey(objId)) {
				//get the adjacency list of the node
				List<YosiNode> nodeList = adjacencyList.get(objId);
				//add the subject to the neighbors (we are not counting the direction here)
				if(!this.isContainedByIri(nodeList, subNode)) {
					nodeList.add(subNode);
				}
			} else {
				//new node discovered in the graph
				List<YosiNode> nodeList = new ArrayList<YosiNode>();
				nodeList.add(subNode);
				adjacencyList.put(objId, nodeList);
			}

			//do the same with the subject with respect to the object
			//take the id of the subject node
			int sbjId = subNode.getId_();
			//check if the object is already present in the adjacency list
			if(adjacencyList.containsKey(sbjId)) {
				//get the adjacency list of the node
				List<YosiNode> nodeList = adjacencyList.get(sbjId);
				//add the object to the neighbors (we are not counting the direction here)
				if(!this.isContainedByIri(nodeList, objNode)) {
					nodeList.add(objNode);
				}
			} else {
				//new node discovered in the graph
				List<YosiNode> nodeList = new ArrayList<YosiNode>();
				nodeList.add(objNode);
				adjacencyList.put(sbjId, nodeList);
			}
		}
	}

	private boolean isContainedByIri(List<YosiNode> nodesList, YosiNode n) {
		for(YosiNode yn : nodesList) {
			if(yn.getIri().equals(n.getIri())) {
				return true;
			}
		}
		return false;
	}

	private YosiNode retrieveByIri(List<YosiNode> nodesList, YosiNode n) {
		for(YosiNode ys: nodesList) {
			if(ys.getIri().equals(n.getIri())) {
				return ys;
			}
		}
		return null;
	}



	/** Compute the score of the query independent component on this answer graph.
	 * Need the total degree of the graph we are working on.
	 * 
	 * @param totalDegree the degree of the collection graph we are working on. 
	 * 
	 * */
	public double computeQueryIndependentScore(int totalDegree) {
		//the score to return. It is a max
		double score = Double.NEGATIVE_INFINITY;
		for(YosiNode root : this.nodesOfTheGraph) {
			//each node of this graph will be used as root
			double s = this.computeScoreForARoot(root, totalDegree);
			if(s > score)
				//update the score
				score = s;
		}
		return score;
	}

	private double computeScoreForARoot(YosiNode root, int totalDegree) {
		//take the degree of this root
		int rootId_ = root.getId_();
		int rootDegree = this.adjacencyList.get(rootId_).size();

		//compute the probability of the root
		double score = Math.log((double) rootDegree / totalDegree);

		//re-set the tree so it is rooted in this root
		this.reRootTheTree(root);


		//now compute the other probabilities
		for(YosiNode node : this.nodesOfTheGraph) {
			if(!node.equalById(root)) {
				//we operate on the other nodes, not the root
				//we use the sum because we are returning log values
				score += computeScoreForANode(node);
			}
		}
		return score;
	}

	/** Given a root node, this method starts from that root to set the tree structure
	 * in the answer graph as a tree starting from that root.
	 * */
	private void reRootTheTree(YosiNode root) {
		int rootId_ = root.getId_();
		root.setParentId(-1);
		//this list represents the nodes already corrected
		List<Integer> correctedNodes = new ArrayList<Integer>();
		correctedNodes.add(rootId_);

		//now we do a BFS to update the roots of the graph
		Queue<Integer> exploringNodes = new LinkedList<Integer>();
		exploringNodes.add(rootId_);

		//do the BFS
		while(!exploringNodes.isEmpty()) {
			int nodeId = exploringNodes.poll();
			//get the neighbors of this node
			List<YosiNode> neighbors = this.adjacencyList.get(nodeId);
			for(YosiNode neighbor : neighbors) {
				//for every neighbor
				int neighborId = neighbor.getId_();
				//if we didn't already set it (it is not the parent)
				if(!correctedNodes.contains(neighborId)) {
					//indicate its parent
					neighbor.setParentId(nodeId);
					//set the fact that we checked it
					correctedNodes.add(neighborId);
					//add it to the list of nodes to explore
					exploringNodes.add(neighbor.getId_());
				}
			}
		}

	}

	private double computeScoreForANode( YosiNode node ) {
		//get the degree of the node (numerator)
		int nodeId_ = node.getId_();
		int nodeDegree = this.adjacencyList.get(nodeId_).size();


		//get the sum of the degrees of all the siblings of the node plus the node itself (denominator)
		int denominator = 0;
		//get the id of the parent
		int parentId = node.getParentId();
		//get all the nodes in the adjacency list of the parent
		List<YosiNode> adParentList = this.adjacencyList.get(parentId);
		//for every node of the list
		for(YosiNode n : adParentList) {
			//only for the sons of the parent - in the paper they talk about the neighbours of the parent
			//not necessarily only the sons. 
			//			if(n.getParentId() == parentId) {
			//get the degree of the node
			int sonId_ = n.getId_();
			int sonDegree = this.adjacencyList.get(sonId_).size();
			denominator += sonDegree;
			//			}
		}

		double score = (double) nodeDegree / denominator;
		score = Math.log(score);
		return score;
	}



}
