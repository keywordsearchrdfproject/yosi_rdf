package it.unipd.dei.ims.yosi.offline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;

/** Yosi algorithm: phase 2.
 * <p>
 * Creation and indexing (both unigram and bigram) of the 
 * documents of the single nodes (not the v*).
 * */
public class SingleNodeDocumentCreationPhase {

	/** Database string
	 * */
	private String jdbcConnectionString;

	/** The output directory where to save the documents.
	 * */
	private String nodeOutputDirectoryPath;

	private int docCounter, fileCounter;
	
	private String nodeDocumentsUnigramIndexPath;
	private String nodeDocumentsBigramIndexPath;
	
	/** the database schema we are using right now*/
	private String schema;


	private static String SQL_GET_NODES = "SELECT id_, title_, content_ "
			+ "FROM yosi_node ORDER BY id_ limit ? offset ?";

	public SingleNodeDocumentCreationPhase () {
		docCounter = 0;
		fileCounter = 0;
		this.setup();
	}

	private void setup() {
		try {
			Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			this.jdbcConnectionString = map.get("jdbc.connection.string");
			this.nodeOutputDirectoryPath = map.get("node.output.directory.path");
			this.nodeDocumentsUnigramIndexPath = map.get("node.documents.unigram.index.path");
			this.nodeDocumentsBigramIndexPath = map.get("node.documents.bigram.index.path");
			
			this.schema = map.get("schema");
			SQL_GET_NODES = "SELECT id_, title_, content_ FROM " + this.schema + 
					".yosi_node ORDER BY id_ limit ? offset ?";


		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void singleNodeDocumentsCreation() {
		//create the documents 
		this.createSingleNodeDocuments();
		//index them
		this.indexTheSingleNodeDocuments();
	}
	

	/**Utilizes the information inside the yosi_node table to create TREC files containing the
	 * documents corresponding to single nodes documents.
	 * */
	private void createSingleNodeDocuments() {

		Connection connection = null;
		int limit = 10000, offset = 0;

		try {
			//clean the directory with the files
			File outDir = new File(this.nodeOutputDirectoryPath);
			if(!outDir.exists())
				outDir.mkdirs();
			FileUtils.cleanDirectory(outDir);

			BufferedWriter writer = null;

			//open the connection
			connection = ConnectionHandler.createConnectionAsOwner(this.jdbcConnectionString, this.getClass().getName());;
			//read all the yosi_node table
			while(true) {

				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, limit, offset, SQL_GET_NODES);
				offset += limit;
				if(iterator.next()) {
					iterator.beforeFirst();
					while(iterator.next()) {
						//deal with the multiple documents inside a file
						if(docCounter%2048==0) {
							//create a new file TREC with the documents written in it
							if(writer!=null) {
								writer.close();
							}
							fileCounter++;
							//create the writer
							String outputFile = this.nodeOutputDirectoryPath + "/" + fileCounter + ".trec";
							Path outputPath = Paths.get(outputFile);
							writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8);
							System.out.println("printed " + docCounter + " node documents");
						}
						docCounter++;
						createNodeDocument(iterator, writer);
					}
				} else {
					break;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null) {
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void indexTheSingleNodeDocuments () {
		IndexerDirectoryOfTRECFiles indexer = new IndexerDirectoryOfTRECFiles();
		//set directory of the documents to index
		indexer.setDirectoryToIndex(this.nodeOutputDirectoryPath);
		
		indexer.setIndexPath(this.nodeDocumentsUnigramIndexPath);
		indexer.index("unigram");
		
		indexer.setIndexPath(this.nodeDocumentsBigramIndexPath);
		indexer.index("bigram");
	}

	/** Writes on the file pointed by the writer
	 * the information of the node contained in the iterator.
	 * 
	 * @param iterator a ResultSet located to the tuple we need to elaborate.
	 * @param writer BufferedWriter on the file where we are writing
	 * @throws IOException 
	 * @throws SQLException 
	 * */
	private static void createNodeDocument(ResultSet iterator, BufferedWriter writer) throws IOException, SQLException {
		int id = iterator.getInt("id_");

		writer.write("<DOC>");
		writer.newLine();

		writer.write("\t<DOCNO>"+id+"</DOCNO>");
		writer.newLine();

		writer.write("\t<TITLE>"+ iterator.getString("title_") + "</TITLE>");
		writer.newLine();

		writer.write("\t<CONTENT>"+ iterator.getString("content_") + "</CONTENT>");
		writer.newLine();
		writer.write("</DOC>");
		writer.newLine();
		
		writer.flush();
	}

	public String getNodeOutputDirectoryPath() {
		return nodeOutputDirectoryPath;
	}

	public void setNodeOutputDirectoryPath(String nodeOutputDirectoryPath) {
		this.nodeOutputDirectoryPath = nodeOutputDirectoryPath;
	}

	public String getNodeDocumentsUnigramIndexPath() {
		return nodeDocumentsUnigramIndexPath;
	}

	public void setNodeDocumentsUnigramIndexPath(String nodeDocumentsUnigramIndexPath) {
		this.nodeDocumentsUnigramIndexPath = nodeDocumentsUnigramIndexPath;
	}

	public String getNodeDocumentsBigramIndexPath() {
		return nodeDocumentsBigramIndexPath;
	}

	public void setNodeDocumentsBigramIndexPath(String nodeDocumentsBigramIndexPath) {
		this.nodeDocumentsBigramIndexPath = nodeDocumentsBigramIndexPath;
	}

}
