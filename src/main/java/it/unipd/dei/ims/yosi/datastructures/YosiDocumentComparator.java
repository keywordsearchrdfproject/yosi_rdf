package it.unipd.dei.ims.yosi.datastructures;

import java.util.Comparator;


/** Comparator for a max heap.
 * */
public class YosiDocumentComparator implements Comparator<YosiDocument> {

	public int compare(YosiDocument o1, YosiDocument o2) {
		double dis1 = o1.getScore();
		double dis2 = o2.getScore();
		if(dis1<dis2)
			return 1;
		else if(dis1>dis2)
			return -1;
		return 0;
	}
	
}
