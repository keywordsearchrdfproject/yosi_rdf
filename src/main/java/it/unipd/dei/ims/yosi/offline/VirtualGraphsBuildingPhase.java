package it.unipd.dei.ims.yosi.offline;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.Tokeniser;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.terms.PorterStemmer;
import org.terrier.terms.Stopwords;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.terrier.bigram.Bigram;
import it.unipd.dei.ims.terrier.bigram.BigramUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiComparator;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;

/** Yosi Algorithm: phase 3
 * <p>
 * In this class, we create the virtual graphs than then will
 * be transformed in virtual documents as required by Yosi 2016.
 * <p>
 * The class also stores information of the graphs inside the yosi_node table. 
 * This information is used later. 
 * <p>
 * NB: it is necessary to have the Statistics of the database in a RDB for the graph in order to 
 * perform the calculations.
 * */
public class VirtualGraphsBuildingPhase {

	private static String SQL_GET_NODES = "SELECT id_, iri, degree_, title_, content_, static_weight_ "
			+ "from yosi_node order by id_ limit ? offset ?";

	private static String SQL_GET_NEIGHBORS = "SELECT subject_, predicate_, object_ from triple_store where subject_ = ?";

	private static String SQL_INSERT_NODE_IN_VIRTUAL_DOC = "insert into yosi_virtual_document(v_id, u_id, ws_) "
			+ "values (?, ?, ?)";

	private static String SQL_UPDATE_WTF_CONTENT = "update yosi_node SET unigram_length_title = ?, unigram_length_content = ?, "
			+ "bigram_length_title = ?, bigram_length_content = ? WHERE id_=?";

	private static String SQL_RETRIEVE_STATIC_WEIGHT = "select ws_ from yosi_virtual_document where v_id = ? and u_id = ?";
	
	private String schema;

	private String jdbcConnectionString;

	/** path of the directory where to save the virtual graphs.
	 * */
	private String virtualGraphsDirectoryPath; 

	/** parameter of the weighted term frequency in the Yosi 
	 * formula
	 * */
	private double sigma;

	/** the maximum radius we allow for the creation of the virtual graphs
	 * */
	private int tau;
	
	private int limit;
	
	private int offset;

	public VirtualGraphsBuildingPhase () {
		this.setup();
	}

	private void setup() {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			//setting system properties for terrier to find the terrier_home and the /etc directory
			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);

			this.jdbcConnectionString = map.get("jdbc.connection.string");
			this.virtualGraphsDirectoryPath = map.get("virtual.graphs.directory.path");
			this.tau = Integer.parseInt(map.get("tau"));
			this.sigma = Double.parseDouble(map.get("sigma"));

			this.limit = Integer.parseInt(map.getOrDefault("limit", "10000"));
			this.offset = Integer.parseInt(map.getOrDefault("offset", "0"));
			
			this.schema = map.get("schema");
			
			SQL_GET_NODES = "SELECT id_, iri, degree_, title_, content_, static_weight_ "
					+ "from " +this.schema + ".yosi_node order by id_ limit ? offset ?";

			SQL_GET_NEIGHBORS = "SELECT subject_, predicate_, object_ from "
					+ this.schema + ".triple_store where subject_ = ?";

			SQL_INSERT_NODE_IN_VIRTUAL_DOC = "insert into " + this.schema + ".yosi_virtual_document(v_id, u_id, ws_) "
					+ "values (?, ?, ?)";

			SQL_UPDATE_WTF_CONTENT = "update " + this.schema + ".yosi_node "
					+ "SET unigram_length_title = ?, unigram_length_content = ?, "
					+ "bigram_length_title = ?, bigram_length_content = ? WHERE id_=?";

			SQL_RETRIEVE_STATIC_WEIGHT = "select ws_ from " + this.schema + 
					".yosi_virtual_document where v_id = ? and u_id = ?";
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void virtualGraphsBuildingPhase() {
		//check the directory where we have to write
		File directory = new File(this.virtualGraphsDirectoryPath);
		if(!directory.exists()) {
			directory.mkdirs();
		}
		//clean the directory
		try {
			FileUtils.cleanDirectory(directory);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Connection connection = null;
		//connect to the database
		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());

			//create one virtual graphs for each node in the yosi_node table
			this.buildTheGraphs(connection, tau);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection!=null)
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

	}

	/** Creates the graphs for each document in the yosi_node table.
	 * As described by Yosi 2016.
	 * */
	private void buildTheGraphs(Connection connection, int tau) {
		
		int docCounter = this.offset;
		int dirCounter = (int) Math.ceil((double)this.offset / 2048);

		PreparedStatement updatePrepared = null;

		try {
			/* update statement to update the length 
			 * of the nodes in the table. Defined so early here
			 * in order to batch update later
			 * */
			updatePrepared = connection.prepareStatement(SQL_UPDATE_WTF_CONTENT);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.print("starting writing the documents");

		Stopwatch timer = Stopwatch.createStarted();
		Stopwatch totalTimer = Stopwatch.createStarted();

		while(true) {
			
			
			//iterate over the yosi_table (for every node v, we create v*)
			try {
				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, this.limit, offset, SQL_GET_NODES);
				this.offset += this.limit;

				if(iterator.first()) {
					//get the cursor to the begin
					iterator.beforeFirst();

					while(iterator.next()) {
						
						//this little controll checks if we have 
						//been terminated
						if(Thread.interrupted()) {
							ThreadState.setOffLine(false);
							return;
						}
						
						//for each line of the window
						int id_ = iterator.getInt("id_");

						if(docCounter%2048==0) {
							//need to create a new sub-directory
							dirCounter++;//we start with 1
							File d = new File(this.virtualGraphsDirectoryPath + "/" + (dirCounter));
							if(!d.exists())
								d.mkdirs();
							System.out.println("printed " + docCounter + " virtual documents in directory "+ (dirCounter-1) + " after " + totalTimer + " now updating...");

							//update the yosi_node table
							updatePrepared.executeBatch();
							updatePrepared.clearBatch();
						}

						if(docCounter%100==0) {
							System.out.println("printed " + docCounter + " virtual documents in directory "+ (dirCounter) + " in " + timer);
							timer.reset().start();
						}

						//now we create the document of this node v
						docCounter++;
						String outputDocumentPath = this.virtualGraphsDirectoryPath + "/" + dirCounter + "/" + id_ + ".ttl";

						buildOneDocument(connection, iterator, outputDocumentPath, updatePrepared);
					}
				} else {
					break;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/** Creates and prints the document v* of a node v given a ResultSet positioned
	 * to the tuple of this node in the yosi_node table.
	 * 
	 * @param vdDirectory path where to save the graph
	 * @param updateStatement this is tricky: an SQL statement with the update query: 
	 * update yosi_node SET sum_wtf_title = ?, sum_wtf_content = ? WHERE id_=?
	 * This query is used to update the wtf values inside the yosi_node table. 
	 * We need to instantiate this prepared statement early on in order to be able
	 * to perform a batch update and be more computationally efficients. 
	 * */
	private void buildOneDocument(Connection connection, ResultSet iterator,
			String vdDirectory, PreparedStatement updateStatement) throws SQLException {

		//get the id of the node
		int id_ = iterator.getInt(1);

		//get the IRI of the first node
		String iri = iterator.getString(2);

		//distance of the starting v from itself. Here it is its static weight
		double staticWeight = iterator.getDouble("static_weight_");

		//create the source node s. Set IRI, distance, id and radius
		YosiNode s = new YosiNode(iri);
		s.setIri(iri);
		s.setDistance(staticWeight);
		s.setId_(id_);
		s.setRadius(0);
		//create min heap
		PriorityQueue<YosiNode> queue = new PriorityQueue<YosiNode>(new YosiComparator());
		queue.add(s);

		//the graph that we are going to build and then print
		Model cluster = new TreeModel ();

		//a map to keep track of the nodes inside our frontier (that is, the nodes
		//inside v*)
		Map<String, YosiNode> frontier = new HashMap<String, YosiNode>();

		//a list with the IDs to keep track of the discovered nodes outside our frontier (we are discovering them)
		Map<String, YosiNode> border = new HashMap<String, YosiNode>();
		border.put(iri, s);//the source will be the first node to enter our frontier

		//adapted Dijkstra  (you have no idea how much I love this algorithm)
		while(!queue.isEmpty()) {
			//take the node who needs to go inside the frontier
			YosiNode v = queue.poll();
			String vString = v.getStringContent();

			//this node is added to the frontier (we won't touch its distance anymore)
			frontier.put(vString, v);
			//this node is removed from the borderNodes
			border.remove(vString);

			//get the triple of which this node is the object in order to add it to the building subgraph 
			Statement t = v.getTriple();

			/* read the information from the database about the content and title field of this node
			 * and update the object node with this information. They will be used later in the calculations 
			 * of other information. */
			String SQL_VD_INTERROGATION = "select title_, content_ from " + this.schema + ".yosi_node where iri=?";
			PreparedStatement pfVd = connection.prepareStatement(SQL_VD_INTERROGATION);
			pfVd.setString(1, vString);
			ResultSet rsVD = pfVd.executeQuery();
			if(rsVD.next()) {
				String textVd = rsVD.getString(1);
				String contentVd = rsVD.getString(2);
				//set the title field and content field
				v.setTitleField(textVd);
				v.setContentField(contentVd);
			}
			/******************************/

			if(t!=null) {
				//if t==null, this is the starting node, no problem

				//add the triple to the building subgraph
				cluster.add(t);
			}

			int vRadius = v.getRadius();

			//take the neighbors
			PreparedStatement psNeighbors = connection.prepareStatement(SQL_GET_NEIGHBORS);
			psNeighbors.setString(1, vString);
			ResultSet rs = psNeighbors.executeQuery();
			//check the neighbors
			while(rs.next()) {//for each neighbor
				String predicate = rs.getString(2);
				String object = rs.getString(3);

				if(UrlUtilities.checkIfValidURL(object)) {//we are only interested in URI nodes for the logic of Dijkstra
					if(frontier.containsKey(object)) {
						//if the neighbor is already inside the frontier, we leave it alone
						continue;
					}
					//if the neighbor is not already been settled
					//get the weight of the object
					String weightQuery = "SELECT id_, static_weight_ from " + this.schema + ".yosi_node where iri = ?";
					PreparedStatement weightStatement = connection.prepareStatement(weightQuery);
					weightStatement.setString(1, object);
					ResultSet wRs = weightStatement.executeQuery();
					if(wRs.next()) {
						/*the node is subject of something. Otherwise,
						 * it is a URI appearing only as object. In this case,
						 * we are assuming that it's utility (and semantic)
						 * is similar to that of a Literal node. So it is
						 * treated in the following else as such.*/

						int objectId = wRs.getInt(1);
						double staticWeightOfNeighbor = wRs.getDouble(2);

						//check the distance from s
						//XXX assuming all edges with weight 0
						double alt = v.getDistance() + staticWeightOfNeighbor;
						YosiNode u;
						//initialize u
						if(border.containsKey(object)) {
							//if we already visited this node, we have the object
							u = border.get(object);
						} else {
							//visiting a new node. Initialize it
							u = new YosiNode(object);
							u.setDistance(Double.MAX_VALUE);
							u.setId_(objectId);
							u.setRadius(vRadius + 1);
							u.setPredicate(predicate);

							URI subj = new URIImpl(vString);
							URI pred = new URIImpl(predicate);
							URI obj = new URIImpl(object);

							Statement connectingStatement = new StatementImpl(subj, pred, obj);

							if(u.getRadius() > tau)
								continue;//beyond the radius, don't add the node to the computation
							u.setTriple(connectingStatement);
							border.put(object, u);
							queue.add(u);
						}

						//check if we have to update the distance of the node
						if(alt < u.getDistance()) {
							//update the distance
							u.setDistance(alt);
							//update the predicate connecting u to v
							u.setPredicate(predicate);

							//update the radius and check we are not too far away
							u.setRadius(vRadius + 1);
							if(u.getRadius() > this.tau)
								continue;

							//update the triple connecting u to v
							URI subj = new URIImpl(vString);
							URI pred = new URIImpl(predicate);
							URI obj = new URIImpl(object);

							Statement connectingStatement = new StatementImpl(subj, pred, obj);

							u.setTriple(connectingStatement);

							//need to update the queue in order to always have the min at the head
							if(queue.contains(u))
								//this operation is O(n), so not really recommended, but we will have
								//small queue, so I reckoned it was ok
								queue.remove(u);
							queue.add(u);
						}
					} else {
						//URI which is only object
						if(v.getRadius() + 1 > tau) {
							continue;
						}
						//treat it like a 'literal'
						//thus, build the triple and add it anyway
						URI subj = new URIImpl(vString);
						URI pred = new URIImpl(predicate);
						URI obj = new URIImpl(object);
						
						Statement connectingStatement = new StatementImpl(subj, pred, obj);
						
						cluster.add(connectingStatement);
						
					}
				} else {
					//in this case, it is a literal and we add it anyway to v* (we suppose it is part of the subject node)
					URI subj = new URIImpl(vString);
					URI pred = new URIImpl(predicate);
					//we know that if we are in this else clause object is a literal
					Literal obj = BlazegraphUsefulMethods.dealWithTheObjectLiteralString(object);

					Statement connectingStatement = new StatementImpl(subj, pred, obj);

					cluster.add(connectingStatement);
				}
			}
		}//end of the creation of the set of nodes v*

		//now we print the virtual document
		BlazegraphUsefulMethods.printTheDamnGraph(cluster, vdDirectory);

		//we can use the graph to compute other useful informations that we will need in the statistic table
		try {
			computeOfflineInformation(connection, frontier, id_,  cluster, updateStatement);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void computeOfflineInformation(Connection connection, 
			Map<String, YosiNode> frontier, 
			int v_id, 
			Model cluster,
			PreparedStatement updateStatement) throws Exception {

		// ******************************************************
		// ****** FIRST: COMPUTATION OF THE STATIC WEIGHTS ******
		// ******************************************************

		List<Object> elementList = computeStaticWeights(connection, frontier, v_id);

		//use these two lists to keep track of the distinct words comparing in the title and content field of v*
		List<String> titleList = (List<String>)elementList.get(0);
		List<String> contentList = (List<String>)elementList.get(1);
		//use these strings to keep track of all that words that compare in the fields of v*
		String titleDocument = ((String)elementList.get(2)).trim();
		String contentDocument = ((String)elementList.get(3)).trim();

		// ******************************************************
		// *********** SECOND: LENGTHS FOR UNIGRAMS *************
		// ******************************************************

		//relative static weight of in v in v*, this will be useful a lot later
		double ws_v = 0;

		PreparedStatement psRetrieve = connection.prepareStatement(SQL_RETRIEVE_STATIC_WEIGHT);
		psRetrieve.setInt(1, v_id);
		psRetrieve.setInt(2, v_id);
		ResultSet rs = psRetrieve.executeQuery();
		rs = psRetrieve.executeQuery();
		if(rs.next()) {
			ws_v = rs.getDouble(1);
		}

		computeUnigramTitleAndContentLengths(titleList, contentList, connection, frontier, ws_v, v_id, sigma, updateStatement);

		// ******************************************************
		// *********** THIRD: LENGTHS FOR BIGRAMS ***************
		// ******************************************************

		computeBigramTitleAndContentLengths(connection, frontier, ws_v, v_id, sigma, 
				updateStatement, titleDocument, contentDocument);


		updateStatement.setInt(5, v_id);
		updateStatement.addBatch();

	}

	/**Computes and inserts in memory the relative static weights of the node of v* inside the document itself.
	 * In the process, calculates and returns the string content of the fields of v*.
	 * 
	 * @param frontier a map containing the YosiNode inside v* (including v)
	 * @param v_id the id of the root node of v*, the one that appears in the table yosi_node 
	 * 
	 * @return A list containing 4 Objects. The first one is the list of distinct words comparing in the
	 * title field of v*. The second element is a list of distinct words comparing in the content field of v*.
	 * The third and fourth elements contain respectively a string with the whole title field and content field of v*.
	 * */
	private static List<Object> computeStaticWeights(Connection connection, Map<String, YosiNode> frontier, 
			int v_id) throws Exception {
		//use these two lists to keep track of the distinct words comparing in the title and content field of v*
		List<String> titleList = new ArrayList<String>();
		List<String> contentList = new ArrayList<String>();
		//use these strings to keep track of all that words that compare in the fields of v*
		String titleDocument = "";
		String contentDocument = "";

		PreparedStatement psInsertion = null;
		psInsertion = connection.prepareStatement(SQL_INSERT_NODE_IN_VIRTUAL_DOC);

		//iteration on all the nodes of v*
		for(Entry<String, YosiNode> uEntry : frontier.entrySet()) {
			//for each node u
			YosiNode u = uEntry.getValue();//node
			Integer u_id = u.getId_();//id of the node

			//get the distance of u from v
			double ws = u.getDistance();

			//here we insert in memory the information about the relative static weight w_s of the node u
			//we have computed and saved this information later during the Dijkstra algorithm
			psInsertion.setInt(1, v_id);
			psInsertion.setInt(2, u_id);
			psInsertion.setDouble(3, ws);

			psInsertion.addBatch();

			Document doc;
			//XXX NB: here you need to have fixed the etc/ directory with the indications for the stemmer and the stopword list
			Stopwords stop = new Stopwords(null);
			PorterStemmer stemmer = new PorterStemmer();

			//add words to the lists (create the title document of v*, already indexed)
			//use the FileDocument pipeline to extrapolate the words (this procedure eliminates ',', '.' and other
			//nuisances)
			doc = new FileDocument(new StringReader(u.getTitleField()), new HashMap(), Tokeniser.getTokeniser());
			while(!doc.endOfDocument()) {//for every word in the document
				String s = doc.getNextTerm();
				if(s==null)
					break;
				//check that it is not a stopword
				if(!stop.isStopword(s)) {
					//stem the word
					s = stemmer.stem(s);
					//populate the title list with the word if it is new
					if(!titleList.contains(s))
						titleList.add((String) s);
					//add the word to the title string (repetitions here are necessary)
					titleDocument = titleDocument + " " + s;
				}
			}

			//create the content document of v*
			doc = new FileDocument(new StringReader(u.getContentField()), new HashMap(), Tokeniser.getTokeniser());
			while(!doc.endOfDocument()) {//for eery word in the document
				String s = doc.getNextTerm();
				if(s==null)
					break;
				//check that it is not a stopword
				if(!stop.isStopword(s)) {
					//stem the word
					s = stemmer.stem(s);
					//populate the title list with the word if it is new
					if(!contentList.contains(s))
						contentList.add((String) s);
					//add the word to the title string (repetitions here are necessary)
					contentDocument = contentDocument + " " + s;
				}
			}
		}//end of for cycle over all the nodes

		//now we can insert the nodes of v*
		psInsertion.executeBatch();
		psInsertion.clearBatch();
		

		List<Object> list = new ArrayList<Object>();
		list.add(titleList);
		list.add(contentList);
		list.add(titleDocument);
		list.add(contentDocument);

		return list;
	}
	
	/**Computes the title and content lengths of v* for unigrams and inserts the data in the provided preparedStatement in order
	 * to be added to the batch.
	 * 
	 * @param ws_v the relative static weight of v in v*. That is, the static weight of v.
	 * @throws SQLException 
	 * */
	private static void computeUnigramTitleAndContentLengths(List<String> titleList, List<String> contentList,
			Connection connection, Map<String, YosiNode> frontier, double ws_v, int v_id, double sigma, 
			PreparedStatement updateStatement)
					throws SQLException {
		//numbers to create and insert in the database
		double contentSum = 0, titleSum = 0, ws_u = 0;

		PreparedStatement psRetrieve;
		ResultSet rs;
		/*	here we compute the length for unigrams for the title field. 
		I changed the order of the two concatenated sums. In this way, we can
		spare some accesses in memory*/ 
		for(Entry<String, YosiNode> uEntry : frontier.entrySet()) {
			//sum over all the nodes in v*

			//we get the relative weight of u in v*
			YosiNode u = uEntry.getValue();
			int u_id = u.getId_();

			psRetrieve = connection.prepareStatement(SQL_RETRIEVE_STATIC_WEIGHT);
			psRetrieve.setInt(1, v_id);
			psRetrieve.setInt(2, u_id);
			rs = psRetrieve.executeQuery();
			if(rs.next()) {
				//get ws_
				ws_u = rs.getDouble(1);
			}

			//we compute the gaussian weight
			double weight = computeGaussianWeight(ws_u, ws_v, sigma);

			for(String t: contentList) {
				//sum over all the distinct words in the document v* in the content field
				//XXX NB: here we are assuming that the contentField contains all the worlds that are
				//also contained in the title field of v*.

				//check if the node contains the word t
				String title = u.getTitleField();
				String content = u.getContentField();
				MemoryIndex titleIndex = null, contentIndex = null;
				try {
					titleIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title);
					contentIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content);
				} catch (Exception e) {
					e.printStackTrace();
				}

				//for the content
				Lexicon<String> cLex = contentIndex.getLexicon();
				LexiconEntry cLe = cLex.getLexiconEntry(t);
				if(cLe!=null) {
					int cTf = cLe.getFrequency();
					double contribution = weight*cTf;
					contentSum += contribution;
				}

				//for the title
				Lexicon<String> tLex = titleIndex.getLexicon();
				LexiconEntry tLe = tLex.getLexiconEntry(t);
				if(tLe!=null) {
					int tTf = tLe.getFrequency();
					double contribution = weight*tTf;
					titleSum += contribution;
				}
			}
		}//end of the computation of title and content length for unigrams

		//add the data to the prepared statement in order to update later
		updateStatement.setDouble(1, titleSum);
		updateStatement.setDouble(2, contentSum);

	}
	
	private static void computeBigramTitleAndContentLengths(Connection connection, Map<String, YosiNode> frontier, double ws_v, 
			int v_id, double sigma, 
			PreparedStatement updateStatement, String titleDocument, String contentDocument)
					throws SQLException {
		//data structure and int for the bigrams for title and content bigram fields. Here we extract them distinct,
		//because we are going to use these sets as index for the sum
		List<Bigram> titleBigramsList = BigramUsefulMethods.extractDistinctBigramsFromString(titleDocument);
		double titleBigramSum = 0;

		List<Bigram> contentBigramsList = BigramUsefulMethods.extractDistinctBigramsFromString(contentDocument);
		double contentBigramSum = 0;

		/*we need to distinguish in general the procedure for the title
		 * and the procedure for the content field. This is necessary because, in principle, 
		 * bigrams in the title field can be different from bigrams in content field, even if the content field 
		 * includes the title field.
		 * */
		
		//iteration on all the nodes of v* for the title field
		for(Entry<String, YosiNode> uEntry : frontier.entrySet()) {
			//for each node u
			YosiNode u = uEntry.getValue();//node
			Integer u_id = u.getId_();//id of the node

			//relative static weight of the node u in v*
			double ws_u = 0;

			//computation of the gaussian weight
			PreparedStatement psRetrieve = connection.prepareStatement(SQL_RETRIEVE_STATIC_WEIGHT);
			psRetrieve.setInt(1, v_id);
			psRetrieve.setInt(2, u_id);
			ResultSet rs = psRetrieve.executeQuery();
			if(rs.next()) {
				//get ws_
				ws_u = rs.getDouble(1);
			}
			double weight = computeGaussianWeight(ws_u, ws_v, sigma);

			//for each title bigram
			for(Bigram tt : titleBigramsList) {

				//extrapolates the bigram from the title
				List<Bigram> nodeUBigramTitleList = BigramUsefulMethods.extractBigramsFromString(u.getTitleField());
				int tBTf = 0;
				if(nodeUBigramTitleList.size()>0 && tt != null) {
					tBTf = BigramUsefulMethods.getBigramFrequency(tt, nodeUBigramTitleList);
				}

				if(tBTf==0) {
					continue;
				}

				double contribution = weight*tBTf;
				titleBigramSum += contribution;
			}
		}

		//iteration on all the nodes of v* for the content field
		for(Entry<String, YosiNode> uEntry : frontier.entrySet()) {

			//for each node u
			YosiNode u = uEntry.getValue();//node
			Integer u_id = u.getId_();//id of the node

			//relative static weight of the node u in v*
			double ws_u = 0;

			//computation of the gaussian weight
			PreparedStatement psRetrieve = connection.prepareStatement(SQL_RETRIEVE_STATIC_WEIGHT);
			psRetrieve.setInt(1, v_id);
			psRetrieve.setInt(2, u_id);
			ResultSet rs = psRetrieve.executeQuery();

			if(rs.next()) {
				//get ws_
				ws_u = rs.getDouble(1);
			}
			double weight = computeGaussianWeight(ws_u, ws_v, sigma);

			//for each title bigram
			for(Bigram tt : contentBigramsList) {
				//extrapolates the bigrams from the content field of the node u
				List<Bigram> nodeUBigramContentList = BigramUsefulMethods.extractBigramsFromString(u.getContentField());
				//content bigram term frequency
				int cBTf = BigramUsefulMethods.getBigramFrequency(tt, nodeUBigramContentList);
				if(cBTf==0) {
					continue;
				}

				double contribution = weight*cBTf;
				contentBigramSum += contribution;

			}
		}
		updateStatement.setDouble(3, titleBigramSum);
		updateStatement.setDouble(4, contentBigramSum);
	}
	
	private static double computeGaussianWeight(double ws_u, double ws_v, double alpha) {
		double firstTerm = (ws_u - ws_v);
		double toTheSecond = -Math.pow(firstTerm, 2);
		double thirdTerm = (double) toTheSecond / (2*Math.pow(alpha, 2));
		double weight = Math.pow(Math.E, thirdTerm);
		return weight;
	}

	public String getVirtualGraphsDirectoryPath() {
		return virtualGraphsDirectoryPath;
	}

	public void setVirtualGraphsDirectoryPath(String virtualGraphsDirectoryPath) {
		this.virtualGraphsDirectoryPath = virtualGraphsDirectoryPath;
	}


}
