package it.unipd.dei.ims.yosi.offline;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;

/** Yosi algorithm: phase 1.
 * <p>
 * Computes some statistics.
 * <p>
 * TODO
 * NB: the execution of this class must be preceded by the execution of the 
 * Statistical phase of the Clustering algorithm.
 * 
 * 
 * */
public class YosiStatisticsCalculationPhase {

	/** Database string
	 * */
	private String jdbcConnectionString;


	private static String SQL_SELECT_SUBJECTS = "SELECT DISTINCT SUBJECT_ FROM TRIPLE_STORE LIMIT ? OFFSET ?";

	private static String SQL_GET_NEIGHBORS = "SELECT id_, subject_, predicate_, object_ from triple_store where subject_ = ?";

	private static String SQL_GET_IN_DEGREE = "SELECT in_degree from node where node_name=?";

	private static String SQL_INSERT_YOSI_NODE = "INSERT INTO public.yosi_node(" + 
			"	iri, title_, content_, degree_, static_weight_)\n" + 
			"	VALUES (?, ?, ?, ?, ?);";
	
	/** query to count the total number of nodes we are going to elaborate*/
	private static String SQL_GET_NUMBER_OF_SUBJECTS = "SELECT count(DISTINCT subject_) FROM TRIPLE_STORE;";
	
	private static String SQL_TRUNCATE_YOSI_NODE = "TRUNCATE TABLE yosi_node;";
	
	/** The schema we are currently working on*/
	private String schema;

	public YosiStatisticsCalculationPhase () {
		this.setup();
	}

	public void setup() {
		try {
			Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			this.jdbcConnectionString = map.get("jdbc.connection.string");
			this.schema = map.get("schema");
			
			SQL_SELECT_SUBJECTS = "SELECT DISTINCT SUBJECT_ FROM " + schema + 
					".TRIPLE_STORE LIMIT ? OFFSET ?";

			SQL_GET_NEIGHBORS = "SELECT id_, subject_, predicate_, object_ from "
					+ this.schema +".triple_store where subject_ = ?";

			SQL_GET_IN_DEGREE = "SELECT in_degree from " + this.schema + ".node where node_name=?";

			SQL_INSERT_YOSI_NODE = "INSERT INTO " + this.schema + ".yosi_node(" + 
					"	iri, title_, content_, degree_, static_weight_)\n" + 
					"	VALUES (?, ?, ?, ?, ?);";
			
			SQL_GET_NUMBER_OF_SUBJECTS = "SELECT count(DISTINCT subject_) FROM " + this.schema + ".TRIPLE_STORE;";
			
			SQL_TRUNCATE_YOSI_NODE = "TRUNCATE TABLE " + this.schema + ".yosi_node;";

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Calculates the statistics for the Yosi Algorithm and saves them in the corresponding 
	 * table in the database.
	 * <p>
	 * This methods computes the first statistics of the nodes, useful to run the part which builds the virtual documents.
	 * In particular, the method inserts the information in the yosi_node table. This information includes:
	 * <ul>
	 * <li>ID of the node (autoincrement value)
	 * <li>URI of the node (index on this value)
	 * <li>title (field) the title field, composed of the text extrapolated only from the URI of the node
	 * <li>content (field) the content field, text composed by words extrapolated from the URI and the
	 * literal in the neighbor of the node.
	 * <li>degree total degree (comprising in degree and out degree) of the node. Useful to compute the static weight
	 * <li>static weight Static weight of the node. Computed from the degree (maybe redundant, study if necessary)
	 * </ul>
	 * <p>
	 * This methods reads from the triple_store table to obtain all the nodes that are subjects. 
	 * We do this, instead of all the nodes, because we are in RDF domain. A true node
	 * in the Yosi paper is a node obtained from a triple. In RDF some nodes are just ausiliary information, 
	 * or literals. A node which has literals as neighbors is a real entity node. THe literals
	 * produce an additional information that serves as composition for the node document.
	 * In RDF only URI nodes are subjects and, therefore, real counterparts of entity node. 
	 * <p>
	 * In the future we could explore the possibility to consider as entity nodes only the nodes that are
	 * subjects and moreover have literals as neighbors.  
	 * <p>
	 * NB: in the construction of the graph, we are considering only nodes which are Resources (URI)
	 * and which are subjects of certin triples. There can be nodes which are URI but compare
	 * only as object inside the graph. In this case, they are deemed too small to be considered as
	 * useful information.
	 * */
	public void computeYosiStatistics() {
		Connection connection = null;

		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());

			//now we create a couple of tables just to be sure that they are there
			String createTable = 
					"CREATE TABLE if not exists " + this.schema + ".yosi_node" + 
					"(" + 
					"    id_ serial,\n" + 
					"    iri text COLLATE pg_catalog.\"default\",\n" + 
					"    content_ text COLLATE pg_catalog.\"default\",\n" + 
					"    degree_ integer,\n" + 
					"    unigram_length_title real DEFAULT 0,\n" + 
					"    unigram_length_content real DEFAULT 0,\n" + 
					"    title_ text COLLATE pg_catalog.\"default\",\n" + 
					"    static_weight_ real,\n" + 
					"    bigram_length_title real DEFAULT 0,\n" + 
					"    bigram_length_content real DEFAULT 0,\n" + 
					"    CONSTRAINT id_pkey PRIMARY KEY (id_)\n" + 
					");\n" + 
					"CREATE INDEX if not exists iri_index\n" + 
					"    ON " + this.schema + ".yosi_node USING btree\n" + 
					"    (iri COLLATE pg_catalog.\"default\")\n" + 
					"    TABLESPACE pg_default;\n" + 
					"CREATE INDEX if not exists iri_index_\n" + 
					"    ON " + this.schema + ".yosi_node USING btree\n" + 
					"    (iri COLLATE pg_catalog.\"default\")\n" + 
					"    TABLESPACE pg_default;";
			
			Statement creatingStatement = connection.createStatement();
//			creatingStatement.executeUpdate(createTable);
			
			createTable = "CREATE TABLE if not exists " + this.schema + ".yosi_virtual_document\n" + 
					"(\n" + 
					"    v_id integer,\n" + 
					"    u_id integer,\n" + 
					"    ws_ real\n" + 
					");\n" + 
					"CREATE INDEX if not exists node_in_doc\n" + 
					"    ON "+ this.schema + ".yosi_virtual_document USING btree\n" + 
					"    (v_id, u_id)\n" + 
					"    TABLESPACE pg_default;";
			
			creatingStatement = connection.createStatement();
//			creatingStatement.executeUpdate(createTable);
			
			System.out.println("Created MRF-KS tables if they did not exist");
			
			
			
			//clean the table yosi_node
			Statement cleaningStatement = connection.createStatement();
			cleaningStatement.executeUpdate(SQL_TRUNCATE_YOSI_NODE);
			System.out.println("truncated table yosi_node successfully");
			
			//get the number of different subjects (do not be put out of road with the name of the variable)
			Statement numberOfTriplesStatement = connection.createStatement();
			ResultSet numberOfTriples = numberOfTriplesStatement.executeQuery(SQL_GET_NUMBER_OF_SUBJECTS);
			numberOfTriples.next();
			int numTriples = numberOfTriples.getInt(1);
			
			
			int limit = 100000;
			int offset = 0;

			int totalCount = 0;
			int counter = 0;

			while(true) {
				//read the triple_store table to study the subjects
				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, limit, offset, SQL_SELECT_SUBJECTS);
				offset += limit;
				if(iterator.first()) {//if we still have nodes to read

					//get the cursor to the begin
					iterator.beforeFirst();
					PreparedStatement insertPrepared = connection.prepareStatement(SQL_INSERT_YOSI_NODE);

					while(iterator.next()) {
						//for each node in this window over the table
						
						//check if we can continue
						if(Thread.interrupted() || !ThreadState.isOffLine()) {
							ThreadState.setOffLine(false);
							return;
						}

						//get the subject
						String subject = iterator.getString(1);
						//the title field, obtained from the URI of the node.
						String title = UrlUtilities.takeWordsFromIri(subject);
						//content field. In Yosi it was created as
						//attribute name : attribute content.
						//we add the strings taken from the attributes and
						//the values of the literal nodes. 
						String content = title;

						//get information about the neighbors of the subject node
						//get the neighbors
						PreparedStatement ps = connection.prepareStatement(SQL_GET_NEIGHBORS);

						ps.setString(1, subject);
						ResultSet rs = ps.executeQuery();

						int outDegree = 0;

						//here we only create the documents of the single node and the first statistics
						while(rs.next()) {
							//for each neighbor

							//get the URI of the neighbor 
							String iriPredicate = rs.getString(3);
							String iriNeighbor = rs.getString(4);

							if(UrlUtilities.checkIfValidURL(iriNeighbor)) {
								//this is a proper neighbor
								//increment the out degree
								outDegree++;
							} else {
								//this is a literal. We add the content to the node content

								content = content + " " + 
										UrlUtilities.takeWordsFromIri(iriPredicate) + " " + 
										BlazegraphUsefulMethods.dealWithTheObjectLiteralString(iriNeighbor).stringValue();
							}

						}
						//close the iterators
						ps.close();
						rs.close();

						//get the in degree of the node (in order to have the complete degree)
						ps = connection.prepareStatement(SQL_GET_IN_DEGREE);
						ps.setString(1, subject);
						rs = ps.executeQuery();
						boolean b = rs.next();
						int inDegree = rs.getInt(1);

						//sum the in degree with the out degree of only URI nodes
						int degree = outDegree + inDegree;

						//now we can insert in the table yosi_node the information regarding this node


						//IRI
						insertPrepared.setString(1, subject);
						//title_
						insertPrepared.setString(2, title);
						//content_
						insertPrepared.setString(3, content);
						//degree
						insertPrepared.setInt(4, degree);
						//static_weight_
						double sw = (double) 1/ Math.log(Math.E + degree);;
						insertPrepared.setDouble(5, sw);
						
						insertPrepared.addBatch();
						counter++;
						
						if(counter>=1000) {
							//execute a batch of insertions
							insertPrepared.executeBatch();
							totalCount += counter;
							counter = 0;
							insertPrepared.clearBatch();
							System.out.println("inserted " + totalCount + " vertices of " + numTriples);
						}
					}
					
					if(counter>0) {
						//insert the last remaining records
						insertPrepared.executeBatch();
						totalCount += counter;
						counter=0;
						insertPrepared.clearBatch();
						System.out.println("inserted " + totalCount + " vertices (last time)");
					}
					
				} else {
					//end of the whole table
					break;
				}

			}//end of the while(true)

		} catch (SQLException e) {
			System.err.println("Error in connecting to " + this.jdbcConnectionString);
			e.printStackTrace();
		} finally {
			if(connection!=null)
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}
