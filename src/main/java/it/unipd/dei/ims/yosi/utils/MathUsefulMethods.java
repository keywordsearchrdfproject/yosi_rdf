package it.unipd.dei.ims.yosi.utils;

public class MathUsefulMethods {

	/** Computes the Gaussian weight used in the weighted term frequency as described
	 * by Yosi. 
	 * That is:
	 * e^exp, where exp is (-(ws(v*, u) - ws(v*, v)^2))/(2*sigma^2)*/
	public static double computeGaussianWeight(double ws_u, double ws_v, double sigma) {
		double firstTerm = (ws_u - ws_v);
		double toTheSecond = -Math.pow(firstTerm, 2);
		double thirdTerm = (double) toTheSecond / (2*Math.pow(sigma, 2));
		double weight = Math.pow(Math.E, thirdTerm);
		return weight;
	}
}
