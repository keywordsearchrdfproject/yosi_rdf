package it.unipd.dei.ims.yosi.datastructures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.rum.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;

/** This class represents a node inside a virtual document v* */
public class YosiNode {
	/** Content of the string (an URI)*/
	private String stringContent;
	
	private String iri;
	
	/**The content of the node. */
	private Value content;
	
	/** Distance of the node from the center of the virtual document.
	 * Set to infinity at the beginning for each node.
	 * */
	private double distance = Double.MAX_VALUE;
	
	/** Id of the node, given by the RDB*/
	private int id_;
	
	/**Distance from the central node as a simple count of the edges (as if it were
	 * an unweighted graph)*/
	private int radius;
	
	/** This string represents the predicate connecting the node to the previous one 
	 * in the RDF graph.*/
	private String predicate;
	
	/** The statement in which this node is the object. */
	private Statement triple;
	
	/** The title field as described by Yosi 2016*/
	private String titleField = "";
	
	/** The content field as described by Yosi*/
	private String contentField = "";
	
	/** The score of the node. Used to rank the node when we are ranking
	 * roots and keynodes.
	 * */
	private double score;
	
	/**one possible keyword contained by this node (if present )*/
	private String keyword;
	
	/** The previous node in the shortest path*/
	private YosiNode previousNode;
	
	/** Id of the parent node of this node. Used in certain scenarios to set who
	 * is the parent of this node and don't change the previousNode field.
	 * 
	 * */
	private int parentId;
	
	/** The weight of the node */
	private double weight;
	
	private List<String> keywords;
	
	private List<Statement> listOfLiteralNeighbors;
	
	public YosiNode () {
		keywords = new ArrayList<String>();
		listOfLiteralNeighbors = new ArrayList<Statement>();
	}
	
	public YosiNode(String content) {
		this.stringContent = content;
		keywords = new ArrayList<String>();
		listOfLiteralNeighbors = new ArrayList<Statement>();
	}
	
	public YosiNode(String content, double distance) {
		this.stringContent = content;
		this.distance = distance;
		keywords = new ArrayList<String>();
		listOfLiteralNeighbors = new ArrayList<Statement>();
	}
	
	public YosiNode(Value content, double distance) {
		this.content = content;
		this.distance = distance;
		keywords = new ArrayList<String>();
		listOfLiteralNeighbors = new ArrayList<Statement>();
	}

	
	/** Copy constructor */
	public YosiNode(YosiNode another) {
		super();
		
		this.stringContent = another.stringContent;
		this.content = another.content;
		this.distance = another.distance;
		this.id_ = another.id_;
		this.radius = another.radius;
		this.predicate = another.predicate;
		this.triple = another.triple;
		this.titleField = another.titleField;
		this.contentField = another.contentField;
		this.score = another.score;
		this.keyword = another.keyword;
		this.previousNode = another.previousNode;
	}
	
	
	public String getStringContent() {
		return stringContent;
	}

	public void setStringContent(String stringContent) {
		this.stringContent = stringContent;
	}

	public Value getContent() {
		return content;
	}

	public void setContent(Value content) {
		this.content = content;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public int getId_() {
		return id_;
	}

	public void setId_(int id_) {
		this.id_ = id_;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public String getPredicate() {
		return predicate;
	}

	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}

	public Statement getTriple() {
		return triple;
	}

	public void setTriple(Statement triple) {
		this.triple = triple;
	}
	
	public String toString() {
		return "id: " + this.id_ + "; distance: " + distance + "; radius " + radius + "; score: " + this.score;
		
	}

	public String getTitleField() {
		return titleField;
	}

	public void setTitleField(String titleField) {
		this.titleField = titleField;
	}

	public String getContentField() {
		return contentField;
	}

	public void setContentField(String contentField) {
		this.contentField = contentField;
	}
	
	/** Adds the provided string to the title of this node after a space*/
	public void addToTitleField(String add) {
		this.titleField = this.titleField + " " + add;
	}
	
	public void addToContentField(String add) {
		this.contentField = this.contentField + " " + add;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	/** returns true the the id of the parameter is equals to
	 * the Id of this node.
	 * */
	public boolean equalById(YosiNode second) {
		if(this.getId_() == second.getId_())
			return true;
		return false;
	}
	
	public boolean equalByContentString(YosiNode second) {
		if(this.getStringContent().equals(second.getStringContent())) {
			return true;
		}
		return false;
	}

	public YosiNode getPreviousNode() {
		return previousNode;
	}

	public void setPreviousNode(YosiNode previousNode) {
		this.previousNode = previousNode;
	}

	public YosiNode(String stringContent, Value content, double distance, int id_, int radius, String predicate,
			Statement triple, String titleField, String contentField, double score, String keyword,
			YosiNode previousNode) {
		super();
		this.stringContent = stringContent;
		this.content = content;
		this.distance = distance;
		this.id_ = id_;
		this.radius = radius;
		this.predicate = predicate;
		this.triple = triple;
		this.titleField = titleField;
		this.contentField = contentField;
		this.score = score;
		this.keyword = keyword;
		this.previousNode = previousNode;
	}
	
	/** Copies a list of objects inside a new list. The
	 * two lists are separated and independent
	 * */
	public static List<YosiNode> copyListOfYosiNodes(List<YosiNode> originalList) {
		
		List<YosiNode> returningList = new ArrayList<YosiNode>();
		
		for(YosiNode o : originalList) {
			YosiNode n = new YosiNode(o);
			returningList.add(n);
		}
		
		return returningList;
	}
	
	/** Given a map with list of YosiNode as elements, returns a list with the distinct set of YosiNodes.
	 * 
	 * */
	public static List<YosiNode> getListOfDistinctNodesFromMap(Map<String, List<YosiNode>> map) {
		List<YosiNode> returningList = new ArrayList<YosiNode>();
		for(Entry<String, List<YosiNode>> entry : map.entrySet()) {
			List<YosiNode> queryList = entry.getValue();
			for(YosiNode node : queryList) {
				//check that this element is not already contained in the list
				if(!YosiNode.checkDuplicateInListByStringContent(node, returningList)) {
					returningList.add(node);
				}
			}
		}
		return returningList;
	}
	
	/** Given a map with list of YosiNode as elements, returns a list with the distinct set of YosiNodes.
	 * <p>
	 * This methods takes a map which contains keyword for keys and list
	 * of YosiNodes as values. This map represent the various sets U_ri.
	 * The returned value will be a map which will be the union 
	 * of all the lists U_ri. This map will contain as values the keyword nodes inside r* identified by their IRI.
	 * <p>
	 * Also, the method writes in the 'keywords' field of each node, writing the list of keywords matched from that node.
	 * 
	 * */
	public static Map<String, YosiNode> getMapOfDistinctNodesFromMap(Map<String, List<YosiNode>> map) {
		Map<String, YosiNode> returningMap = new HashMap<String, YosiNode>();
		//for each U_ri
		for(Entry<String, List<YosiNode>> entry : map.entrySet()) {
			//get the query word and the list U_ri
			String queryword = entry.getKey();
			List<YosiNode> queryList = entry.getValue();
			for(YosiNode node : queryList) {
				//check that this element is not already contained in the list (we don0t want repeated nodes)
				if(!returningMap.containsKey(node.getIri())) {
					//enrich the node informing it of its keyword
					node.getKeywords().add(queryword);
					//insert the node
					returningMap.put(node.getIri(), node);
				} else {
					//we need to update the keywords
					YosiNode n = returningMap.get(node.getIri());
					n.getKeywords().add(queryword);
				}
			}
		}
		return returningMap;
	}
	
	/** Checks if a list contains a YosiNode or not by String Content.
	 * */
	private static boolean checkDuplicateInListByStringContent(YosiNode node, List<YosiNode> list) {
		for(YosiNode listNode : list) {
			if(listNode.equalByContentString(node))
				return true;
		}
		return false;
	}

	public String getIri() {
		return iri;
	}

	public void setIri(String iri) {
		this.iri = iri;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}
	
	/** returns a string representing the predicate of 
	 * the connecting triple of this node parsed with terrier.
	 * */
	public String getConnectingPredicateAsString() {
		Statement t = this.getTriple();
		if(t!=null) {
			String s = t.getPredicate().toString();
			//be sure that we are using the simple tokeniser
			ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
			s = UrlUtilities.takeWordsFromIri(s);
			return TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(s);
		}
		else
			return "";
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public List<Statement> getListOfLiteralNeighbors() {
		return listOfLiteralNeighbors;
	}

	public void setListOfLiteralNeighbors(List<Statement> listOfLiteralNeighbors) {
		this.listOfLiteralNeighbors = listOfLiteralNeighbors;
	}
	
	public void addNeighbour(Statement t) {
		this.listOfLiteralNeighbors.add(t);
	}
	
	
	
}
