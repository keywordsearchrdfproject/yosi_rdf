package it.unipd.dei.ims.yosi.online;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.utility.ApplicationSetup;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiDataWrapper;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;
import it.unipd.dei.ims.yosi.datastructures.YosiNodeScoreComparator;
import it.unipd.dei.ims.yosi.utils.YosiNodeStaticScoringFunction;

/** Yosi algorithm: phase 6 (online)
 * <p>
 * After the creation of a ranked list of roots, the top-n roots, here 
 * we select the top-n keyword nodes to create the set S. 
 * With this set, we will then create the result subgraphs.
 * 
 * */
public class TopKeywordNodeSelectionPhase {

	private static final String SQL_RETRIEVE_VIRTUAL_DOCUMENT_NODES = "SELECT u_id FROM "
			+ DatabaseState.getSchema() +".yosi_virtual_document where v_id = ?";

	private static final String SQL_GET_NODE_FIELDS = "SELECT title_, content_ FROM "
			+ DatabaseState.getSchema() + ".yosi_node where id_ = ?";

	private static final String SQL_GET_STATIC_RELATIVE_WEIGHT = "SELECT v_id, u_id, ws_ FROM "
			+ DatabaseState.getSchema() + ".yosi_virtual_document "
			+ " where v_id=? and u_id=?";

	private static final String SQL_GET_VIRTUAL_DOCUMENT_NODES = "select v_id, u_id, ws_ "
			+ "from " + DatabaseState.getSchema() + ".yosi_virtual_document where v_id=?";

	private static final String SQL_GET_LENGTHS = "SELECT unigram_length_title, unigram_length_content, "
			+ "bigram_length_title, bigram_length_content from "
			+ DatabaseState.getSchema() + ".yosi_node where id_ = ?";


	private String jdbcConnectionString;

	private YosiDataWrapper parameters;

	/** File where the ordered roots are saved*/
	private String rootNodesFilePath;

	/** Number of top roots and top keyword nodes to retrieve
	 * */
	private int n;

	/** Query we are answering*/
	private String query;

	private String virtualDocumentsUnigramIndexPath;
	private String nodeVirtualDocumentsUnigramIndexPath;
	private String virtualDocumentsBigramIndexPath;
	private String nodeVirtualDocumentsBigramIndexPath;

	/** File where to save the keyword nodes ordered*/
	private String keywordNodesFilePath;

	private double lambdaUnigramTitle, lambdaUnigramContent, 
	lambdaBigramTitle, lambdaBigramContent, 
	lambdaQueryIndependent,
	sigma;

	private Connection connection;

	public TopKeywordNodeSelectionPhase() {
		
		this.setup();
	}

	private void setup() {
		try {
			Map<String, String> map = 
					PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			//get the terrier home and etc directory where we have the property file and set them
			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);

			this.jdbcConnectionString = map.get("jdbc.connection.string");
			this.rootNodesFilePath = map.get("root.nodes.file.path");

			this.n = Integer.parseInt(map.get("n"));

			this.query = map.get("query");

			this.virtualDocumentsUnigramIndexPath = map.get("virtual.documents.unigram.index.path");
			this.nodeVirtualDocumentsUnigramIndexPath = map.get("node.virtual.documents.unigram.index.path");
			this.virtualDocumentsBigramIndexPath = map.get("virtual.documents.bigram.index.path");
			this.nodeVirtualDocumentsBigramIndexPath = map.get("node.virtual.documents.bigram.index.path");
			
			this.keywordNodesFilePath = map.get("keyword.nodes.file.path");

			lambdaUnigramTitle = Double.parseDouble(map.get("lambda.unigram.title"));
			lambdaUnigramContent = Double.parseDouble(map.get("lambda.unigram.content"));
			lambdaBigramTitle= Double.parseDouble(map.get("lambda.bigram.title"));
			lambdaBigramContent = Double.parseDouble(map.get("lambda.bigram.content"));
			lambdaQueryIndependent = Double.parseDouble(map.get("lambda.query.independent"));

			//sigma
			sigma = Double.parseDouble(map.get("sigma"));


			//prepareParametersObject();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**given the validated fields, this method prepares an object to
	contain a series of other object derived (e.g. indexes)
	*/
	public void prepareParametersObject() {
		parameters = new YosiDataWrapper();
		
		Index virtualDocumentsUnigramIndex = IndexOnDisk.createIndex(virtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramVirtualDocumentsIndex(virtualDocumentsUnigramIndex);

		Index nodeVirtualDocumentsUnigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramNodeIndex(nodeVirtualDocumentsUnigramIndex);

		Index virtualDocumentsBigramIndex = IndexOnDisk.createIndex(virtualDocumentsBigramIndexPath, "data");
		parameters.setBigramVirtualDocumentsIndex(virtualDocumentsBigramIndex);

		Index nodeVirtualDocumentsBigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsBigramIndexPath, "data");
		parameters.setBigramNodeIndex(nodeVirtualDocumentsBigramIndex);

		parameters.setSigma(sigma);
		parameters.setRootsOutputFile(this.rootNodesFilePath);
		parameters.setQuery(query);
		parameters.setN(n);

		parameters.setLambdaBigramContent(lambdaBigramContent);
		parameters.setLambdaBigramTitle(lambdaBigramTitle);
		parameters.setLambdaUnigramContent(lambdaUnigramContent);
		parameters.setLambdaUnigramTitle(lambdaUnigramTitle);
		parameters.setLambdaQueryIndependent(lambdaQueryIndependent);

		parameters.setKeywordsOutputFile(keywordNodesFilePath);
		try {
			parameters.setConnection(ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName()));
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void computeTopNKeywordNodes() {
		connection = null;
		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());

			Map<String, List<Integer>> USetsMap = this.computeUSets();

			if(ThreadState.isOnLine())
				this.keywordNodesRankingPhase(USetsMap, connection, parameters);

			//close the indexes
			parameters.getUnigramNodeIndex().close();
			parameters.getBigramNodeIndex().close();
			parameters.getUnigramVirtualDocumentsIndex().close();
			parameters.getBigramVirtualDocumentsIndex().close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/** Provided the set of roots, this method retrieves the nodes that belong to the
	 * virtual documents r*, finds the ones matching some keyword q_i, 
	 * and returns them divided in sets depending on the
	 * queryword q_i that they contain. 
	 * These are the so called sets U_i, in the Yosi paper.
	 * 
	 * @param path a string with the path of the file containing the IDs of the root nodes in ranked order.
	 * @param n the number of top elements to be chosen.
	 * @throws IOException 
	 * */
	private Map<String, List<Integer>> computeUSets() throws IOException {
		Map<String, List<Integer>> USetsMap = new HashMap<String, List<Integer>>();

		//read the information about the roots from the file
		Path inputPath = Paths.get(this.rootNodesFilePath);
		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)) {
			String line = "";
			//set the tokeniser we want to use, single-term
			ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
			System.setProperty("tokeniser", "EnglishTokeniser");

			//get the query words
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);

			//initialize the U_i sets, one for every query word q_i
			for(String q_i : queryWords) {
				List<Integer> U_i = new ArrayList<Integer>();
				USetsMap.put(q_i, U_i);
			}

			File xmlRootFile = new File(this.rootNodesFilePath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlRootFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("root");

			//for each root r in the file
			for(int i = 0; i < nList.getLength(); ++i) {
				Node rootNode = nList.item(i);
				int rootId = Integer.parseInt(rootNode.getTextContent());

				//now use the root id to get all the nodes of the virtual document r*
				PreparedStatement pStmt = connection.prepareStatement(SQL_RETRIEVE_VIRTUAL_DOCUMENT_NODES);
				pStmt.setInt(1, rootId);
				ResultSet rs = pStmt.executeQuery();

				while(rs.next()) {
					
					if(Thread.interrupted()) {
						ThreadState.setOnLine(false);
						return USetsMap;
					}
					//for each node u of the virtual document r*
					String uId = rs.getString("u_id");
					//now we need to check the content of u, and understand if it contains at least one query word
					PreparedStatement nodeFieldPS = connection.prepareStatement(SQL_GET_NODE_FIELDS);
					nodeFieldPS.setInt(1, Integer.parseInt(uId));
					ResultSet nodeFieldRS = nodeFieldPS.executeQuery();

					//tke the content field of the node. Because we suppose that the content field
					//includes the title field, we take only the first one.
					nodeFieldRS.next();
					String uContent = nodeFieldRS.getString("content_");

					//now get an index for the content
					MemoryIndex contentIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(uContent);
					Lexicon<String> uContentLex = contentIndex.getLexicon();

					for(String s : queryWords) {
						//for every query word, check if the node u contains the query word
						LexiconEntry le = uContentLex.getLexiconEntry(s);
						if(le!=null) {
							//if this node u contains the query word s, we can add it to U_i

							//take the set U_i
							List<Integer> U_i = USetsMap.get(s);
							//add the id of node u to this list if it's not already present
							//this can happen when a v* and a u* are overlapped
							int uID = Integer.parseInt(uId);
							if(!U_i.contains(uID))
								U_i.add(Integer.parseInt(uId));
						}
					}
					contentIndex.close();
				}
			}
			return USetsMap;

		} catch (SQLException e) {
			e.printStackTrace();
			e.getNextException().printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/** Given the sets U_i of keyword nodes, the method ranks them and
	 * prints them in a file whose path must be provided in the parameters
	 * object.
	 * 
	 * @param USetsMap map containing, for each query word, the corresponding set U_i.
	 * */
	private  void keywordNodesRankingPhase(Map<String, List<Integer>> USetsMap, 
			Connection connection, 
			YosiDataWrapper parameters) {

		//we need the total degree of the graph as information for further phases
		String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from " + DatabaseState.getSchema() + ".yosi_node";
		PreparedStatement totalDegreeStatement;
		int totalDegree = 0;
		BufferedWriter writer;
		Path outputPath;

		try {
			//get the total degree from the database
			totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet totalDegreeRS = totalDegreeStatement.executeQuery();
			if(totalDegreeRS.next()) {
				totalDegree = totalDegreeRS.getInt(1);
			}

			//open the file where we will write down the results
			outputPath = Paths.get(keywordNodesFilePath);
			writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			writer.write("<keynodes>");
			writer.newLine();

			parameters.setTotalDegree(totalDegree);

			//in the map we have all the sets U_i. We can score the nodes in each U_i. 
			//Then, we will take the top n of them. 

			//for each U_i, that is, for each query word
			for(Entry<String, List<Integer>> entry : USetsMap.entrySet()) {
				//take the list of nodes
				List<Integer> U_i = entry.getValue();
				//for each node in U_i

				//max heap to score the nodes 
				PriorityQueue<YosiNode> queue_i = new PriorityQueue<YosiNode>(new YosiNodeScoreComparator());

				for(int uID : U_i) {//for every node in the set U_i
					//we now rank this keyword node
					//do it in the same way of the roots

					//uId is the ID of the node u inside U_i, which contains the keyword q_i.
					//we compute now the score(Q, u_i)
					double score_u_i = YosiNodeStaticScoringFunction.computeKeywordNodeScore(uID, 0, connection, query, parameters);

					//create an object YosiNode with the id and the score just computed
					YosiNode u_i = new YosiNode();
					u_i.setId_(uID);
					u_i.setScore(score_u_i);

					queue_i.add(u_i);
				} //ended the computation for the nodes of a single U_i
				//now we have ended the scoring of the nodes in U_i. We print the first n of them.
				//write the query
				for(int i = 0; i < n; ++i) {
					YosiNode node = queue_i.poll();
					//control in case n is greater than the number of nodes
					if(node==null)
						break;
					writer.write("\t\t<node queryword=\""+ entry.getKey() + "\" score=\"" + node.getScore() + "\">" + node.getId_() + "</node>");
					writer.newLine();
				}
				writer.flush();
				//empty the queue if there are nodes left
				queue_i.clear();

			}//done for all U_i
			writer.write("</keynodes>");
			writer.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRootNodesFilePath() {
		return rootNodesFilePath;
	}

	public void setRootNodesFilePath(String rootNodesFilePath) {
		this.rootNodesFilePath = rootNodesFilePath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getKeywordNodesFilePath() {
		return keywordNodesFilePath;
	}

	public void setKeywordNodesFilePath(String keywordNodesFilePath) {
		this.keywordNodesFilePath = keywordNodesFilePath;
	}

	public String getVirtualDocumentsUnigramIndexPath() {
		return virtualDocumentsUnigramIndexPath;
	}

	public void setVirtualDocumentsUnigramIndexPath(String virtualDocumentsUnigramIndexPath) {
		this.virtualDocumentsUnigramIndexPath = virtualDocumentsUnigramIndexPath;
	}

	public String getNodeVirtualDocumentsUnigramIndexPath() {
		return nodeVirtualDocumentsUnigramIndexPath;
	}

	public void setNodeVirtualDocumentsUnigramIndexPath(String nodeVirtualDocumentsUnigramIndexPath) {
		this.nodeVirtualDocumentsUnigramIndexPath = nodeVirtualDocumentsUnigramIndexPath;
	}

	public String getVirtualDocumentsBigramIndexPath() {
		return virtualDocumentsBigramIndexPath;
	}

	public void setVirtualDocumentsBigramIndexPath(String virtualDocumentsBigramIndexPath) {
		this.virtualDocumentsBigramIndexPath = virtualDocumentsBigramIndexPath;
	}

	public String getNodeVirtualDocumentsBigramIndexPath() {
		return nodeVirtualDocumentsBigramIndexPath;
	}

	public void setNodeVirtualDocumentsBigramIndexPath(String nodeVirtualDocumentsBigramIndexPath) {
		this.nodeVirtualDocumentsBigramIndexPath = nodeVirtualDocumentsBigramIndexPath;
	}
}
