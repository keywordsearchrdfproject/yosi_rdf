package it.unipd.dei.ims.yosi.main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.jena.ext.com.google.common.base.Stopwatch;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** An extension of the entry point YosiMainClass that 
 * provides the execution of the off-line and on-line phases of the
 * Yosi system with timer to control the execution.
 * */
public class MRFKSControlRoomWithTimer extends YosiMainClass{

	/** set to true if we require the execution of the off-line 
	 * part of the MRF-KS algorithm. 
	 * */
	private boolean isOffline = true;

	/** provided time for the off-line phase in hours*/
	private int offLineTime;

	/** provided time for the on-line time phase in minutes*/
	private int onLineTime;

	/**builder of the class. It takes care to set all the parameters
	 * reading them from the properties/main.properties
	 * file
	 * 
	 * 
	 * */
	public MRFKSControlRoomWithTimer() {
		//setup all the parameters in the main class
		super();

		//set the kind of database we are using.
		//this is necessary in order to correctly translate graphs into documents
		String dbName = map.get("rdf.database.name");
		DatabaseState.setType(dbName); 
		
		String time = map.get("on.line.time");
		time = (time != null) ? time : "1000";
		this.onLineTime = Integer.parseInt(time);
		
		time = map.get("off.line.time");
		time = (time != null) ? time : "24";
		this.offLineTime = Integer.parseInt(time);
		
		this.isOffline = Boolean.parseBoolean(map.get("is.offline"));
	}



	public void offLinePhaseWithTimer() {
		//we create am ExecutorService to deal with this thing
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = null;

		try {
			ConnectionHandler.createConnectionAsOwner(map.get("jdbc.connection.string"), this.getClass().getName());

			//we have the authorization to execute
			ThreadState.setOnLine(true);
			
			//now create a Future object to produce our thread
			future = executor.submit(new OfflineTask(this));

			//give the execution 24h of time to complete
//			String s  = future.get(10, TimeUnit.SECONDS);
			String s = future.get(this.offLineTime, TimeUnit.HOURS);
			System.out.println(s);
			System.out.print("\n\n–––––––––––––––––––––––––––\n\n");
			timeWriter.write(s);
			timeWriter.newLine();
			timeWriter.flush();
			timeWriter.close();

		} catch (SQLException e) {
			ThreadState.setOffLine(false);//cancel the authorization to execute
			System.err.println("error in connection to the database in the " + this.getClass().getName() + " class "
					+ "with string: " + map.get("jdbc.connection.string"));
			e.printStackTrace();
		} catch (IOException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);//cancel the authorization to execute
			System.err.print("problems with writing the file with the times");
			e.printStackTrace();
		} catch (InterruptedException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);//cancel the authorization to execute
			System.out.println("there was an InterruptedException during the off-line"
					+ " phase of MRF-KS");
			e.printStackTrace();
		} catch (ExecutionException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);//cancel the authorization to execute
			System.out.println("there was an ExecutionException during the off-line phase of MRF-KS");
			e.printStackTrace();
		} catch (TimeoutException e) {
			future.cancel(true);
			ThreadState.setOffLine(false);//cancel the authorization to execute
			System.out.println("the off-line phase of MRF-KS could not terminate in time!");
		} finally {
			try {
				ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			} catch (SQLException e) {
				System.err.println("error in closing the connection at the end of the off-line phase");
				e.printStackTrace();
			}
		}

		executor.shutdownNow();
	}

	/** This task represent the thread that will attempt the off-line phase 
	 * of TSA.
	 * */
	private class OfflineTask implements Callable<String> {

		private MRFKSControlRoomWithTimer execution;

		/**Constructor to get the reference to our current execution
		 * with all the data we need.
		 * */
		public OfflineTask(MRFKSControlRoomWithTimer exec) {
			this.execution = exec;
		}

		@Override
		public String call() throws Exception {
			//here we execute our off-line computations
			System.out.println("starting the execution of the off-line phase");
			Stopwatch offFlineTimer = Stopwatch.createStarted();
			//simply call the offlinePhase() method that we have in the MainClass
			this.execution.offlinePhase();
			return "TIME off-line " + offFlineTimer.stop();
		}

	}

	/** This method simply calls each of the phases implemented in 
	 * YosiMainClass that perform the off-line phase.
	 * */
	private void offlinePhase() {
		if(ThreadState.isOffLine())
			this.phase0();
		
		if(ThreadState.isOffLine())
			this.phase1();
		
		if(ThreadState.isOffLine())
			this.phase2();
		
		if(ThreadState.isOffLine())
			this.phase3();
		
		if(ThreadState.isOffLine())
			this.phase4();
	}

	/** This method execute the on-line phase, executing 
	 * once per query provided in the queries.properties
	 * file.
	 * 
	 * In particular, the method performs 4 different phases, one for each 
	 * pipeline.
	 * */
	protected void onlinePhaseWithTimer() {
		//first of all, get the list of queries
		String queryPath = this.map.get("query.file");
		try {
			Map<String, String> queryMap = PropertiesUsefulMethods.getSinglePropertyFileMap(queryPath);

			//setup the connection to the database once and for all, we will close it 
			//at the end of the execution
			ConnectionHandler.createConnectionAsOwner(map.get("jdbc.connection.string"), this.getClass().getName());

			//now for each query
			for(Entry<String, String> entry : queryMap.entrySet()) {
				ThreadState.setOnLine(true);
				
				//set the information of this query
				String queryId = entry.getKey();
				String query = entry.getValue();
				this.setQueryId(queryId);
				this.setQuery(query);

				this.executeOnLinePhaseWithTimer();
			}
			
			timeWriter.close();

		} catch (IOException e) {
			System.err.println("Unable to read the query file " + queryPath);
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("Unable to connect to the database "
					+ "with the jdbc driver: " + map.get("jdbc.connection.string"));
			e.printStackTrace();
		} finally {
			try {
				ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
			} catch (SQLException e) {
				System.err.println("error closing the connection to the database");
				e.printStackTrace();
			}
		}

	}

	/** Executes the on-line phase for only one set query 
	 * with a maximum limit on the required time.
	 * */
	private void executeOnLinePhaseWithTimer() {
		//create the thread that will take care of this query
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(new OnLinePhaseTask(this));

		try {
			//execute the operation and report the time used
			String s = future.get(this.onLineTime, TimeUnit.SECONDS);
			System.out.println(s);
			timeWriter.write(s);
			timeWriter.newLine();
			timeWriter.flush();
		} catch (TimeoutException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("TIME MRF-KS " + this.queryId + " -1");
		} catch (InterruptedException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("there was an InterruptedException during the MRF-KS on-line phase");
			e.printStackTrace();
		} catch (ExecutionException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.out.println("there was an ExecutionException during the MRF-KS on-line phase");
			e.printStackTrace();
		} catch (IOException e) {
			future.cancel(true);
			ThreadState.setOnLine(false);
			System.err.println("error with the writer in the MRF-KS on-line phase");
			e.printStackTrace();
		}

		executor.shutdownNow();
	}

	private class OnLinePhaseTask implements Callable<String> {
		/**link to the current execution*/
		private MRFKSControlRoomWithTimer execution;
		
		/**Constructor to get the reference to our current execution
		 * with all the data we need.
		 * */
		public OnLinePhaseTask(MRFKSControlRoomWithTimer exec) {
			this.execution = exec;
		}
		
		@Override
		public String call() throws Exception {
			System.out.println("starting the execution of the MRF-KS pipeline for query " + this.execution.queryId);
			Stopwatch timer = Stopwatch.createStarted();
			/*the boolean true is a legacy from previous
			 * implementations. It is necessary in order
			 * for the method to set the new query and queryId
			 * */
			this.execution.onLinePhase(true);//simply call the method of the parent class
			return "TIME MRF-KS " + this.execution.queryId + " " + timer.stop();
		}
	}
	
	/** Simply calls all the methods that made up the on-line phase
	 * for one query. */
	private void onLinePhase(boolean multipleQueries) {
		try {
			if(ThreadState.isOnLine())
				this.phase5(multipleQueries);
			if(ThreadState.isOnLine())
				this.phase6(multipleQueries);
			if(ThreadState.isOnLine())
				this.phase7(multipleQueries);
			if(ThreadState.isOnLine())
				this.phase8(multipleQueries);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/** This main is the starting point of all the project.
	 * With the parameters in the file main.properties you can
	 * command the execution of this class.
	 * 
	 * */
	public static void main(String[] args) {
		MRFKSControlRoomWithTimer execution = new MRFKSControlRoomWithTimer();

		if(execution.isOffline()) {
			execution.offLinePhaseWithTimer();
		} else {
			execution.onlinePhaseWithTimer();
		}
	}



	public boolean isOffline() {
		return isOffline;
	}



	public void setOffline(boolean isOffline) {
		this.isOffline = isOffline;
	}



	public int getOffLineTime() {
		return offLineTime;
	}



	public void setOffLineTime(int offLineTime) {
		this.offLineTime = offLineTime;
	}



	public int getOnLineTime() {
		return onLineTime;
	}



	public void setOnLineTime(int onLineTime) {
		this.onLineTime = onLineTime;
	}

}
