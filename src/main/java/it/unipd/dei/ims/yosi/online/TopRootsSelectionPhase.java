package it.unipd.dei.ims.yosi.online;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiDataWrapper;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;
import it.unipd.dei.ims.yosi.datastructures.YosiNodeScoreComparator;
import it.unipd.dei.ims.yosi.utils.YosiNodeStaticScoringFunction;

/** Yosi algorithm: phase 5. (online)
 * <p>
 * First of the online sequence.
 * <p>
 * Ranks the roots of the virtual documents with the scoring function provided
 * by Yosi 2016.
 * */
public class TopRootsSelectionPhase {

	private String query;
	
	private String jdbcConnectionString;
	
	private String virtualDocumentsUnigramIndexPath;
	private String nodeVirtualDocumentsUnigramIndexPath;
	private String virtualDocumentsBigramIndexPath;
	private String nodeVirtualDocumentsBigramIndexPath;
	
	private int n;
	
	private int totalDegree;
	
	private double lambdaUnigramTitle, lambdaUnigramContent, 
	lambdaBigramTitle, lambdaBigramContent, 
	lambdaQueryIndependent,
	sigma;
	
	private String rootFile;
	
	private String schema;

	public TopRootsSelectionPhase() {
		totalDegree = 0;
		
		this.setup();
	}

	private void setup() {
		Map<String, String> map;
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);
			
			this.jdbcConnectionString = map.get("jdbc.connection.string");
			
			this.schema = map.get("schema");
			this.query = map.get("query");
			
			//reading from the properties
			lambdaUnigramTitle = Double.parseDouble(map.get("lambda.unigram.title"));
			lambdaUnigramContent = Double.parseDouble(map.get("lambda.unigram.content"));
			lambdaBigramTitle= Double.parseDouble(map.get("lambda.bigram.title"));
			lambdaBigramContent = Double.parseDouble(map.get("lambda.bigram.content"));
			lambdaQueryIndependent = Double.parseDouble(map.get("lambda.query.independent"));
			sigma = Double.parseDouble(map.get("sigma"));
			this.n = Integer.parseInt(map.get("n"));
			
			virtualDocumentsUnigramIndexPath = map.get("virtual.documents.unigram.index.path");
			nodeVirtualDocumentsUnigramIndexPath = map.get("node.virtual.documents.unigram.index.path");
			virtualDocumentsBigramIndexPath = map.get("virtual.documents.bigram.index.path");
			nodeVirtualDocumentsBigramIndexPath = map.get("node.virtual.documents.bigram.index.path");
			
			this.rootFile = map.get("root.nodes.file.path");
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Orders the roots and writes them in a file.
	 * */
	public void topRootSelectionPhase() {
		File f = new File(rootFile);
		File fP = f.getParentFile();//support directory
		if(!fP.exists()) {
			fP.mkdirs();
		}
		try {
			AnswerLists lists = this.findRoots();
			List<String> candidateRootsDocNo = lists.getDocNoList();
			List<Integer> candidateRootsDocId = lists.getDocIdList();
			List<String> orderedRoots = this.rankRoots(candidateRootsDocNo, candidateRootsDocId);
			this.writeTheResults(orderedRoots);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeTheResults(List<String> orderedRoots) {
		Path outputPath = Paths.get(this.rootFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING)) {
			writer.write("<roots>");
			writer.newLine();
			for(String node : orderedRoots) {
				String[] nParts = node.split(",");
				String nodeId = nParts[0];
				String nodeScore = nParts[1];
				writer.write("\t<root score=\"" + nodeScore+ "\">" + nodeId + "</root>");
				writer.newLine();
			}
			writer.write("</roots>");
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/** This method uses the query to find the roots that are at 
	 * the center of some virtual document r* that contains all
	 * the query words inside the query.
	 * <p>
	 * creates and returns two lists, one with the docno and the other
	 * with the docid of the roots.
	 * The docid are 0-based and are the identifiers used by
	 * Terrier when it saves into the index the documents. They
	 * identify a document inside the index.
	 * The docno is usually a field that is inserted as metadata.
	 * It is indicated in the terrier.properties file as a metadata.
	 * In the TREC format, you specify it in the DOCNO tag.
	 *<p> 
	 * An important lesson here if you are using Terrier: use
	 * the docid. Only when you need it, you can easily switch 
	 * to the docno. The passage from docno to docid is not immediate
	 * since there is apparently no metadata ready in the
	 * Terrier index to do such a thing.
	 * 
	 * @param indexPath path where to find the index of the virtual documents v* obtained from
	 * the costruction of small subgraphs. This is not the index of the simple node documents.
	 * */
	private /*List<String>*/ AnswerLists findRoots() throws IOException {
		//open the index:
		Index index = IndexOnDisk.createIndex(this.virtualDocumentsUnigramIndexPath, "data");

		//get the query words after indexing (make sure to take unigrams)
		ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
		System.setProperty("tokeniser", "EnglishTokeniser");
		List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);

		//get the inverted index
		PostingIndex<Pointer> invertedIndex = (PostingIndex<Pointer>) index.getInvertedIndex();
		MetaIndex meta = index.getMetaIndex();

		//use the index to know which documents have the query words
		Lexicon<String> lexicon = index.getLexicon();
		List<String> candidatesList = new ArrayList<String>();
		//the terrier docid of the candidates root documents
		List<Integer> candidateDocId = new ArrayList<Integer>();
		//for the first query word
		LexiconEntry le1 = lexicon.getLexiconEntry(queryWords.get(0));
		if(le1!=null) {
			//get the documents containing the query word
			IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le1);
			while (postings.next() != IterablePosting.EOL) {
				candidateDocId.add(postings.getId());

				String docno = meta.getItem("docno", postings.getId());
				candidatesList.add(docno);
			}
		}

		//now proceed with the other keywords. Do the intersections of the sets containing the words
		//to get at the end only the documents with all the keywords
		for(int i = 1; i< queryWords.size(); ++i) {
			//find the documents of this query word
			List<String> newSet = new ArrayList<String>();
			List<Integer> newSetDocId = new ArrayList<Integer>();
			LexiconEntry le = lexicon.getLexiconEntry(queryWords.get(i));
			if(le != null) {
				IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le);
				while (postings.next() != IterablePosting.EOL) {
					String docno = meta.getItem("docno", postings.getId());
					newSet.add(docno);
					newSetDocId.add(postings.getId());
				}
				//intersection of the new list with the first one
				candidateDocId.retainAll(newSetDocId);
				candidatesList.retainAll(newSet);
			}
		}
		
		//added later in order to have a correspondence
		//between docno and docid at the same index of the 
		//two lists
		candidatesList.clear();
		for(int i = 0; i <  candidateDocId.size(); ++i) {
			int docid = candidateDocId.get(i);
			String docno = meta.getItem("docno", docid);
			candidatesList.add(docno);
		}
		//DEBUG string
		//System.out.println("candidate virtual documents: " + candidatesList);

		AnswerLists answerLists = new AnswerLists();
		answerLists.setDocIdList(candidateDocId);
		answerLists.setDocNoList(candidatesList);
		//now we have the roots. We need to rank them
		return answerLists;
//		return candidatesList;
	}
	
	/** Just a little class to return 2 lists. One with the
	 * docid and the other with the docno of the documents 
	 * ranked with terrier. 
	 * I admit I had to use this little trick because
	 * I wanted to use the docno at all costs, while it is
	 * much more useful to stick with the docid and then 
	 * transit to the docno only when you need it. 
	 * Let it be a lesson to us all folks.
	 * */
	public class AnswerLists {
		public List<String> docNoList;
		public List<Integer> docIdList;
		
		public List<String> getDocNoList() {
			return docNoList;
		}
		public void setDocNoList(List<String> docNoList) {
			this.docNoList = docNoList;
		}
		public List<Integer> getDocIdList() {
			return docIdList;
		}
		public void setDocIdList(List<Integer> docIdList) {
			this.docIdList = docIdList;
		}
	}
	
	private List<String> rankRoots(List<String> rootsDocno,
									List<Integer> rootsDocid) {
		//XXX 
//		if(roots.size()<=n)
//		return roots;
		
		Connection connection = null;
		try {
			connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectionString, this.getClass().getName());
			
			//we need a little information for the query independent potential function
			//i.e. the total degree of the graph
			String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from " + this.schema + ".yosi_node";
			PreparedStatement totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet totalDegreeRS = totalDegreeStatement.executeQuery();
			if(totalDegreeRS.next()) {
				totalDegree = totalDegreeRS.getInt(1);
			}
			
			//max heap to score the nodes 
			PriorityQueue<YosiNode> queue = new PriorityQueue<YosiNode>(new YosiNodeScoreComparator());
			//object to keep the parameters we are using in a simple way
			YosiDataWrapper parameters = this.prepareDataWrapper(connection, totalDegree);
			int counter = 0;
			
			for(String rDocno : rootsDocno) {//for every root t
				if(Thread.interrupted()) {
					ThreadState.setOnLine(false);
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
					return null;
				}
				
				int rDocid = rootsDocid.get(counter);
				counter++;
				double score = YosiNodeStaticScoringFunction.computeKeywordNodeScore(Integer.parseInt(rDocno),
						rDocid,
						connection, 
						query, 
						parameters);
				
				//create an object YosiNode with the id and the score just computed
				YosiNode root = new YosiNode();
				root.setId_(Integer.parseInt(rDocno));
				root.setScore(score);
				//add the node to the max heap. 
				queue.add(root);
			}
			List<String> orderedRoots = new ArrayList<String>();
			for(int i = 0; i < n; ++i) {
				YosiNode node = queue.poll();
				if(node==null) {
					break;
				}
				orderedRoots.add(node.getId_()+"," + node.getScore());
			}

			return orderedRoots;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if(connection != null) 
			{
				try {
					ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());
//					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
		
	}

	/** Method to prepare separately the object that contains all the useful parameters
	 * */
	private YosiDataWrapper prepareDataWrapper(Connection connection, long totalDegree) {
		YosiDataWrapper parameters = new YosiDataWrapper();
		
		parameters.setJdbcConnectingString(this.jdbcConnectionString);
		
		Index virtualDocumentsUnigramIndex = IndexOnDisk.createIndex(virtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramVirtualDocumentsIndex(virtualDocumentsUnigramIndex);
		
		Index nodeVirtualDocumentsUnigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramNodeIndex(nodeVirtualDocumentsUnigramIndex);
		
		Index virtualDocumentsBigramIndex = IndexOnDisk.createIndex(virtualDocumentsBigramIndexPath, "data");
		parameters.setBigramVirtualDocumentsIndex(virtualDocumentsBigramIndex);
		
		Index nodeVirtualDocumentsBigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsBigramIndexPath, "data");
		parameters.setBigramNodeIndex(nodeVirtualDocumentsBigramIndex);
		
		parameters.setSigma(sigma);
		
		parameters.setFilePath(rootFile);
		parameters.setQuery(query);
		parameters.setN(n);
		
		parameters.setLambdaBigramContent(lambdaBigramContent);
		parameters.setLambdaBigramTitle(lambdaBigramTitle);
		parameters.setLambdaUnigramContent(lambdaUnigramContent);
		parameters.setLambdaUnigramTitle(lambdaUnigramTitle);
		parameters.setLambdaQueryIndependent(lambdaQueryIndependent);
		
		parameters.setConnection(connection);
		parameters.setTotalDegree(totalDegree);

		return parameters;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getRootFile() {
		return rootFile;
	}

	public void setRootFile(String rootFile) {
		this.rootFile = rootFile;
	}

	public String getVirtualDocumentsUnigramIndexPath() {
		return virtualDocumentsUnigramIndexPath;
	}

	public void setVirtualDocumentsUnigramIndexPath(String virtualDocumentsUnigramIndexPath) {
		this.virtualDocumentsUnigramIndexPath = virtualDocumentsUnigramIndexPath;
	}

	public String getNodeVirtualDocumentsUnigramIndexPath() {
		return nodeVirtualDocumentsUnigramIndexPath;
	}

	public void setNodeVirtualDocumentsUnigramIndexPath(String nodeVirtualDocumentsUnigramIndexPath) {
		this.nodeVirtualDocumentsUnigramIndexPath = nodeVirtualDocumentsUnigramIndexPath;
	}

	public String getVirtualDocumentsBigramIndexPath() {
		return virtualDocumentsBigramIndexPath;
	}

	public void setVirtualDocumentsBigramIndexPath(String virtualDocumentsBigramIndexPath) {
		this.virtualDocumentsBigramIndexPath = virtualDocumentsBigramIndexPath;
	}

	public String getNodeVirtualDocumentsBigramIndexPath() {
		return nodeVirtualDocumentsBigramIndexPath;
	}

	public void setNodeVirtualDocumentsBigramIndexPath(String nodeVirtualDocumentsBigramIndexPath) {
		this.nodeVirtualDocumentsBigramIndexPath = nodeVirtualDocumentsBigramIndexPath;
	}

}
