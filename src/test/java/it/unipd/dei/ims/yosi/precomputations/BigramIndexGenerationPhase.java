package it.unipd.dei.ims.yosi.precomputations;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.CollectionStatistics;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Yosi part 4.1
 * <p>
 * This class implements the indexing of a collection of
 * TREC documentss in an index with all the information that we could. 
 * 
 * If you want to index with bigrams, you need to set the correct terrier.properties file
 * and set in this file the following informations:
 * <ul>
 * <li>tokeniser The tokeniser you want to use. englishTokeniser the default, BigramTokeniser for Bigrams.</li>
 * <li>termpipelines The element of the pipeline to use. Stopwords when you are operating
 * on unigram, NGramStopwords for nGrams. For the Stemmer you may only insert the stemming
 * class you want to use, e.g. PorterStemmer.
 * </ul>
 * */
public class BigramIndexGenerationPhase {

	/** This method indexes a class of TREC document. A directory full of files
	 * into an index directory.
	 * <p>
	 * If you want to index with bigrams, you need to set the correct terrier.properties file
	 * and set in this file the following informations:
	 * <ul>
	 * <li>tokeniser The tokeniser you want to use. englishTokeniser the default, BigramTokeniser for Bigrams.</li>
	 * <li>termpipelines The element of the pipeline to use. Stopwords when you are operating
	 * on unigram, NGramStopwords for nGrams. For the Stemmer you may only insert the stemming
	 * class you want to use, e.g. PorterStemmer.
	 * </ul>
	 * 
	 * */
	public static void bigramIndexing(String directoryToIndex, String destinationDirectory) {

		//get all the files inside the directory
		List<String> paths  = PathUsefulMethods.getListOfFiles(directoryToIndex);

		//we index the collection
		BasicIndexer indexer = new BasicIndexer(destinationDirectory, "data");
		Collection coll = new TRECCollection(paths);

		indexer.index(new Collection[]{coll});

		//index the collection with bigrams
		Index index = IndexOnDisk.createIndex(destinationDirectory, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		//done
	}
	
	
	// ##### ##### ##### #####

	public static void main(String[] args) throws Exception {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		String aDirectoryToIndex = map.get("TREC.documents.directory");
		String destinationDirectory = map.get("output.index.directory");

		BigramIndexGenerationPhase.bigramIndexing(aDirectoryToIndex, destinationDirectory);

		System.out.println("done");
	}
}
