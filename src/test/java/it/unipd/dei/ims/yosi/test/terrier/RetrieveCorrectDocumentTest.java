package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.util.Map;

import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Discover how to utilize the docno of a document to retrieve information about that document*/
public class RetrieveCorrectDocumentTest {

	public static void main(String[] args) throws IOException {
		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		Index index = IndexOnDisk.createIndex("/Volumes/HD2/RDF_DATASETS/LinedMDB/YOSI/node_documents_index", "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) index.getDirectIndex();
		DocumentIndex doi = index.getDocumentIndex();
		Lexicon<String> lex = index.getLexicon();
		int docid = 10; //docids are 0-based
		IterablePosting postings = di.getPostings(doi.getDocumentEntry(docid));
		while (postings.next() != IterablePosting.EOL) {
		    Map.Entry<String,LexiconEntry> lee = lex.getLexiconEntry(postings.getId());
		    System.out.println(lee.getKey() + " with frequency " + postings.getFrequency());
		}
		
		//if possible, find the docid of a document with certain docno
		MetaIndex meta = index.getMetaIndex();
		int docno = meta.getDocument("docid", "19");
		System.out.println("docno: " + docno);
		
//		PostingIndex<Pointer> inv = (PostingIndex<Pointer>) index.getInvertedIndex();
		System.out.print("haha");
	}
}
