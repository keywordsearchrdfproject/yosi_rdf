package it.unipd.dei.ims.yosi.online.generating.answers;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.bigram.Bigram;
import it.unipd.dei.ims.terrier.bigram.BigramUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;
import it.unipd.dei.ims.yosi.datastructures.YosiNodeScoreComparator;
import it.unipd.dei.ims.yosi.utils.MathUsefulMethods;

/** Yosi RDF phase 5.
 * <p>
 * Ranking of the root nodes based on score(Q, r) as described by Yosi.
 * <p>
 * NB: we have made the following usage of the ID of the nodes produced in the 
 * yosi_node table. Every node is identified by it ID. The same ID is used also to identify
 * the virtual document v* build around that central node.
 * 
 * */
public class TopRootsSelectionPhase {

	private static final String SQL_GET_NODE_RELATIVE_WEIGHT = "select v_id, u_id, ws_ from yosi_virtual_document where v_id=? and u_id=?";

	private static final String SQL_GET_VIRTUAL_DOCUMENT_NODES = "select v_id, u_id, ws_ from yosi_virtual_document where v_id=?";

	private static final String SQL_GET_FIELDS = "SELECT title_, content_"
			+ " from yosi_node where id_ = ?";

	private static final String SQL_GET_LENGTHS = "SELECT unigram_length_title, unigram_length_content, "
			+ "bigram_length_title, bigram_length_content from yosi_node where id_ = ?";

	/** This method uses the query to find the roots that are at 
	 * the center of some virtual document r* that contains all
	 * the query words inside the query.
	 * 
	 * @param indexPath path where to find the index of the virtual documents v* obtained from
	 * the costruction of small subgraphs. This is not the index of the simple node documents.
	 * */
	public static List<String> findRoots(String indexPath, String query) throws IOException {
		//open the index:
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " virtual documents v*, with number of"
				+ " tokens: " 
				+ index.getCollectionStatistics().getNumberOfTokens());

		//get the query words after indexing
		List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);

		//get the inverted index
		PostingIndex<Pointer> invertedIndex = (PostingIndex<Pointer>) index.getInvertedIndex();
		MetaIndex meta = index.getMetaIndex();

		//use the index to know which documents have the query words
		Lexicon<String> lexicon = index.getLexicon();
		List<String> candidatesList = new ArrayList<String>();

		//for the first query word
		LexiconEntry le1 = lexicon.getLexiconEntry(queryWords.get(0));
		if(le1!=null) {
			//get the documents containing the query word
			IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le1);
			while (postings.next() != IterablePosting.EOL) {
				String docno = meta.getItem("docno", postings.getId());
				candidatesList.add(docno);
			}
		}

		//now proceed with the other keywords. Do the intersections of the sets containing the words
		//to get at the end only the documents with all the keywords
		for(int i = 1; i< queryWords.size(); ++i) {
			//find the documents of this query word
			List<String> newSet = new ArrayList<String>();
			LexiconEntry le = lexicon.getLexiconEntry(queryWords.get(i));
			IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le);
			while (postings.next() != IterablePosting.EOL) {
				String docno = meta.getItem("docno", postings.getId());
				newSet.add(docno);
			}
			//intersection of the new list with the first one
			candidatesList.retainAll(newSet);
		}

		System.out.println("candidate virtual documents: " + candidatesList);

		//now we have the roots. We need to rank them
		return candidatesList;
	}

	/** This method gives a value to the roots, defining its ranking. We need to compute five values.
	 *
	 * @param trecIndexPath path to the directory containing the index of the virtual documents v* 
	 * produced from the nodes.
	 * */
	public static List<String> rankRoots(List<String> roots, 
			String jdbcConnectingString, 
			Connection connection, 
			String trecNodeIndexPath, 
			String query,
			int n,
			double lambdaUnigramTitle,
			double lambdaUnigramContent,
			double lambdaBigramTitle,
			double lambdaBigramContent,
			double lambdaQueryIndependent,
			double sigma) {

		//the index is now created and open for reading
		Index nodeIndex = IndexOnDisk.createIndex(trecNodeIndexPath, "data");

		try {
			//get the query words after indexing
			List<String> queryWords = TerrierUsefulMethods.pipelineStopWordsAndStemmerToList(query);

			
			//TODO put again this two lines of code when finished debugging
			//if the number of answer root is < than the number n, we don't need to rank them
			//in order to give back only the top-n. 
//			if(roots.size()<=n)
//				return roots;

			//we need a little information for the query independent potential function
			//i.e. the total degree of the graph
			String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from yosi_node";
			PreparedStatement totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet totalDegreeRS = totalDegreeStatement.executeQuery();
			int totalDegree = 0;
			if(totalDegreeRS.next()) {
				totalDegree = totalDegreeRS.getInt(1);
			}

			//max heap to score the nodes 
			PriorityQueue<YosiNode> queue = new PriorityQueue<YosiNode>(new YosiNodeScoreComparator());

			//one computation for each root
			for(String r : roots) {//for every root t
				//r is a number, the id of the node v defining the document v*
				//we need to compute the score of this root

				double score = computeRootScore(r, connection, queryWords, nodeIndex, 
						lambdaUnigramTitle, 
						lambdaUnigramContent, 
						lambdaBigramTitle, 
						lambdaBigramContent, 
						lambdaQueryIndependent, 
						sigma,
						totalDegree);
				//create an object YosiNode with the id and the score just computed
				YosiNode root = new YosiNode();
				root.setId_(Integer.parseInt(r));
				root.setScore(score);
				//add the node to the max heap. 
				queue.add(root);
			}

			List<String> orderedRoots = new ArrayList<String>();
			for(int i = 0; i < n; ++i) {
				YosiNode node = queue.poll();
				if(node==null) {
					break;
				}
				orderedRoots.add(node.getId_()+"," + node.getScore());
			}

			return orderedRoots;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	/** Computes score(Q, r) following the Yosi specifics.
	 * 
	 * @param r id of the node root inside the yosi_node table
	 * @param queryWords a list of strings with the keywords of the query, after they have been indicized by Terrier.
	 * @param totalDegree the total degree of the graph, i.e. the sum of the degree of all the nodes*/
	private static double computeRootScore(String r, 
			Connection connection, 
			List<String> queryWords, 
			Index nodeIndex,
			double lambdaUnigramTitle,
			double lambdaUnigramContent,
			double lambdaBigramTitle,
			double lambdaBigramContent,
			double lambdaQueryIndependent,
			double sigma,
			int totalDegree) {
		//the score we are building here
		double score = 0;
		//we need to compute 5 different scores
		for(String q : queryWords) {
			//for each query word there is a contribution
			try {
				//first element: potential function of the unigrams for the content and titlefield
				score+=computeUnigramPotentialFunctions(r, connection, q, nodeIndex, 
						lambdaUnigramTitle,
						lambdaUnigramContent,
						sigma);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		score += computeQueryIndependentPotentialFunction(r, connection, totalDegree, lambdaQueryIndependent);

		//re-create the query as whole string
		List<Bigram> bigramList = BigramUsefulMethods.extractDistinctBigramsFromListOfString(queryWords);
		for(Bigram queryBigram : bigramList) {
			score += computeBigramPotentialFunctions(r, connection, queryBigram, nodeIndex,
					lambdaBigramTitle,
					lambdaBigramContent,
					sigma);
		}
		return score;
	}


	private static double computeBigramPotentialFunctions(String r_id, Connection connection, Bigram queryBigram, Index nodeIndex,
			double lambdaBigramTitle,
			double lambdaBigramContent,
			double sigma) {

		//compute the maximum likelihood estimators
		Pair <Double, Double> bigramPair = computeBigramMaximumLikelihoodEstimators(r_id, connection, queryBigram, sigma);
		double p_qi_v_title = bigramPair.getLeft();
		double p_qi_v_content = bigramPair.getRight(); 

		//compute the alphas
		//		double alpha_title = computeAlphaForNodesTitleFieldBigrams(nodeIndex, r_title_field);
		//		double alpha_content = computeAlphaForNodesContentField(nodeIndex, r_content_field);

		//compute the Dirichlet smoothing values
		//TODO

		double potentialFunctionTitle = Math.log(p_qi_v_title);
		double potentialFunctionContent = Math.log(p_qi_v_content);

		double contribution = lambdaBigramTitle * potentialFunctionTitle + lambdaBigramContent * potentialFunctionContent;

		return contribution;
	}

	/** Computes the MLE for bigrams, that is P(q_i, q_i+1 | u*) for a virtual
	 * document u*, where u is a node of the graph.
	 * 
	 * @param queryBigram the bigram q_i, q_i+1 that is the token of this computation.
	 * @param sigma the parameter of the gaussian weight, as described in Yosi 2016.
	 * */
	private static Pair<Double, Double> 	computeBigramMaximumLikelihoodEstimators(String r_id, Connection connection, 
			Bigram queryBigram, double sigma) {

		//***** compute the numerator, wtf(queryBigram | r*) ***** //
		try {
			//first of all, we need the static relative weight of r
			//w.r.t. itself for the computation of the wtf
			PreparedStatement stmt = connection.prepareStatement(SQL_GET_NODE_RELATIVE_WEIGHT);
			int rId = Integer.parseInt(r_id);
			stmt.setInt(1, rId);
			stmt.setInt(2, rId);
			ResultSet rs = stmt.executeQuery();

			double vvScore = 0;
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}

			//now get the weight for every other node, save in vuScore
			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, rId);
			rs = stmt.executeQuery();

			double titleScore = 0, contentScore = 0;

			while(rs.next()) {//for each node u in v*
				//sum over all the nodes u in r* in order to compute wtf
				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				double weight = MathUsefulMethods.computeGaussianWeight(vuScore, vvScore, sigma);

				//get the title and content field in order to compute the bigram frequency
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();
				int titleTf = 0, contentTf = 0;

				//compute the term frequency
				if(fieldSet.next()) {
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");

					//get bigrams from the title
					List<Bigram> titleBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(title_);
					//get the bigram frequency
					titleTf = BigramUsefulMethods.getBigramFrequency(queryBigram, titleBigramList);

					//get bigrams from the content
					List<Bigram> contentBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(content_);
					//get the bigram frequency
					contentTf = BigramUsefulMethods.getBigramFrequency(queryBigram, contentBigramList);
				}
				//update the score of the title and content
				titleScore += weight * titleTf;
				contentScore += weight * contentTf;
			}
			//at the end of this WHILE, we have the numerator of the MLE. Now we get the length (denominator)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, rId);
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 0.0, contentLength = 0.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("bigram_length_title");
				contentLength = lengthRs.getDouble("bigram_length_content");
			}
			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = (double) (titleScore / titleLength);
			}
			if(contentLength!=0) {
				contentMLE = (double) (contentScore / contentLength);
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
		return null;

	}

	/** Computes the query independent potential function, as described by Yosi
	 * 
	 * @param totalDegree the degree of the graph, i.e. the sum of all the degrees*/
	private static double computeQueryIndependentPotentialFunction(String r, Connection connection, int totalDegree,
			double lambdaQueryIndependent) {
		//get the numerator
		String SQL_GED_NODE_DEGREE = "SELECT degree_ from yosi_node where id_ = ?";
		try {
			PreparedStatement ps = connection.prepareStatement(SQL_GED_NODE_DEGREE);
			ps.setInt(1, Integer.parseInt(r));

			ResultSet degreeRS = ps.executeQuery();
			int degree = 0;
			if(degreeRS.next()) {
				degree = degreeRS.getInt(1);
			}

			//now we compute the fraction
			double argument = (double) degree / totalDegree;
			return lambdaQueryIndependent * Math.log(argument);


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

	/**Computes the first element of the score, the sum over the unigram components
	 * 
	 * @param r a string with the id of the root node inside the yosi_node table.
	 * 
	 * @param queryWOrd is the keyword which we are currently scoring.
	 * @throws SQLException 
	 * */
	private static double computeUnigramPotentialFunctions(String r, Connection connection, 
			String queryWord, Index nodeIndex,
			double lambdaUnigramTitle,
			double lambdaUnigramContent,
			double sigma) throws SQLException {

		String SQL_SELECT_ROOT_NODE = "SELECT title_, content_, unigram_length_content,"
				+ " unigram_length_title FROM yosi_node WHERE id_ = ?";
		PreparedStatement rootPrepStatement = connection.prepareStatement(SQL_SELECT_ROOT_NODE);
		rootPrepStatement.setInt(1, Integer.parseInt(r));
		ResultSet rootRs = rootPrepStatement.executeQuery();
		String r_content_field = "", r_title_field = "";
		if(rootRs.next()) {
			r_content_field = rootRs.getString("content_");
			r_title_field = rootRs.getString("title_");
		}

		//compute the potential functions

		//first, compute the maximum likelihood estimator on the node r for the content field
		//P(q_i| v*_<content/title>)
		Pair<Double, Double> unigramPair = computeMaximumLikelihoodEstimators(r, queryWord, 
				connection, sigma, nodeIndex);
		double p_qi_v_title = unigramPair.getLeft();
		double p_qi_v_content = unigramPair.getRight();

		//compute the alphas
		double alpha_title = computeAlphaForNodesTitleField(nodeIndex, r_title_field);
		double alpha_content = computeAlphaForNodesContentField(nodeIndex, r_content_field);

		//compute the maximum likelihood estimator over all the collection (Dirichlet smoothing)
		double p_qi_V_title = computeTitleCollectionMaximumLikelihoodEstimator(nodeIndex, queryWord);
		double p_qi_V_content = computeContentCollectionMaximumLikelihoodEstimator(nodeIndex, queryWord);

		//argument of the logarithm
		double titleArgument = ((1 - alpha_title) * p_qi_v_title) + (alpha_title) * p_qi_V_title;
		double contentArgument = ((1 - alpha_content) * p_qi_v_content) + (alpha_content) * p_qi_V_content;

		double potentialFunctionTitle = Math.log(titleArgument);
		double potentialFunctionContent = Math.log(contentArgument);

		double contribution = lambdaUnigramTitle * potentialFunctionTitle + lambdaUnigramContent * potentialFunctionContent;

		return contribution;
	}

	/** Computes the value P(q_i | V_title)*/
	private static double computeTitleCollectionMaximumLikelihoodEstimator(Index nodeIndex, String queryWord) {
		//first, the total number of words in the collection of node documents
		Lexicon<String> lex = nodeIndex.getLexicon();
		//get the total number of words in the collection that appears in the title field
		long denominator = nodeIndex.getCollectionStatistics().getFieldTokens()[0];

		//the numerator, the term frequency of the word in all the documents in the collection
		LexiconEntry le = lex.getLexiconEntry(queryWord);
		FieldLexiconEntry fLe = (FieldLexiconEntry) le;
		int[] frequencies = fLe.getFieldFrequencies();
		int numerator = frequencies[0];//0 because TITLE is the first field by construction

		return (double) numerator/denominator;
	}

	/** Computes the value P(q_i | V_content)*/
	private static double computeContentCollectionMaximumLikelihoodEstimator(Index nodeIndex, String queryWord) {
		//get the total number of words in the collection that appears in the content field
		long denominator = nodeIndex.getCollectionStatistics().getFieldTokens()[1];

		//the numerator, the term frequency of the word in all the documents in the collection
		Lexicon<String> lex = nodeIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(queryWord);
		FieldLexiconEntry fLe = (FieldLexiconEntry) le;
		int[] frequencies = fLe.getFieldFrequencies();
		int numerator = frequencies[1];//1 because CONTENT is the second field by construction

		return (double) numerator/denominator;
	}

	/** Compute the value of alpha_title for nodes
	 * */
	private static double computeAlphaForNodesContentField(Index nodeIndex, String r_content_field){
		//the two field are TITLE and CONTENT by construction
		double[] averageFieldsLengths = nodeIndex.getCollectionStatistics().getAverageFieldLengths();
		double muContent = averageFieldsLengths[1];

		//the value |D|, the length of this document 
		MemoryIndex mIndex;
		double x = 0;
		try {
			mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(r_content_field);
			x = mIndex.getCollectionStatistics().getAverageDocumentLength();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (double) muContent / (muContent + x);
	}

	private static double computeAlphaForNodesTitleField(Index nodeIndex, String r_title_field){
		//the two field are TITLE and CONTENT by construction
		double[] averageFieldsLengths = nodeIndex.getCollectionStatistics().getAverageFieldLengths();
		double mu = averageFieldsLengths[0];

		//the value |D|, the length of this document 
		MemoryIndex mIndex;
		double x = 0;
		try {
			mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(r_title_field);
			x = mIndex.getCollectionStatistics().getAverageDocumentLength();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return (double) mu / (mu + x);
	}

	/** Computes P(q_i|r*) for the title field and the content field 
	 * for one query word q_i and a root r. This method return
	 * a Pair. The left element is the title MLE, the right
	 * element is the content MLE.
	 * 
	 * @param queryWord the query word q_i what we are considering now in the computation of the MLE.
	 * @param sigma the value sigma compairing in the wtf formula.
	 * @param nodeIndex the index of the documents corresponding to the single documents*/
	private static Pair<Double, Double> computeMaximumLikelihoodEstimators(String r, String queryWord, 
			Connection connection, double sigma,
			Index nodeIndex) {
		//first, compute wtf(q_i, v*)
		String firstSql = "select v_id, u_id, ws_ from yosi_virtual_document "
				+ "where v_id=? and u_id=?";
		try {
			//get the weight of v with respect to itself, save in vvScore
			PreparedStatement stmt = connection.prepareStatement(firstSql);
			int r_id = Integer.parseInt(r);
			stmt.setInt(1, r_id);
			stmt.setInt(2, r_id);

			ResultSet rs = stmt.executeQuery();
			double vvScore = 0;
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}

			//now get the weight for every other node, save in vuScore
			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, r_id);
			rs = stmt.executeQuery();

			double sumTitle = 0, sumContent = 0;

			while(rs.next()) {
				//sum over each node u in v* to compute the numerator of the maximum likelihood

				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				//now we can compute the weight to multiply with tf
				double num = vuScore - vvScore;
				num = - Math.pow(num, 2);
				double exp = (double) num / (2 * Math.pow(sigma, 2));
				exp = Math.exp(exp);

				//now we have the weight for this node. Let's find the tf and then
				//multiply the two
				int tfTitle = 0, tfContent = 0;
				//get the content field of the node u from the yosi_node table in order to compute the frequency of q_i in it
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();
				if(fieldSet.next()) {
					//get the text corresponding to the title and content field
					//we index it in order to get the term frequency of the query word q_i
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");
					try {
						//index the title
						MemoryIndex mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title_);
						Lexicon<String> mLex = mIndex.getLexicon();
						LexiconEntry mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfTitle = mLe.getFrequency();

						//index the content
						mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content_);
						mLex = mIndex.getLexicon();
						mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfContent = mLe.getFrequency();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				//multiply with the term frequency for title and content
				//(the weight is the same)
				//weight * tf
				double elem = exp * tfTitle;
				sumTitle = sumTitle + elem;

				elem = exp * tfContent;
				sumContent = sumContent + elem;
			}//end of the while representing the sum at numerator of the MLE

			//now retrieving the denominator (length of the document v*)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, Integer.parseInt(r));
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 1.0, contentLength = 1.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("unigram_length_title");
				contentLength = lengthRs.getDouble("unigram_length_content");
			}

			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = sumTitle / titleLength;
			}
			if(contentLength!=0) {
				contentMLE = sumContent / contentLength;
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;

	}

	
	
	
	//############################




	public static void main(String[] args) throws IOException {
		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/ranking_nodes.properties");
		String jdbcConnectingString = map.get("jdbc.connection.string");

		//index of the virtual documents
		String indexPath = map.get("virtual.documents.unigram.index.path");
		//index of the documents derived from the nodes
		String trecNodeIndexPath = map.get("node.virtual.documents.unigram.index.path");

		String query = map.get("query");
		double lambdaUnigramContent = 0.2, lambdaUnigramTitle = 0.2, lambdaBigramContent = 0.2, lambdaBigramTitle=0.2, lambdaQueryIndependent=0.2;
		double sigma = 1;
		int n = 10;

		//reading from the properties
		lambdaUnigramTitle = Double.parseDouble(map.get("lambda.unigram.title"));
		lambdaUnigramContent = Double.parseDouble(map.get("lambda.unigram.content"));
		lambdaBigramTitle= Double.parseDouble(map.get("lambda.bigram.title"));
		lambdaBigramContent = Double.parseDouble(map.get("lambda.bigram.content"));
		lambdaQueryIndependent = Double.parseDouble(map.get("lambda.query.independent"));
		sigma = Double.parseDouble(map.get("sigma"));
		n = Integer.parseInt(map.get("n"));

		List<String> candidatesRoot = TopRootsSelectionPhase.findRoots(indexPath, query);

		try {
			Connection connection = DriverManager.getConnection(jdbcConnectingString);
			List<String> orderedRoots = TopRootsSelectionPhase.rankRoots(candidatesRoot, 
					jdbcConnectingString, 
					connection, 
					trecNodeIndexPath, 
					query, 
					n, 
					lambdaUnigramTitle,
					lambdaUnigramContent,
					lambdaBigramTitle,
					lambdaBigramContent, 
					lambdaQueryIndependent,
					sigma);

			//print the roots in a file
			Path outputPath = Paths.get(map.get("roots.output.file"));
			BufferedWriter writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			writer.write("<roots>");
			writer.newLine();
			for(String node : orderedRoots) {
				String[] nParts = node.split(",");
				String nodeId = nParts[0];
				String nodeScore = nParts[1];
				writer.write("\t<root score=\"" + nodeScore+ "\">" + nodeId + "</root>");
				writer.newLine();
			}
			writer.write("</roots>");
			writer.close();
			connection.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 


		System.out.println("done");
	}
}
