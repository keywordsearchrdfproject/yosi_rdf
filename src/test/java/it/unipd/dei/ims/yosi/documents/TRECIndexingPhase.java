package it.unipd.dei.ims.yosi.documents;

import java.io.IOException;
import java.util.Map;

import it.unipd.dei.ims.rum.indexing.RumTRECIndexer;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** Phase 8 of the Yosi algorithm. 
 * We index the answer TREC documents in order to be able to use them later.
 * <p>
 * We index both with unigram and with bigram.
 * 
 * */
public class TRECIndexingPhase {

	public static void main(String[] args) throws IOException {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/indexing.properties");


		//my personal indexer
		RumTRECIndexer idx = new RumTRECIndexer();
		
		idx.setTerrier_home(map.get("terrier.home"));
		idx.setTerrier_etc(map.get("terrier.etc"));
		idx.setDirectoryToIndex(map.get("directory.to.index"));
		
		//index first for unigrams
		idx.setIndexPath(map.get("unigram.index.path"));
		idx.index("unigram");

		//then for bigrams
		idx.setIndexPath(map.get("bigram.index.path"));
		idx.index("bigram");
		
	}
}
