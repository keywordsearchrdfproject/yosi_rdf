package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.sql.Array;
import java.util.Map;

import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.BlockPosting;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class WordsInsideADocumentTest {

	public static void main(String[] args) throws IOException {
		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);

		String indexPath = map.get("v.d.index.path");
		//open the new index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents, with number of"
				+ "tokens: " 
				+ index.getCollectionStatistics().getNumberOfTokens());
		
		PostingIndex<Pointer> inv = (PostingIndex<Pointer>) index.getInvertedIndex();
		MetaIndex meta = index.getMetaIndex();
		Lexicon<String> lex = index.getLexicon();
		LexiconEntry le = lex.getLexiconEntry( "cameron" );
		IterablePosting postings = inv.getPostings((BitIndexPointer) le);
		while (postings.next() != IterablePosting.EOL) {
		    String docno = meta.getItem("docno", postings.getId());
//		  int[] positions = ((BlockPosting)postings).getPositions();
		    System.out.println(docno + " with frequency " );
//		    + postings.getFrequency() + " and positions ");
//		    + Array
//		s.toString(positions));
		}

	}
}
