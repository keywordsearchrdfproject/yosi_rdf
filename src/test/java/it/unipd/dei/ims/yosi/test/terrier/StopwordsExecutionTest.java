package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.util.Map;

import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.terms.PorterStemmer;
import org.terrier.terms.Stopwords;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;

public class StopwordsExecutionTest {

	public static void main(String[] args) throws Exception {
		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String document = "First they ignore you, then they laugh at you and hate you. Then they fight you. Then you win";
		String[] doc = document.split(" ");
		
		Stopwords stop = new Stopwords(null);
		PorterStemmer stemmer = new PorterStemmer();

		for(String s : doc) {
			s = s.replaceAll(",", "").replaceAll("\\.", "");
			boolean isStop = stop.isStopword(s);
			if(!isStop) {
				String result = stemmer.stem(s);
				System.out.println(result);
			}
		}
		
		MemoryIndex mem = TerrierUsefulMethods.getMemoryIndexFromDocument(document);
		

		
		
//		stemmer.stem();
//		System.out.println(stemmer.toString());

	}
}
