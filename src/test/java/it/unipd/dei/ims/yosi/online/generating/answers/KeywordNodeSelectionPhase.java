package it.unipd.dei.ims.yosi.online.generating.answers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.utility.ApplicationSetup;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.bigram.Bigram;
import it.unipd.dei.ims.terrier.bigram.BigramUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.yosi.datastructures.YosiDataWrapper;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;
import it.unipd.dei.ims.yosi.datastructures.YosiNodeScoreComparator;
import it.unipd.dei.ims.yosi.utils.MathUsefulMethods;
import it.unipd.dei.ims.yosi.utils.YosiNodeStaticScoringFunction;

/** Yosi RDF phase 6.
 * <p>
 * After the creation of a ranked list of roots, the top-n roots, here 
 * we select the top-n keyword nodes to create the set S. 
 * With this set, we will then create the result subgraphs.
 * */
public class KeywordNodeSelectionPhase {

	private static final String SQL_RETRIEVE_VIRTUAL_DOCUMENT_NODES = "SELECT u_id FROM "
			+ "yosi_virtual_document where v_id = ?";

	private static final String SQL_GET_NODE_FIELDS = "SELECT title_, content_ FROM yosi_node where id_ = ?";

	private static final String SQL_GET_STATIC_RELATIVE_WEIGHT = "SELECT v_id, u_id, ws_ FROM yosi_virtual_document "
			+ " where v_id=? and u_id=?";

	private static final String SQL_GET_VIRTUAL_DOCUMENT_NODES = "select v_id, u_id, ws_ "
			+ "from yosi_virtual_document where v_id=?";

	private static final String SQL_GET_LENGTHS = "SELECT unigram_length_title, unigram_length_content, "
			+ "bigram_length_title, bigram_length_content from yosi_node where id_ = ?";

	/** Provided the set of roots, this method retrieves the nodes that belongs to the
	 * virtual documents v* and scores, finds the ones matching some keyword, 
	 * and reurns them. These are the so called sets U_i, in the Yosi paper.
	 * 
	 * @param path a string with the path of the file containing the IDs of the root nodes in ranked order.
	 * @param n the number of top elements to be chosen.
	 * @throws IOException 
	 * */
	private static Map<String, List<Integer>> computeUSets(YosiDataWrapper parameters) throws IOException {
		Map<String, List<Integer>> USetsMap = new HashMap<String, List<Integer>>();

		//read from the file where we saved the IDs of the root nodes
		String path = parameters.getFilePath();
		Path inputPath = Paths.get(path);

		Connection connection = parameters.getConnection();
		String query = parameters.getQuery();

		try(BufferedReader reader = Files.newBufferedReader(inputPath, UsefulConstants.CHARSET_ENCODING)) {
			String line = "";
			//set the tokeniser we want to use, single-term
			ApplicationSetup.setProperty("tokeniser", "EnglishTokeniser");
			
			//get the query words
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(query);
			
			//initialize the U_i sets
			for(String q_i : queryWords) {
				List<Integer> U_i = new ArrayList<Integer>();
				USetsMap.put(q_i, U_i);
			}
			
			while((line=reader.readLine())!=null) {
				//for each root r

				//take the id of one root from the file
				int rootId = Integer.parseInt(line);

				//now use the root id to get all the nodes of the virtual document r*
				PreparedStatement pStmt = connection.prepareStatement(SQL_RETRIEVE_VIRTUAL_DOCUMENT_NODES);
				pStmt.setInt(1, rootId);
				ResultSet rs = pStmt.executeQuery();

				while(rs.next()) {
					//for each node u of the virtual document r*
					String uId = rs.getString("u_id");
					//now we need to check the content of u, and understand if it contains at least one query word
					PreparedStatement nodeFieldPS = connection.prepareStatement(SQL_GET_NODE_FIELDS);
					nodeFieldPS.setInt(1, Integer.parseInt(uId));
					ResultSet nodeFieldRS = nodeFieldPS.executeQuery();

					//tke the content field of the node. Because we suppose that the content field
					//includes the title field, we take only the first one.
					nodeFieldRS.next();
					String uContent = nodeFieldRS.getString("content_");

					//now get an index for the content
					MemoryIndex contentIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(uContent);
					Lexicon<String> uContentLex = contentIndex.getLexicon();

					for(String s : queryWords) {
						//for every query word, check if the node u contains the query word
						LexiconEntry le = uContentLex.getLexiconEntry(s);
						if(le!=null) {
							//if this node u contains the query word s, we can add it to U_i

							//take the set U_i
							List<Integer> U_i = USetsMap.get(s);
							//add the id of node u to this list if it's not already present
							//this can happen when a v* and a u* are overlapped
							int uID = Integer.parseInt(uId);
							if(!U_i.contains(uID))
								U_i.add(Integer.parseInt(uId));
						}
					}
					contentIndex.close();
				}
			}//last while, ended the root nodes r and their documents r*

			return USetsMap;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/** Given the sets U_i of keyword nodes, the method ranks them and
	 * prints them in a file whose path must be provided in the parameters
	 * object.
	 * 
	 * @param USetsMap map containing, for each query word, the corresponding set U_i.
	 * */
	private static void /*Map<String, PriorityQueue<YosiNode>>*/ keywordNodesRankingPhase(Map<String, List<Integer>> USetsMap, 
			Connection connection, String query, 
			YosiDataWrapper parameters) {
		//we need the total degree of the graph as information for further phases
		String SQL_GET_TOTAL_DEGREE = "SELECT SUM(degree_) from yosi_node";
		PreparedStatement totalDegreeStatement;
		int totalDegree = 0;
		BufferedWriter writer;
		String keywordsOutputFile = parameters.getKeywordsOutputFile();
		Path outputPath;
		int n = parameters.getN();

		//		Map<String, PriorityQueue<YosiNode>> rankedKeyNodes = new HashMap<String, PriorityQueue<YosiNode>>();

		try {
			totalDegreeStatement = connection.prepareStatement(SQL_GET_TOTAL_DEGREE);
			ResultSet totalDegreeRS = totalDegreeStatement.executeQuery();
			if(totalDegreeRS.next()) {
				totalDegree = totalDegreeRS.getInt(1);
			}
			outputPath = Paths.get(keywordsOutputFile);
			writer = Files.newBufferedWriter(outputPath, UsefulConstants.CHARSET_ENCODING);
			writer.write("<keynodes>");
			writer.newLine();

			parameters.setTotalDegree(totalDegree);

			//in the map we have all the sets U_i. We can score the nodes in each U_i. 
			//Then, we will take the top n of them. 

			//for each U_i, that is, for each query word
			for(Entry<String, List<Integer>> entry : USetsMap.entrySet()) {
				//take the list of nodes
				List<Integer> U_i = entry.getValue();
				//for each node in U_i

				//max heap to score the nodes 
				PriorityQueue<YosiNode> queue_i = new PriorityQueue<YosiNode>(new YosiNodeScoreComparator());
				for(int uID : U_i) {
					//we now rank this keyword node
					//do it in the same way of the roots

					//uId is the ID of the node u inside U_i, which contains the keyword q_i.
					//we compute now the score(Q, u_i)
					double score_u_i = YosiNodeStaticScoringFunction.computeKeywordNodeScore(uID, 0, connection, query, parameters);

					//create an object YosiNode with the id and the score just computed
					YosiNode u_i = new YosiNode();
					u_i.setId_(uID);
					u_i.setScore(score_u_i);

					queue_i.add(u_i);
				} //ended the computation for the nodes of a single U_i
				//now we have ended the scoring of the nodes in U_i. We print the first n of them.
				//write the query
				for(int i = 0; i < n; ++i) {
					YosiNode node = queue_i.poll();
					//control in case n is greater than the number of nodes
					if(node==null)
						break;
					writer.write("\t\t<node queryword=\""+ entry.getKey() + "\" score=\"" + node.getScore() + "\">" + node.getId_() + "</node>");
					writer.newLine();
				}
				writer.flush();
				//emty the queue if there are nodes left
				queue_i.clear();

			}//done for all U_i
			writer.write("</keynodes>");
			writer.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static double computeKeywordNodeScore(int vID, Connection connection, String query, 
			YosiDataWrapper parameters) {
		double score = 0;

		//**** FIRST COMPONENT: UNIGRAMS *****
		score += computeUnigramComponent(vID, connection, query, parameters);

		//***** SECOND COMPONENT: BIGRAMS *****
		score += computeBigramComponents(vID, parameters);

		//***** THIRD COMPONENT: QUERY INDEPENDENT *****
		score+=computeQueryIndependentPotentialFunction(vID, parameters);



		return score;
	}

	/**Computes the bigram component of the score(Q, v) function.
	 * */
	private static double computeBigramComponents(int vID, YosiDataWrapper parameters) {
		//the returning score
		double score = 0;

		//extrapolates all the possible bigrams from the query
		String query = parameters.getQuery();
		List<Bigram> bigramList = BigramUsefulMethods.extractDistinctBigramsFromString(query);
		for(Bigram queryBigram : bigramList) {
			score += computeOneBigramComponent(vID, queryBigram, parameters);
		}
		return score;
	}

	/** Computes the bigram contribution to the sum for one bigram*/
	private static double computeOneBigramComponent(int vID, Bigram queryBigram, YosiDataWrapper parameters) {
		double lambdaBigramTitle = parameters.getLambdaBigramTitle();
		double lambdaBigramContent = parameters.getLambdaBigramContent();


		//first, compute the MLE
		Pair<Double, Double> bigramPair = computeBigramMaximumLikelihoodEstimators(vID, queryBigram, parameters);

		//compute the alphas
		//TODO

		//compute the Dirichlet smoothing values
		//TODO

		//XXX from here on it is simplified since we don't have the Dirichlet smoothing (we don't have information
		//regarding the collection as bigram)
		double p_qi_v_title = bigramPair.getLeft();
		double p_qi_v_content = bigramPair.getRight(); 

		double potentialFunctionTitle = Math.log(p_qi_v_title);
		double potentialFunctionContent = Math.log(p_qi_v_content);

		double contribution = lambdaBigramTitle * potentialFunctionTitle + lambdaBigramContent * potentialFunctionContent;
		return contribution;

	}


	/** Computes the MLE for the title and the content field considering bigrams.
	 * */
	private static Pair<Double, Double> computeBigramMaximumLikelihoodEstimators(int vID, Bigram queryBigram, 
			YosiDataWrapper parameters) {

		Connection connection = parameters.getConnection();
		double sigma = parameters.getSigma();

		try {
			//computation of the numerator of the MLE
			//first take the relative static weight of w_s(v*, v) to be used a lot later
			PreparedStatement stmt = connection.prepareStatement(SQL_GET_STATIC_RELATIVE_WEIGHT);
			stmt.setInt(1, vID);
			stmt.setInt(2, vID);
			ResultSet rs = stmt.executeQuery();

			double vvScore = 0;
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}

			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, vID);
			rs = stmt.executeQuery();
			double titleNumerator = 0, contentNumerator = 0;
			while(rs.next()) {
				//sum over all the nodes u in v*

				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				double weight = MathUsefulMethods.computeGaussianWeight(vuScore, vvScore, sigma);

				//get the title and content field in order to compute the bigram frequency
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();

				//compute the term frequency
				int titleTf = 0, contentTf = 0;
				if(fieldSet.next()) {
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");

					//get bigrams from the title
					List<Bigram> titleBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(title_);
					//get the bigram frequency
					titleTf = BigramUsefulMethods.getBigramFrequency(queryBigram, titleBigramList);

					//get bigrams from the content
					List<Bigram> contentBigramList = BigramUsefulMethods.extractBigramsFromStringWithPipeline(content_);
					//get the bigram frequency
					contentTf = BigramUsefulMethods.getBigramFrequency(queryBigram, contentBigramList);
				}
				//update the score of the title
				titleNumerator += weight * titleTf;
				contentNumerator += weight * contentTf;
			}

			//at the end of this WHILE, we have the numerator of the MLE. Now we get the length (denominator)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, vID);
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 0.0, contentLength = 0.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("bigram_length_title");
				contentLength = lengthRs.getDouble("bigram_length_content");
			}

			//now we compute the MLEs
			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = (double) (titleNumerator / titleLength);
			}
			if(contentLength!=0) {
				contentMLE = (double) (contentNumerator / contentLength);
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static double computeQueryIndependentPotentialFunction(int vID, YosiDataWrapper parameters) {
		long totalDegree = parameters.getTotalDegree();
		double lambdaQueryIndependent = parameters.getLambdaQueryIndependent();

		//get the numerator
		Connection connection = parameters.getConnection();
		String SQL_GED_NODE_DEGREE = "SELECT degree_ from yosi_node where id_ = ?";
		try {
			PreparedStatement ps = connection.prepareStatement(SQL_GED_NODE_DEGREE);
			ps.setInt(1, vID);

			ResultSet degreeRS = ps.executeQuery();
			int degree = 0;
			if(degreeRS.next()) {
				degree = degreeRS.getInt(1);
			}

			//now we compute the fraction
			double argument = (double) degree / totalDegree;
			return lambdaQueryIndependent * Math.log(argument);


		} catch (SQLException e) {
			e.printStackTrace();
		}

		return 0;
	}

	/** Computes the first part of the score of a node u. That means it computes the title and content 
	 * part of the unigram component.
	 * */
	private static double computeUnigramComponent(int uID, Connection connection, String query,
			YosiDataWrapper parameters) {
		double score = 0;

		//take the keyword and process it through the pipeline
		List<String> queryWords = TerrierUsefulMethods.pipelineStopWordsAndStemmerToListDitinctWords(query);

		//sum over all the query words
		for(String s : queryWords) {
			score+=computeOneUnigramComponent(uID, connection, s, parameters);

		}

		return score;
	}

	/** Computes the value of the potential function for the title field of the unigram.
	 * */
	private static double computeOneUnigramComponent(int uID, Connection connection, String queryWord, 
			YosiDataWrapper parameters) {

		//**** 1 : compute the maximum likelihood estimator P(q_i, v*) for the title field
		Pair<Double, Double> unigramMLE = computeUnigramMaximumLikelihoodEstimators(uID, queryWord, parameters);

		//***** 2: compute the alphas *****
		Pair<Double, Double> alphas = computeAlphasForNodesUnigram(uID, parameters);

		//***** 3 : compute the smoothing values ****** //
		Pair<Double, Double> unigramSmooth = computeUnigramSmoothingValues(queryWord, parameters);

		//now we can compute the potential function for unigrams

		//title part
		//first get the values
		double titleAlpha = alphas.getLeft();
		double titleMLE = unigramMLE.getLeft();
		double titleSmooth = unigramSmooth.getLeft();
		//the computations
		double logArgument = ((1 - titleAlpha) * titleMLE) + ((titleAlpha)*titleSmooth);
		double titleLog = Math.log(logArgument);

		//content part
		double contentAlpha = alphas.getRight();
		double contentMLE = unigramMLE.getRight();
		double contentSmooth = unigramSmooth.getRight();
		// the computations
		logArgument = ((1 - contentAlpha) * contentMLE) + ((contentAlpha)*contentSmooth);
		double contentLog = Math.log(logArgument);

		//all together now
		double unigramContribution = parameters.getLambdaUnigramTitle() * titleLog + 
				parameters.getLambdaUnigramContent() * contentLog;
		return unigramContribution;

	}

	private static Pair<Double, Double> computeUnigramSmoothingValues(String queryWord, YosiDataWrapper parameters) {
		//compute the total number of words in the collection for both fields (denominators)
		Index nodeIndex = parameters.getNodeIndex();
		long[] denominators = nodeIndex.getCollectionStatistics().getFieldTokens();
		long titleDenominator = denominators[0];
		long contentDenominator = denominators[1];

		//get the numerator for both fields, that total frequency of the query word in the collection
		Lexicon<String> lex = nodeIndex.getLexicon();
		FieldLexiconEntry fLe = (FieldLexiconEntry)lex.getLexiconEntry(queryWord);
		int[] frequencies = fLe.getFieldFrequencies();
		int titleNumerator = frequencies[0];
		int contentNumerator = frequencies[1];

		double tVal = (double) titleNumerator / titleDenominator;
		double cVal = (double) contentNumerator / contentDenominator;

		Pair<Double, Double> pair = new MutablePair<Double, Double>(tVal, cVal);
		return pair;
	}

	private static Pair<Double, Double> computeAlphasForNodesUnigram(int vID, YosiDataWrapper parameters) {
		Index nodeIndex = parameters.getVirtualDocumentsIndex();
		double[] averageFieldsLength = nodeIndex.getCollectionStatistics().getAverageFieldLengths();

		double muTitle = averageFieldsLength[0];
		double muContent = averageFieldsLength[1];

		double xTitle = 0, xContent = 0;

		//get the document connected to the node v
		Connection connection = parameters.getConnection();
		try {
			PreparedStatement fieldPS = connection.prepareStatement(SQL_GET_NODE_FIELDS);
			fieldPS.setInt(1, vID);
			ResultSet fieldRS = fieldPS.executeQuery();
			String content_ = "", title_ = "";
			if(fieldRS.next()) {
				content_ = fieldRS.getString("content_");
				title_ = fieldRS.getString("title");
			}

			//now we take the length of the fields
			MemoryIndex titleIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title_);
			MemoryIndex contentIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content_);
			xTitle = titleIndex.getCollectionStatistics().getAverageDocumentLength();
			xContent = contentIndex.getCollectionStatistics().getAverageDocumentLength();
			titleIndex.close(); contentIndex.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		double tAlpha = (double) muTitle / (muTitle + xTitle);
		double cAlpha = (double) muContent / (muContent + xContent);

		Pair<Double, Double> alphaPair = new MutablePair<Double, Double>(tAlpha, cAlpha);
		return alphaPair;
	}

	/** Here we compute the MLE for the title and content fields for unigram. 
	 * That is, we compute P(q_i | v*).
	 * 
	 * @param vID id of the node u that we are scoring.
	 * */
	private static Pair<Double, Double> computeUnigramMaximumLikelihoodEstimators(int vID, String queryWord, YosiDataWrapper parameters) {

		//get the parameters from the data wrapper
		Connection connection = parameters.getConnection();

		// ****** NUMERATOR ******** //
		//first, we need w_s(v*, v), because it will be used many times

		try {
			double vvScore = 0;

			PreparedStatement stmt = connection.prepareStatement(SQL_GET_STATIC_RELATIVE_WEIGHT);
			stmt.setInt(1, vID);
			stmt.setInt(2, vID);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				vvScore = rs.getDouble("ws_");
			}
			//now we have the value w_s(v*, v). We now can compute the numerator, wtf(q_i, v*)
			double titleNumerator = 0, contentNumerator = 0;

			//get all the nodes composing v*
			stmt = connection.prepareStatement(SQL_GET_VIRTUAL_DOCUMENT_NODES);
			stmt.setInt(1, vID);
			rs = stmt.executeQuery();
			//iterate over the u in v*
			while(rs.next()) {
				//get the id of u
				int uId = rs.getInt("u_id");
				//get the w_s score of u in v*
				double vuScore = rs.getDouble("ws_");

				//compute the gaussian weight to multiply with the tf
				double weight = MathUsefulMethods.computeGaussianWeight(vuScore, vvScore, parameters.getSigma());

				//now we find the term frequencies
				int tfTitle = 0, tfContent = 0;
				//get the content field of the node u from the yosi_node table in order to compute the frequency of q_i in it
				PreparedStatement fieldPreparedStatement = connection.prepareStatement(SQL_GET_NODE_FIELDS);
				fieldPreparedStatement.setInt(1, uId);
				ResultSet fieldSet = fieldPreparedStatement.executeQuery();
				if(fieldSet.next()) {
					//get the text corresponding to the title and content field
					//we index it in order to get the term frequency of the query word q_i
					String title_=fieldSet.getString("title_");
					String content_ = fieldSet.getString("content_");
					try {
						//index the title and check the frequency of the word
						MemoryIndex mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(title_);
						Lexicon<String> mLex = mIndex.getLexicon();
						LexiconEntry mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfTitle = mLe.getFrequency();

						//index the content and get the frequency of the query word
						mIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(content_);
						mLex = mIndex.getLexicon();
						mLe = mLex.getLexiconEntry(queryWord);
						if(mLe!=null)
							tfContent = mLe.getFrequency();
						mIndex.close();
					} catch (Exception e) {
						e.printStackTrace();
					}

					//multiply with the term frequency for title and content
					//(the weight is the same)
					//weight * tf
					double elem = weight * tfTitle;
					titleNumerator = titleNumerator + elem;

					elem = weight * tfContent;
					contentNumerator = contentNumerator + elem;
				}
			}//end iteration over the nodes u of v*. THe numerators have been calculated

			//now retrieving the denominators (lengths of v*)
			PreparedStatement lengthStatement = connection.prepareStatement(SQL_GET_LENGTHS);
			lengthStatement.setInt(1, vID);
			ResultSet lengthRs = lengthStatement.executeQuery();
			double titleLength = 1.0, contentLength = 1.0;
			if(lengthRs.next()) {
				titleLength = lengthRs.getDouble("unigram_length_title");
				contentLength = lengthRs.getDouble("unigram_length_content");
			}

			double titleMLE = 0, contentMLE = 0;

			if(titleLength!=0) {
				titleMLE = titleNumerator / titleLength;
			}
			if(contentLength!=0) {
				contentMLE = contentNumerator / contentLength;
			}

			Pair<Double, Double> pair = new MutablePair<Double, Double>(titleMLE, contentMLE);
			return pair;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	//#######################

	public static void main(String[] args) throws IOException {

		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		map = PropertiesUsefulMethods.
				getSinglePropertyFileMap("properties/ranking_nodes.properties");
		
		YosiDataWrapper parameters = new YosiDataWrapper();

		String jdbcConnectingString = map.get("jdbc.connection.string");
		parameters.setJdbcConnectingString(jdbcConnectingString);
		
		String path = map.get("roots.output.file");
		
		int n = Integer.parseInt(map.get("n"));
		
		String query = map.get("query");
		
		String virtualDocumentsUnigramIndexPath = map.get("virtual.documents.unigram.index.path");
		String nodeVirtualDocumentsUnigramIndexPath = map.get("node.virtual.documents.unigram.index.path");
		String virtualDocumentsBigramIndexPath = map.get("virtual.documents.bigram.index.path");
		String nodeVirtualDocumentsBigramIndexPath = map.get("node.virtual.documents.bigram.index.path");
		
		Index virtualDocumentsUnigramIndex = IndexOnDisk.createIndex(virtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramVirtualDocumentsIndex(virtualDocumentsUnigramIndex);
		
		Index nodeVirtualDocumentsUnigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsUnigramIndexPath, "data");
		parameters.setUnigramNodeIndex(nodeVirtualDocumentsUnigramIndex);
		
		Index virtualDocumentsBigramIndex = IndexOnDisk.createIndex(virtualDocumentsBigramIndexPath, "data");
		parameters.setBigramVirtualDocumentsIndex(virtualDocumentsBigramIndex);
		
		Index nodeVirtualDocumentsBigramIndex = IndexOnDisk.createIndex(nodeVirtualDocumentsBigramIndexPath, "data");
		parameters.setBigramNodeIndex(nodeVirtualDocumentsBigramIndex);
		
		

		String keywordsOutputFile = map.get("keyword.nodes.output.file");

		double lambdaUnigramContent = 0.2, 
				lambdaUnigramTitle = 0.2, 
				lambdaBigramContent = 0.2, 
				lambdaBigramTitle=0.2, 
				lambdaQueryIndependent=0.2;

		lambdaUnigramTitle = Double.parseDouble(map.get("lambda.unigram.title"));
		lambdaUnigramContent = Double.parseDouble(map.get("lambda.unigram.content"));
		lambdaBigramTitle= Double.parseDouble(map.get("lambda.bigram.title"));
		lambdaBigramContent = Double.parseDouble(map.get("lambda.bigram.content"));
		lambdaQueryIndependent = Double.parseDouble(map.get("lambda.query.independent"));

		//sigma
		Double sigma = Double.parseDouble(map.get("sigma"));
		parameters.setSigma(sigma);

		try {
			Connection connection = DriverManager.getConnection(jdbcConnectingString);


			parameters.setFilePath(path);
			parameters.setQuery(query);
			parameters.setN(n);
			parameters.setConnection(connection);

			parameters.setLambdaBigramContent(lambdaBigramContent);
			parameters.setLambdaBigramTitle(lambdaBigramTitle);
			parameters.setLambdaUnigramContent(lambdaUnigramContent);
			parameters.setLambdaUnigramTitle(lambdaUnigramTitle);
			parameters.setLambdaQueryIndependent(lambdaQueryIndependent);

			parameters.setKeywordsOutputFile(keywordsOutputFile);

			//obtain the various keyword nodes, separated in their corresponding U_i
			Map<String, List<Integer>> USetsMap =  computeUSets(parameters);
			keywordNodesRankingPhase(USetsMap, connection, query, parameters);
			
			//close the indexes
			virtualDocumentsBigramIndex.close();
			virtualDocumentsUnigramIndex.close();
			nodeVirtualDocumentsBigramIndex.close();
			nodeVirtualDocumentsUnigramIndex.close();
			
			System.out.println("done");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
