package it.unipd.dei.ims.yosi.test.structures;

import it.unipd.dei.ims.yosi.datastructures.YosiNode;

public class CopyClassTest {

	public static void main(String[] args) {
		YosiNode test1 = new YosiNode("Gianni");
		
		YosiNode test2 = new YosiNode(test1);
		test1.setStringContent("haha");
		System.out.println("done");
	}
}
