package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.util.Map;

import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** This class tests how Terrier deals with fields. Given an index genrated with 
 * the command to distinguish fields, it finds the frequency of a word inside the fields in the collection.
 * */
public class IndexWithFieldsTest {

	public static void main(String[] args) throws IOException {
		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String indexPath = map.get("v.d.index.path");


		//open the new index
		Index index = IndexOnDisk.createIndex(indexPath, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents, with number of"
				+ "tokens: " 
				+ index.getCollectionStatistics().getNumberOfTokens());
		
		//lexicon entry for a words
		LexiconEntry entry = index.getLexicon().getLexiconEntry("actor");
		//number of documents in which the word appears
		int num = entry.getDocumentFrequency();
		//total frequency of the word inside the collection
		int totalFrequency = entry.getFrequency();
		//in the case of this index, we have fields, so a fied lexicon entry
		FieldLexiconEntry e = (FieldLexiconEntry) entry;
		//here we have an array with the frequencies of the word 
		int[] frequencies = e.getFieldFrequencies();
		
		Lexicon<String> lex = index.getLexicon();
		System.out.println("done");

	}
}
