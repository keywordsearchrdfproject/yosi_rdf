package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.BigramTokeniser;
import org.terrier.indexing.tokenisation.Tokeniser;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class TokenizationTest {

	public static void main(String[] args) throws IOException {

		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String elaborandum = "This is just a little test for you, my friend";

		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), Tokeniser.getTokeniser());
		
		while(!doc.endOfDocument()) {
			String second = doc.getNextTerm();
			System.out.println(second);
		}
		
		
	}
}
