package it.unipd.dei.ims.yosi.test.terrier;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.TaggedDocument;
import org.terrier.indexing.tokenisation.EnglishTokeniser;
import org.terrier.realtime.memory.MemoryIndex;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

public class TaggedDocumentFromInputStreamTest {

	public static void main(String[] args) throws Exception {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/indexing.properties");
		String terrier_home = map.get("terrier.home");
		String terrier_etc = map.get("terrier.etc");
		String unigramIndexPath = map.get("unigram.index.path");

		String bigramIndexPath = map.get("bigram.index.path");

		System.setProperty("terrier.home", terrier_home);
		System.setProperty("terrier.etc", terrier_etc);
		
		File input = new File("/Volumes/HD2/RDF_DATASETS/LinedMDB/YOSI/answer_documents/57618.trec");
		FileInputStream inputStream = new FileInputStream(input);
		
		//creation of the document from the string of text
		Document document = new TaggedDocument(inputStream, new HashMap(), new EnglishTokeniser());
//		while(!document.endOfDocument()) {
//			System.out.println(document.getNextTerm());
//		}
		
		MemoryIndex memIndex = new MemoryIndex();
		memIndex.indexDocument(document);
		double[] lengths = memIndex.getCollectionStatistics().getAverageFieldLengths();
		System.out.println(lengths[0]);
		
		inputStream = new FileInputStream(input);
		document = new TaggedDocument(inputStream, new HashMap(), new EnglishTokeniser());
		while(!document.endOfDocument()) {
			System.out.println(document.getNextTerm());
		}
		
	}
}
