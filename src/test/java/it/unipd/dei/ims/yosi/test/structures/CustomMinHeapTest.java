package it.unipd.dei.ims.yosi.test.structures;

import java.util.PriorityQueue;

import it.unipd.dei.ims.yosi.datastructures.YosiComparator;
import it.unipd.dei.ims.yosi.datastructures.YosiNode;

public class CustomMinHeapTest {

	public static void main(String[] args) {
		YosiNode n1 = new YosiNode("primo");
		n1.setDistance(1);
		
		YosiNode n2 = new YosiNode("secondo");
		n2.setDistance(5);
		
		YosiNode n3 = new YosiNode("terzo");
		n3.setDistance(10);
		
		PriorityQueue<YosiNode> queue = new PriorityQueue<YosiNode>(new YosiComparator());
		
		queue.add(n3);
		queue.add(n1);
		queue.add(n2);
		
		n1.setDistance(100);
		queue.remove(n1);
		queue.add(n1);
		
		
		while(!queue.isEmpty()) {
			System.out.println(queue.poll());
		}
	}
}
