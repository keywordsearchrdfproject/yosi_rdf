package it.unipd.dei.ims.yosi.precomputations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;

/**Phase 2 of the Yosi algorithm (still offline)
 * 
 * */
public class SingleNodeDocumentCreationPhase {

	
	private static final String SQL_GET_NODES = "SELECT id_, title_, content_ FROM yosi_node ORDER BY id_ limit ? offset ?";
	
	
	private static int docCounter = 0, fileCounter = 0;
	
	/**Utilizes the information inside the yosi_node table to create TREC files containing the
	 * documents corresponding to single nodes documents.
	 * */
	public static void createSingleNodeDocument(String jdbcConnectionString, String outputDirectory) {
		
		Connection connection;
		int limit = 10000, offset = 0;
		BufferedWriter writer = null;
		try {
			//clean the outputDiretory
			File outDir = new File(outputDirectory);
			if(!outDir.exists())
				outDir.mkdirs();
			FileUtils.cleanDirectory(outDir);
			
			connection = DriverManager.getConnection(jdbcConnectionString);
			while(true) {
				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, limit, offset, SQL_GET_NODES);
				offset+=limit;
				if(iterator.next()) {
					iterator.beforeFirst();
					while(iterator.next()) {
						//deal with the multiple documents inside a file
						if(docCounter%2048==0) {
							if(writer!=null) {
								writer.close();
							}
							fileCounter++;
//							File dir = new File(outputDirectory + "/" + fileCounter);
//							dir.mkdirs();
							//create a new file with a writer on it
							Path outputPath = Paths.get(outputDirectory + "/" + fileCounter + ".trec");
							writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8);
							System.out.println("printed " + docCounter + " node documents");
						}
						docCounter++;
						createNodeDocument(iterator, writer);
					}
				} else {
					break;
				}
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Creates the document of one node.
	 * 
	 * @param iterator a ResultSet located to the tuple we need to elaborate.
	 * @param writer BufferedWriter on the file where we are writing
	 * @throws IOException 
	 * @throws SQLException 
	 * */
	private static void createNodeDocument(ResultSet iterator, BufferedWriter writer) throws IOException, SQLException {
		int id = iterator.getInt("id_");
		
		writer.write("<DOC>");
		writer.newLine();
		
		writer.write("\t<DOCNO>"+id+"</DOCNO>");
		writer.newLine();
		
		writer.write("\t<TITLE>"+ iterator.getString("title_") + "</TITLE>");
		writer.newLine();
		
		writer.write("\t<CONTENT>"+ iterator.getString("content_") + "</CONTENT>");
		writer.newLine();
		writer.write("</DOC>");
		writer.newLine();
	}
	
	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/vd_creation.properties");
		
		String jdbcConnectionString = map.get("jdbc.connection.string");
		String outputDirectory = map.get("node.documents.directory");
		SingleNodeDocumentCreationPhase.createSingleNodeDocument(jdbcConnectionString, outputDirectory);
		
		System.out.print("done");
	}
}
