package it.unipd.dei.ims.yosi.precomputations;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.SQLUtilities;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;

/** Phase 1 of the Yosi algorithm. Should be done once offline.
 * <p>
 * Read all the subject nodes and computes some statistics. */
public class StatisticsPhase {
	
	private static final String SQL_SELECT_SUBJECTS = "SELECT DISTINCT SUBJECT_ FROM TRIPLE_STORE LIMIT ? OFFSET ?";
	
	private static final String SQL_GET_NEIGHBORS = "SELECT id_, subject_, predicate_, object_ from triple_store where subject_ = ?";
	
	private static final String SQL_GET_IN_DEGREE = "SELECT in_degree from node where node_name=?";
	
	private static final String SQL_INSERT_YOSI_NODE = "INSERT INTO public.yosi_node(" + 
			"	iri, title_, content_, degree_, static_weight_, sum_wtf_title, sum_wtf_content, ws_)\n" + 
			"	VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
	
	/** Calculates the statistics for the Yosi Algorithm and saves them in the corresponding 
	 * table in the database.
	 * <p>
	 * This methods computes the first statistics of the nodes, useful to run the part which builds the virtual documents.
	 * In particular, the method insert the information in the yosi_node table. This information includes:
	 * <ul>
	 * <li>ID of the node (autoincrement value)
	 * <li>URI of the node (index on this value)
	 * <li>title (field) the title field, composed of the text extrapolated only from the URI of the node
	 * <li>content (field) the content field, text composed by words extrapolated from the URI and the
	 * literal in the neighbor of the node.
	 * <li>degree total degree (comprising in degree and out degree) of the node. Useful to compute the static weight
	 * <li>static weight Static weight of the node. Computed from the degree (maybe redundant, study if necessary)
	 * </ul>
	 * <p>
	 * This methods reads from the triple_store table to obtain all the nodes that are subjects. 
	 * We do this, instead of all the nodes, because we are in RDF domain. A true node
	 * in the Yosi paper is a node obtained from a triple. In RDF some nodes are just ausiliary information, 
	 * or literals. A node which has literals as neighbors is a real entity node. THe literals
	 * produce an additional information that serves as composition for the node document.
	 * In RDF only URI nodes are subjects and, therefore, real counterparts of entity node. 
	 * <p>
	 * In the future we could explore the possibility to consider as entity nodes only the nodes that are
	 * subjects and moreover have literals as neighbors.  
	 * <p>
	 * NB: in the construction of the graph, we are considering only nodes which are Resources (URI)
	 * and which are subjects of certin triples. There can be nodes which are URI but compare
	 * only as object inside the graph. In this case, they are deemed too small to be considered as
	 * useful information.
	 * */
	public static void computeYosiStatistics(String jdbcConnectingString) {
		Connection connection = null;
		try {
			//connect to the database
			connection = DriverManager.getConnection(jdbcConnectingString);
			int limit = 100000;
			int offset = 0;
			int totalCount = 0;
			int counter = 0;
			
			while(true) {
				//execute the query to know the subjects
				ResultSet iterator = SQLUtilities.executeOffsetQuery(connection, limit, offset, SQL_SELECT_SUBJECTS);
				offset += limit;
				if(iterator.first()) {//if we still have nodes to read
					//get the cursor to the begin
					iterator.beforeFirst();
					PreparedStatement insertPrepared = connection.prepareStatement(SQL_INSERT_YOSI_NODE);
					while(iterator.next()) {//for each node in this window over the table
						//get the subject
						String subject = iterator.getString(1);
						//the title field, obtained from the URI of the node.
						String title = UrlUtilities.takeFinalWordFromIRI(subject);
						//content field. In Yosi it vas created as
						//attribute name : attribute content.
						//we add the strings taken from the attributes and
						//the values of the literal nodes. 
						String content = title;
						
						//get informations about the neighbours of the subject node
						//get the neighbors
						PreparedStatement ps = connection.prepareStatement(SQL_GET_NEIGHBORS);
						ps.setString(1, subject);
						ResultSet rs = ps.executeQuery();

						int outDegree = 0;

						//here we only create the documents of the single node and the first statistics
						while(rs.next()) {
							//for each neighbor
							//get the URI of the neighbor 
							String iriPredicate = rs.getString(3);
							String iriNeighbor = rs.getString(4);
							if(UrlUtilities.checkIfValidURL(iriNeighbor)) {
								//this is a proper neighbor
								//increment the out degree
								outDegree++;

								//TODO introduce a flag to decide if we want to to this or not
								//this neighbor is another entity node, but
								//we use it anyway to add information to the content field
//								content = content + "\t" + 
//										UrlUtilities.takeFinalWordFromIRI(iriPredicate) + "\t" + 
//										UrlUtilities.takeFinalWordFromIRI(iriNeighbor);
							}
							else {
								//this is a literal. We add the content to the node content
								content = content + "\t" + 
								UrlUtilities.takeFinalWordFromIRI(iriPredicate) + "\t" + 
										BlazegraphUsefulMethods.dealWithTheObjectLiteralString(iriNeighbor).stringValue();
								//at least one neighbor is a literal. We can consider this node an entity node
							}
						}
						
						ps.close();
						rs.close();
						
						//get the in degree of the node (in order to have the complete degree)
						ps = connection.prepareStatement(SQL_GET_IN_DEGREE);
						ps.setString(1, subject);
						
						rs = ps.executeQuery();
						rs.next();
						int inDegree = rs.getInt(1);
						
						//sum the in degree with the out degree of only URI nodes
						int degree = outDegree + inDegree;
						
						//now we can insert in the table yosi_node the information regarding this node
						
						
						//IRI
						insertPrepared.setString(1, subject);
						//title_
						insertPrepared.setString(2, title);
						//content_
						insertPrepared.setString(3, content);
						//degree
						insertPrepared.setInt(4, degree);
						//static_weight_
						double sw = (double) 1/ Math.log(Math.E + degree);;
						insertPrepared.setDouble(5, sw);
						//wtf_title
						insertPrepared.setDouble(6, 0);
						//wtf_content
						insertPrepared.setDouble(7, 0);
						
						//ws
						insertPrepared.setString(8, "");
						
						insertPrepared.addBatch();
						counter++;
						
						if(counter>=10000) {
							//execute a batch of insertions
							insertPrepared.executeBatch();
							totalCount += counter;
							counter=0;
							insertPrepared.clearBatch();
							System.out.println("inserted " + totalCount + " vertices");
						}
					}
					if(counter>0) {
						//insert the last remaining records
						insertPrepared.executeBatch();
						counter=0;
						insertPrepared.clearBatch();
						System.out.println("inserted " + totalCount + " vertices");
					}
					
				} else {//if we don't have nodes to read
					break;
				}
				
				/*XXX If you will need to consider also the URI without objects, you can make
				 * a query here on the node table, condtioning on the fact the out_degree=0.
				 * Then you can create the information for the yosi_node table checking if the node
				 * is a true URL or a literal.
				 * */
			}//end of the while true
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
		}
	}
	
	public static void main(String[] args) throws IOException {
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/statistics.properties");
		String jdbcConnectingString = map.get("jdbc.connecting.string");
		StatisticsPhase.computeYosiStatistics(jdbcConnectingString);
		
		System.out.print("done");
	}

}
