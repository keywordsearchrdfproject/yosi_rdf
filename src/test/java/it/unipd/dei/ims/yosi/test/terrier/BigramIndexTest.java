package it.unipd.dei.ims.yosi.test.terrier;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.terrier.indexing.Collection;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.CollectionStatistics;
import org.terrier.structures.FieldLexiconEntry;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.rum.utilities.PathUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** tests the ability to index a TREC collection using bigrams. We have a Stemmer with bigrams,
 * stop words removal with bigrams and bigram tokens.
 * 
 * We test if frequencies and average lengths are still correctly computed.
 * 
 * */
public class BigramIndexTest {

	public static void main(String[] args) throws Exception {
		//take the useful information by the properties file
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/trec_ranking_phase.properties");

		//get the terrier home and etc directory where we have the property file and set them
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String aDirectoryToIndex = "/Volumes/HD2/RDF_DATASETS/LinedMDB/YOSI/virtual_documents";
		List<String> paths  = PathUsefulMethods.getListOfFiles(aDirectoryToIndex);
		
		System.out.println(paths);
		
		//directory where to save the output index of bigrams
		String destinationIndex = "/Volumes/HD2/RDF_DATASETS/LinedMDB/YOSI/virtual_documents_bigram_index";

		//we index the collection
		BasicIndexer indexer = new BasicIndexer(destinationIndex, "data");
		Collection coll = new TRECCollection(paths);
		
		indexer.index(new Collection[]{coll});

		//index the collection with bigrams
		Index index = IndexOnDisk.createIndex(destinationIndex, "data");
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");
		
		CollectionStatistics cs = index.getCollectionStatistics();
		cs.getNumberOfFields();
		
		Lexicon<String> le = index.getLexicon();
		Entry<String, LexiconEntry> l = le.getLexiconEntry(1);
		String s = l.getKey();
		LexiconEntry sL = l.getValue();
		
		FieldLexiconEntry leEntry = (FieldLexiconEntry)le.getLexiconEntry("perform type");
		int[] frequencies = leEntry .getFieldFrequencies();
	}
}
