package it.unipd.dei.ims.yosi.test.terrier;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.terrier.indexing.Document;
import org.terrier.indexing.FileDocument;
import org.terrier.indexing.tokenisation.BigramTokeniser;
import org.terrier.indexing.tokenisation.SeparatedTrigramStemmer;
import org.terrier.indexing.tokenisation.Tokeniser;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;

/** Class to test my new BigramTokeniser
 * */
public class BigramTokenisationTest {
	public static void main(String[] args) throws IOException {

		//set the terrier properties
		Map<String, String> map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/support_to_terrier.properties");
		String terrierHome = map.get("terrier.home");
		String terrierEtc = map.get("terrier.etc");

		//setting system properties for terrier to find the terrier_home and the /etc directory
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		String elaborandum = "<DOC>This is just a litttle test for you, my 123456 friend<DOC>";
//		elaborandum = "<c1511545>,  RO, <c1705535>";

//		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), new BigramTokeniser());
		Document doc = new FileDocument(new StringReader(elaborandum), new HashMap(), new SeparatedTrigramStemmer());
		while(!doc.endOfDocument()) {
			String second = doc.getNextTerm();
			System.out.println(second);
		}
		
		
	}
}
